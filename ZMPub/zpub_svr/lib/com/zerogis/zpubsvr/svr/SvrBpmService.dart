import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/UserMethod.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/HttpKeyValueConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';

/*
 * 功能：工作流模块
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class SvrBpmService extends BaseService
{
  /*
    * 查询我的代办任务<br/>
    * @param {Object 必传} pager    分页对象
    * @param {Object 必传} listener 接口
    * @param {Object 选传} page     第几页
    * @param {Object 选传} pageSize 每一页条数
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryMyTask(Pager pager, HttpListener listener,
      {Map<String, Object> param, int page: 1, int pageSize: BasSoftwareConstant
          .PAGER_MIN_SIZE, String method})
  {
    BaseService.showProgressBar(listener);
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryMyTask)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addMap(param)
        .addPagerParam(page)
        .addParam(HttpKeyValueConstant.PARAM_PAGER_SIZE, pageSize)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryMyTask" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      Map<String, dynamic> data = {};

      // 取page对象数据
      if (jo[BasMapKeyConstant.MAP_KEY_TOTAL] != null)
      {
        PagerUtil.createPage(
            pager ?? (pager = new Pager()), page, jo[BasMapKeyConstant.MAP_KEY_TOTAL], pageSizeValue: pageSize);
      }

      // 存list数据和page数据
      data[BasMapKeyConstant.MAP_KEY_LIST] = jo[BasMapKeyConstant.MAP_KEY_DATA] ?? response.data;
      if (pager.getPageNo() != null && !CxTextUtil.isEmptyList(data[BasMapKeyConstant.MAP_KEY_LIST]))
      {
        pager.setPageNo(pager.getPageNo() + 1);
        data[BasMapKeyConstant.MAP_KEY_PAGER] = pager;
      }
      return data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryMyTask" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryMyTask" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
    * 查询用户候选任务<br/>
    * @param {Object 必传} pager    分页对象
    * @param {Object 必传} listener 接口
    * @param {Object 选传} page     第几页
    * @param {Object 选传} pageSize 每一页条数
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryUserTask(Pager pager, HttpListener listener,
      {Map<String, Object> param, int page: 1, int pageSize: BasSoftwareConstant
          .PAGER_MIN_SIZE, String method})
  {
    BaseService.showProgressBar(listener);
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryUserTask)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addMap(param)
        .addPagerParam(page)
        .addParam(HttpKeyValueConstant.PARAM_PAGER_SIZE, pageSize)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryUserTask" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      Map<String, dynamic> data = {};

      // 取page对象数据
      if (jo[BasMapKeyConstant.MAP_KEY_TOTAL] != null)
      {
        PagerUtil.createPage(
            pager ?? (pager = new Pager()), page, jo[BasMapKeyConstant.MAP_KEY_TOTAL], pageSizeValue: pageSize);
      }

      // 存list数据和page数据
      data[BasMapKeyConstant.MAP_KEY_LIST] = jo[BasMapKeyConstant.MAP_KEY_DATA] ?? response.data;
      if (pager.getPageNo() != null && !CxTextUtil.isEmptyList(data[BasMapKeyConstant.MAP_KEY_LIST]))
      {
        pager.setPageNo(pager.getPageNo() + 1);
        data[BasMapKeyConstant.MAP_KEY_PAGER] = pager;
      }
      return data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryUserTask" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryUserTask" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
    * 查询我的历史任务<br/>
    * @param {Object 必传} pager    分页对象
    * @param {Object 必传} listener 接口
    * @param {Object 选传} page     第几页
    * @param {Object 选传} pageSize 每一页条数
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryHisProc(Pager pager, HttpListener listener,
      {Map<String, Object> param, int page: 1, int pageSize: BasSoftwareConstant
          .PAGER_MIN_SIZE, String method})
  {
    BaseService.showProgressBar(listener);
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryHisProc)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addMap(param)
        .addPagerParam(page)
        .addParam(HttpKeyValueConstant.PARAM_PAGER_SIZE, pageSize)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryHisProc" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      Map<String, dynamic> data = {};

      // 取page对象数据
      if (jo[BasMapKeyConstant.MAP_KEY_TOTAL] != null)
      {
        PagerUtil.createPage(
            pager ?? (pager = new Pager()), page, jo[BasMapKeyConstant.MAP_KEY_TOTAL], pageSizeValue: pageSize);
      }

      // 存list数据和page数据
      data[BasMapKeyConstant.MAP_KEY_LIST] = jo[BasMapKeyConstant.MAP_KEY_DATA] ?? response.data;
      if (pager.getPageNo() != null && !CxTextUtil.isEmptyList(data[BasMapKeyConstant.MAP_KEY_LIST]))
      {
        pager.setPageNo(pager.getPageNo() + 1);
        data[BasMapKeyConstant.MAP_KEY_PAGER] = pager;
      }
      return data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryHisProc" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryHisProc" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
    * 查询我发起的未完成流程<br/>
    * @param {Object 必传} pager    分页对象
    * @param {Object 必传} listener 接口
    * @param {Object 选传} page     第几页
    * @param {Object 选传} pageSize 每一页条数
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryMyStarted(Pager pager, HttpListener listener,
      {Map<String, Object> param, int page: 1, int pageSize: BasSoftwareConstant
          .PAGER_MIN_SIZE, String method})
  {
    BaseService.showProgressBar(listener);
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryMyStarted)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addMap(param)
        .addPagerParam(page)
        .addParam(HttpKeyValueConstant.PARAM_PAGER_SIZE, pageSize)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryMyStarted" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      Map<String, dynamic> data = {};

      // 取page对象数据
      if (jo[BasMapKeyConstant.MAP_KEY_TOTAL] != null)
      {
        PagerUtil.createPage(
            pager ?? (pager = new Pager()), page, jo[BasMapKeyConstant.MAP_KEY_TOTAL], pageSizeValue: pageSize);
      }

      // 存list数据和page数据
      data[BasMapKeyConstant.MAP_KEY_LIST] = jo[BasMapKeyConstant.MAP_KEY_DATA] ?? response.data;
      if (pager.getPageNo() != null && !CxTextUtil.isEmptyList(data[BasMapKeyConstant.MAP_KEY_LIST]))
      {
        pager.setPageNo(pager.getPageNo() + 1);
        data[BasMapKeyConstant.MAP_KEY_PAGER] = pager;
      }
      return data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryMyStarted" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryMyStarted" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 撤销我的流程<br/>
   * @param {Object 必传} listener 接口
   * @param {Object 选传} param    参数
   * @param {Object 选传} id       值
   */
  static void updateRevokeProcess(HttpListener listener, {Map<String, Object> param, dynamic id, String method})
  {
    BaseService.showProgressBar(listener, text: "正在撤销");
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_RevokeProcess)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addMap(param)
        .addParam(BasMapKeyConstant.MAP_KEY_ID, id)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "updateRevokeProcess" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return MessageConstant.MSG_DEAL_SUCCESS;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "updateRevokeProcess" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "updateRevokeProcess" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 签收工作流<br/>
   * @param {Object 必传} id       签收工作流程的id
   * @param {Object 必传} listener 接口
   * @param {Object 选传} param    参数
   * @param {Object 选传} method   方法
   */
  static void claimTask(Object id, HttpListener listener, {Map<String, Object> param, String method})
  {
    BaseService.showProgressBar(listener);
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_ClaimTask)
        .addParam(HttpParamKeyValue.PARAM_KEY_ID, id)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addParam(HttpParamKeyValue.PARAM_KEY_USERNAME, UserMethod.getUser().getName())
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "claimTask" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return MessageConstant.MSG_DEAL_SUCCESS;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "claimTask" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "claimTask" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 完成工作流<br/>
   * @param {Object 必传} id       任务ID
   * @param {Object 必传} COMMENT_ 反馈信息
   * @param {Object 选传} FILES_   附件文件名(中间用;隔开)
   */
  static void completeTask(HttpListener listener, {String id, int approved, Map<String, Object> param, String method})
  {
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_CompleteTask)
        .addParam(HttpParamKeyValue.PARAM_KEY_ID, id)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addParam(HttpParamKeyValue.PARAM_KEY_USERNAME, UserMethod.getUser().getName())
        .addParam(HttpParamKeyValue.PARAM_KEY_FK_APPROVED, approved)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "completeTask" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return MessageConstant.MSG_DEAL_SUCCESS;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "completeTask" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "completeTask" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 移交工作流<br/>
   * @param {Object 必传} id       任务ID
   * @param {Object 必传} touser   移交给谁
   */
  static void assignTask(String id, String touser, HttpListener listener, {Map<String, Object> param, String method})
  {
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_AssignTask)
        .addParam(HttpParamKeyValue.PARAM_KEY_ID, id)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addParam(HttpParamKeyValue.PARAM_KEY_USERNAME, UserMethod.getUser().getName())
        .addParam(HttpParamKeyValue.PARAM_KEY_TOUSER, touser)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "assignTask" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return MessageConstant.MSG_DEAL_SUCCESS;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "assignTask" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "assignTask" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }


  /*
   * 退回任务(回到前一个UserTask)<br/>
   * @param {Object 必传} id       任务ID
   * @param {Object 必传} approved 反馈信息
   * @param {Object 选传} backto   附件文件名(中间用;隔开)
   */
  static void returnTask(HttpListener listener,
      {Object id, Object approved, Object backto, Map<String, Object> param, String method})
  {
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_ReturnTask)
        .addParam(HttpParamKeyValue.PARAM_KEY_ID, id)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addParam(HttpParamKeyValue.PARAM_KEY_USERNAME, UserMethod.getUser().getName())
        .addParam(HttpParamKeyValue.PARAM_KEY_FK_APPROVED, approved)
        .addParam(HttpParamKeyValue.PARAM_KEY_BACKTO, backto)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "returnTask" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return MessageConstant.MSG_DEAL_SUCCESS;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "returnTask" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "returnTask" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询历史公共建议<br/>
   * @param {Object 必传} id       签收工作流程的id
   */
  static void queryHisComment(HttpListener listener, {Map<String, Object> param, String method})
  {
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryHisComment)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryHisComment" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return response.data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryHisComment" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryHisComment" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询流程变量<br/>
   * @param {Object 必传} id       签收工作流程的id
   */
  static void getVariables(HttpListener listener, {Map<String, Object> param, Object tag, String method})
  {
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_GetVariables)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "getVariables" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      jo[BasMapKeyConstant.MAP_KEY_TAG] = tag;
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "getVariables" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "getVariables" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询流程进度<br/>
   * @param {Object 必传} id       签收工作流程的id
   */
  static void queryProcProg(HttpListener listener, {Map<String, Object> param, Object tag, String method})
  {
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryProcProg)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryProcProg" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      jo[BasMapKeyConstant.MAP_KEY_TAG] = tag;
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryProcProg" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryProcProg" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询流程实例信息<br/>
   * @param {Object 必传} id       流程实例的id
   */
  static void queryProcInst(HttpListener listener, {Map<String, Object> param, Object tag, String method})
  {
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryProcInst)
        .addParam(HttpParamKeyValue.PARAM_KEY_USERID, UserMethod.getUserNo())
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryProcInst" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      jo[BasMapKeyConstant.MAP_KEY_TAG] = tag;
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryProcInst" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryProcInst" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }
}
