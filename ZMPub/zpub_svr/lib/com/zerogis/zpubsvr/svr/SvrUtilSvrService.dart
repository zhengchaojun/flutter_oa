import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/HttpKeyValueConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';

/*
 * 功能：公共服务模块
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class SvrUtilSvrService extends BaseService
{
  /*
    * 查询实体表序列号<br/>
    * @param {Object 必传} _major 主类型
    * @param {Object 必传} _minor 子类型
    * @param {Object 选传} map 键值对
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void getSequence(int _major, int _minor, final HttpListener listener,
      {dynamic map, String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_UtilSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_GetSequence)
        .addParam(HttpParamKeyValue.PARAM_KEY_MAJOR, _major)
        .addParam(HttpParamKeyValue.PARAM_KEY_MINOR, _minor)
        .addMap(map)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "getSequence" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "getSequence" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(CxTextUtil.isEmpty(method) ? "getSequence" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }
}
