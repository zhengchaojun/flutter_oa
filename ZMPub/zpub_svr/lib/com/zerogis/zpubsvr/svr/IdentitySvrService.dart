import 'package:zpub_svr/com/zerogis/zpubsvr/bean/UserDep.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';

/*
 * 功能：IdentitySvr服务模块
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class IdentitySvrService extends BaseService
{
  /*
   * 查询用户部门<br/>
   */
  static void memberOfOrganPos(Object no, Object position, final HttpListener listener)
  {
    BaseService.showProgressBar(listener, text: "正在查询用户部门");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_IdentitySvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_MemberOfOrganPos)
        .addParam("no", no)
        .addParam("position", position)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid("memberOfOrganPos", jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      // 业务代码
      List<UserDep> list = (response.data as List)
          ?.map((e)
      => e == null ? null : UserDep.fromJson(e as Map<String, dynamic>))?.toList();
      return list;
    }).then((data)
    {
      BaseService.sendMessage("memberOfOrganPos", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage("memberOfOrganPos", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询用户部门<br/>
   */
  static void memberOfGroup(Object no, final HttpListener listener)
  {
    BaseService.showProgressBar(listener, text: "正在查询用户部门");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_IdentitySvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_MemberOfGroup)
        .addParam("no", no)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid("memberOfGroup", jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      // 业务代码
      List<UserDep> list = (response.data as List)
          ?.map((e)
      => e == null ? null : UserDep.fromJson(e as Map<String, dynamic>))?.toList();
      return list;
    }).then((data)
    {
      BaseService.sendMessage("memberOfGroup", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage("memberOfGroup", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }
}
