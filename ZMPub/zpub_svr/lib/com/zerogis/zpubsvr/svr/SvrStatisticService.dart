import 'dart:io';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/HttpKeyValueConstant.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/UserMethod.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/SvrExpConstant.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/SvrExpTypesConstant.dart';

/*
 * 功能：统计相关模块
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class SvrStatisticService extends BaseService
{
  /*
    * 缓冲区管网 gtype=3 矩形  4 多边形  5 圆  r-缓冲区半径<br/>
    * @param {Object 必传} _major 主类型
    * @param {Object 必传} _minor 子类型
    * @param {Object 必传} xy  坐标
    * @param {Object 必传} gtypes  类型
    * @param {Object 必传} gr  默认为0
    * @param {Object 必传} r 半径
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryBufferstat(dynamic map, final HttpListener listener, {String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_StatisticSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Bufferstat)
        .addMap(map)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryBufferstat" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo[BasMapKeyConstant.MAP_KEY_DATA];
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryBufferstat" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryBufferstat" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
    * 缓冲区管网 gtype=3 矩形  4 多边形  5 圆  r-缓冲区半径<br/>
    * @param {Object 必传} _major 主类型
    * @param {Object 必传} _minor 子类型
    * @param {Object 必传} layer  图层
    * @param {Object 必传} gtype  类型
    * @param {Object 必传} xy  坐标
    * @param {Object 必传} gr 半径
    * @param {Object 必传} r 半径
    * @param {Object 必传} regids 默认[可以传为0]
    * @param {Object 必传} rangeType 类型
    * @param {Object 必传} listener 接口
    * @param {Object 选传} page 第几页
    * @param {Object 选传} pageSize 每页数量
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryBybuffer(dynamic map, Pager pager, final HttpListener listener,
      {int page = 1, int pageSize: BasSoftwareConstant.PAGER_MIN_SIZE, String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_StatisticSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Querybybuffer)
        .addMap(map)
        .addPagerParam(page)
        .addParam(HttpKeyValueConstant.PARAM_PAGER_SIZE, pageSize)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryBybuffer" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      Map<String, dynamic> data = {};

      // 取page对象数据
      if (jo[BasMapKeyConstant.MAP_KEY_TOTAL] != null)
      {
        PagerUtil.createPage(
            pager ?? (pager = new Pager()), page, jo[BasMapKeyConstant.MAP_KEY_TOTAL], pageSizeValue: pageSize);
      }

      // 存list数据和page数据
      data[BasMapKeyConstant.MAP_KEY_LIST] = jo[BasMapKeyConstant.MAP_KEY_DATA] ?? response.data;
      if (pager.getPageNo() != null && !CxTextUtil.isEmptyList(data[BasMapKeyConstant.MAP_KEY_LIST]))
      {
        pager.setPageNo(pager.getPageNo() + 1);
        data[BasMapKeyConstant.MAP_KEY_PAGER] = pager;
      }
      return data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryBybuffer" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryBybuffer" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }


}
