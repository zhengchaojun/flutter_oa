import 'package:json_annotation/json_annotation.dart';

part 'UserDep.g.dart';

/*
 * 类描述：用户表和用户部门表
 * 作者：郑朝军 on 2019/7/4
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/7/4
 * 修改备注：
 */
@JsonSerializable()
class UserDep
{
  @JsonKey(name: 'id')
  int id;

  @JsonKey(name: 'glid')
  int glid;

  @JsonKey(name: 'creator')
  int creator;

  @JsonKey(name: 'no')
  String no;

  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'fno')
  String fno;

  @JsonKey(name: 'hrno')
  String hrno;

  @JsonKey(name: 'nickname')
  String nickname;

  @JsonKey(name: 'gender')
  int gender;

  @JsonKey(name: 'email')
  String email;

  @JsonKey(name: 'cell')
  String cell;

  @JsonKey(name: 'telno')
  String telno;

  @JsonKey(name: 'faxno')
  String faxno;

  @JsonKey(name: 'address')
  String address;

  @JsonKey(name: 'zipcode')
  String zipcode;

  @JsonKey(name: 'jjlxr')
  String jjlxr;

  @JsonKey(name: 'jjlxdh')
  String jjlxdh;

  @JsonKey(name: 'memo')
  String memo;

  @JsonKey(name: 'organid')
  int organid;

  @JsonKey(name: 'userid')
  int userid;

  @JsonKey(name: 'organname')
  String organname;

  @JsonKey(name: 'username')
  String username;

  @JsonKey(name: 'position')
  String position;

  UserDep(this.id, this.glid, this.creator, this.no, this.name, this.fno,
      this.hrno, this.nickname, this.gender, this.email, this.cell, this.telno,
      this.faxno, this.address, this.zipcode, this.jjlxr, this.jjlxdh,
      this.memo, this.organid, this.userid, this.organname, this.username,
      this.position,);

  int getId()
  {
    return id;
  }

  void setId(int id)
  {
    this.id = id;
  }

  int getGlid()
  {
    return glid;
  }

  void setGlid(int glid)
  {
    this.glid = glid;
  }

  int getCreator()
  {
    return creator;
  }

  void setCreator(int creator)
  {
    this.creator = creator;
  }

  String getNo()
  {
    return no;
  }

  void setNo(String no)
  {
    this.no = no;
  }

  String getName()
  {
    return name;
  }

  void setName(String name)
  {
    this.name = name;
  }

  String getFno()
  {
    return fno;
  }

  void setFno(String fno)
  {
    this.fno = fno;
  }

  String getHrno()
  {
    return hrno;
  }

  void setHrno(String hrno)
  {
    this.hrno = hrno;
  }

  String getNickname()
  {
    return nickname;
  }

  void setNickname(String nickname)
  {
    this.nickname = nickname;
  }

  int getGender()
  {
    return gender;
  }

  void setGender(int gender)
  {
    this.gender = gender;
  }

  String getEmail()
  {
    return email;
  }

  void setEmail(String email)
  {
    this.email = email;
  }

  String getCell()
  {
    return cell;
  }

  void setCell(String cell)
  {
    this.cell = cell;
  }

  String getTelno()
  {
    return telno;
  }

  void setTelno(String telno)
  {
    this.telno = telno;
  }

  String getFaxno()
  {
    return faxno;
  }

  void setFaxno(String faxno)
  {
    this.faxno = faxno;
  }

  String getAddress()
  {
    return address;
  }

  void setAddress(String address)
  {
    this.address = address;
  }

  String getZipcode()
  {
    return zipcode;
  }

  void setZipcode(String zipcode)
  {
    this.zipcode = zipcode;
  }

  String getJjlxr()
  {
    return jjlxr;
  }

  void setJjlxr(String jjlxr)
  {
    this.jjlxr = jjlxr;
  }

  String getJjlxdh()
  {
    return jjlxdh;
  }

  void setJjlxdh(String jjlxdh)
  {
    this.jjlxdh = jjlxdh;
  }

  String getMemo()
  {
    return memo;
  }

  void setMemo(String memo)
  {
    this.memo = memo;
  }

  int getOrganid()
  {
    return organid;
  }

  void setOrganid(int organid)
  {
    this.organid = organid;
  }

  int getUserid()
  {
    return userid;
  }

  void setUserid(int userid)
  {
    this.userid = userid;
  }

  String getOrganname()
  {
    return organname;
  }

  void setOrganname(String organname)
  {
    this.organname = organname;
  }

  String getUsername()
  {
    return username;
  }

  void setUsername(String username)
  {
    this.username = username;
  }

  String getPosition()
  {
    return position;
  }

  void setPosition(String position)
  {
    this.position = position;
  }

  factory UserDep.fromJson(Map<String, dynamic> srcJson) =>
      _$UserDepFromJson(srcJson);

  Map<String, dynamic> toJson() => _$UserDepToJson(this);
}
