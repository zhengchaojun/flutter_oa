/*
 * 类描述：bpm字段值相关
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class SvrBPMConstant
{
  static final String SVR_PROCESSNAME = 'processName'; // 状态

  static final String SVR_VARIABLES = 'variables'; // 变量

  static final String SVR_BUSINESSKEY = 'businessKey'; // 计划key

  static final String SVR_PRJID = "prjid"; // 项目ID

  static final String SVR_PROCID = "procid"; // 流程ID

  static final String SVR_PROCESS_DEF_ID = "processDefinitionId"; // 流程ID

  static final String SVR_PROCESSID = "processId"; // 流程ID

  static final String SVR_COMMENTS = "comments"; // 公共的消息数据

  static final String SVR_PROCKEY = "prockey"; // 流程key

  static final String SVR_VARIABLE = "variables"; // 变量

  static final String SVR_ASSIGNEE = "assignee"; // 变量

  static final String SVR_CREATETIME = "createTime"; // 变量

  static final String SVR_DUEDATE = "dueDate"; // 变量

  static final String SVR_FORMKEY = "formKey"; // 变量

  static final String SVR_OWNER = "owner"; // 变量

  static final String SVR_PRIORITY = "priority"; // 变量

  static final String SVR_PARENTTASKID = "parentTaskId"; // 变量

  static final String SVR_EXECUTIONID = "executionId"; // 变量

  static final String SVR_PROCESSINSTANCEID = "processInstanceId"; // 变量

  static final String SVR_TASKDEFINITIONKEY = "taskDefinitionKey"; // 变量

  static final String SVR_APPLY_USERNAME = "applyUserName"; // 变量
  
  static final String SVR_ACTIVITIS = "activities"; // 变量
  
  static final String SVR_ACTIVITYNAME = "activityName"; // 变量
  
  static final String SVR_ACTIVITYKEY = "activityKey"; // 变量
  
  static final String SVR_MESSAGE = "message"; // 变量
  
  static final String SVR_START_TIME = "startTime"; // 变量
  
  static final String SVR_END_TIME = "endTime"; // 变量

  static final String SVR_PROCINSTID = "procInstId"; // 变量
}