import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_uievent/com/zerogis/zpubevent/plugin/EventManagerPlugin.dart';

/*
 * 类描述：综合查询模块启动注册模块：可以理解成Android中的AndroidManifest.xml
 * 作者：郑朝军 on 2019/6/1
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/1
 * 修改备注：
 */
class EventPluginApplication
{
  void onCreate()
  {
    List<Plugin> plugin = InitSvrMethod.getInitSvrPlugins();
    plugin.forEach((plugin)
    {
      if (plugin.classurl == EventManagerPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 问题管理插件
        PluginsFactory.getInstance().add(plugin.name, new EventManagerPluginService());
      }
//      else if (plugin.classurl == QueryListPlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 综合查询结果，多条列表界面，ismcard页
//        PluginsFactory.getInstance().add(plugin.name, new QueryListPluginService());
//      }
    });
  }
}
