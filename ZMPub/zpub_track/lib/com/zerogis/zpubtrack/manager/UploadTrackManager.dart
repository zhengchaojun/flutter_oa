import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:common_utils/common_utils.dart';
import 'package:zpub_track/com/zerogis/zpubtrack/constant/FldDefValConstant.dart';

/*
 * 类描述：巡检轨迹记录网络上传 管理类
 * 作者：郑朝军 on 2020/5/20
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2020/5/20
 * 修改备注：
 */
class UploadTrackManager
{
  static UploadTrackManager mInstance;

  /*
   * 计划ID
   */
  dynamic plantID;

  /*
   * 起始日期
   */
  String mQsrq;

  static UploadTrackManager getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new UploadTrackManager();
    }
    return mInstance;
  }

  /*
   * 添加轨迹到数据库中
   * @param {Array} coors 收集的所有坐标集合
   * @param {Object} planid 计划ID
   * @param {Boolean} checkGeomSize 是否检验coors长度 [true=需要检验]
   */
  void add(coors,planid, HttpListener listener,{geosize:true})
  {
    if(!geosize && !CxTextUtil.isEmptyList(coors))
    {
      this.create(coors, planid, listener);
    }
    else if(coors.length >= FldDefValConstant.PAT_LOCATE_REC_GEOM)
    {
      this.create(coors, planid, listener);
    }
  }

  /*
   * 添加轨迹到数据库中
   * @param {Array} coors 收集的所有坐标集合
   * @param {Object} planid 计划ID
   */
  void create(coors, planid, HttpListener listener)
  {
    Map<String,dynamic> param = {
      'userid':UserMethod.getUserId(),
      'userno':UserMethod.getUserNo(),
      'username':UserMethod.getUser().getName(),
      'geom':coors,
      'qsrq':mQsrq,
      'zzrq':DateUtil.getNowDateStr(),
      'bm':DateUtil.getNowDateStr(),
      'planid':planid,
    };
    // 重置上传时间和获取到的坐标 上传坐标
    this.mQsrq = param['zzrq'];
    coors.clear();
//    int _major, int _minor, HttpListener listener, {Map<String, dynamic> value, String method}
//    SvrAreaSvrService.add(MajorMinorConstant.MAJOR_PAT, MajorMinorConstant.MINOR_PAT_LOCATEREC,listener,value:param );
  }

  /*
   * 更新日期
   */
  void updateQsrq()
  {
    this.mQsrq = DateUtil.getNowDateStr();
  }
}
