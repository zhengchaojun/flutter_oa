import 'package:flutter/material.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:weui/weui.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/core/UiWidgetCreator.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'SearchAroundPlugin', '周边搜索', '周边搜索', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'SearchAroundPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * 周边搜索主页 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class SearchAroundPlugin extends PluginStatefulBase
{
  SearchAroundPlugin({Key key, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new SearchAroundPluginState();
  }

  static String toStrings()
  {
    return "SearchAroundPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class SearchAroundPluginState extends PluginBaseState<SearchAroundPlugin>
{
  /*
   * 下拉菜单默认选择的值
   */
  LayerManagerConstant mLayerManager;

  /*
   * 所有图层
   */
  List mLayers;

  void initState()
  {
    super.initState();
    mLayerManager = LayerManager.getInstance();
    mLayers = mLayerManager.queryChild('glid', 0);
  }

  Widget build(BuildContext context)
  {
    return buildBody(context, SingleChildScrollView(child: WeRadioGroup(child: Column(children: createLayer())),));
  }

  /*
   * 创建所有菜单
   */
  List<Widget> createLayer()
  {
    List<Widget> list = <Widget>[];
    mLayers.forEach((value)
    {
      List child = mLayerManager.queryChild('glid', value['layer']);
      list.add(Card(child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        Padding(child: Text(
            value[DBFldConstant.FLD_NAMEC],
            textAlign: TextAlign.start,
            style: BasTextStyleRes.text_color_text1_larger_fontw900),
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),),
        Divider(),
        SizedBox(height: 5,),
        createMenuGridView(child),
      ],),) );
    });
    list.add(Padding(
      padding: EdgeInsets.symmetric(vertical: BasSizeRes.largest, horizontal: BasSizeRes.small_middle),
      child: WeButton('确定', theme: WeButtonType.acquiescent,),));
    return list;
  }

  /*
   * 创建一个菜单中的所有条目
   */
  Widget createMenuGridView(List<dynamic> menu)
  {
    List<Widget> list = <Widget>[];
    for (int i = 0; i < menu.length; i ++)
    {
      list.add(Padding(child: WeRadio(
        value: menu[i][BasMapKeyConstant.MAP_KEY_NAMEC],
        child: UiWidgetCreator.createCommonTopImgText2(
            menu[i], param: {
          BasMapKeyConstant.MAP_KEY_WIDTH: 20.0,
          BasMapKeyConstant.MAP_KEY_HEIGHT: 20.0,
          BasMapKeyConstant.MAP_KEY_STYLE: BasTextStyleRes.text_color_text1_larger
        }, onTap: ()
        {}),
      ), padding: EdgeInsets.symmetric(horizontal: 10),));
    }
    return Container(width: ScreenUtil.getInstance().getScreenWidth(),
      child: SingleChildScrollView(child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: list),
          scrollDirection: Axis.horizontal),);
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class SearchAroundPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new SearchAroundPlugin(), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new SearchAroundPlugin();
  }
}
