1:只存放系统级别(major=99)的bean对象：表对象，系统级别的主子类型,数据库相关通用配置信息存放,管理类相关也放置此处,适用于A，B，C类型的项目，如果不适用则可不放此处
2:提供给外部使用的API类的名称需要加上模块名称
3:尽量节省像这样代码
 举例：List<FldValue> queryFldValueGetSpinnerDataSort(Fld fld)
    {
        FldValuesManagerConstant fldValuesManagerConstant = FldValuesManager.getInstance();
        return fldValuesManagerConstant.queryFldValueGetSpinnerDataSort(fld.getTabname(), fld.getColname());
    }
  解决方案：最好是一行代码就可以取到数据：比如用cx.fldvalue.queryFldValueGetSpinnerDataSort(fld);一行代码搞定，好处：节省代码，操作方便，优化性能(节省了方法)

#使用方法



