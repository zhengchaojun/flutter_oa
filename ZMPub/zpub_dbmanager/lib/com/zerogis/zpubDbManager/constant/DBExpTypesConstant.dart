/*
 * 类描述：条件查询types字段相关
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class DBExpTypesConstant
{
  static const String EXP_TYPES_D = "d";

  static const String EXP_TYPES_S = "s";

  static const String EXP_TYPES_I = "i";

  static const String EXP_TYPES_I_I = "i,i";

  static const String EXP_TYPES_I_D = "i,d";

  static const String EXP_TYPES_I_I_I = "i,i,i";

  static const String EXP_TYPES_I_D_D = "i,d,d";

  static const String EXP_TYPES_I_T_T = "i,t,t";

  static const String EXP_TYPES_I_I_T = "i,i,t";

  static const String EXP_TYPES_I_I_D = "i,i,d";

  static const String EXP_TYPES_I_I_I_D = "i,i,i,d";

  static const String EXP_TYPES_I_I_T_T = "i,i,t,t";
}
