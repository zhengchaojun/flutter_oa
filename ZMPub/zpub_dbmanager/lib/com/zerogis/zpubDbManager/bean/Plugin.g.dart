// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Plugin.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Plugin _$PluginFromJson(Map<String, dynamic> json) {
  return Plugin()
    ..id = json['id'] as int
    ..cata = json['cata'] as String
    ..classurl = json['classurl'] as String
    ..closeable = json['closeable'] as int
    ..dir = json['dir'] as String
    ..icon = json['icon'] as String
    ..initpara = json['initpara']
    ..name = json['name'] as String
    ..runn = json['runn'] as int
    ..sys = json['sys'] as String
    ..titlec = json['titlec'] as String
    ..titlee = json['titlee'] as String
    ..pname = json['pname'] as String
    ..uitype = json['uitype'] as int
    ..service = json['service'] as String
    ..height = json['height'] as int
    ..width = json['width'] as int
    ..modal = json['modal'] as int;
}

Map<String, dynamic> _$PluginToJson(Plugin instance) => <String, dynamic>{
      'id': instance.id,
      'cata': instance.cata,
      'classurl': instance.classurl,
      'closeable': instance.closeable,
      'dir': instance.dir,
      'icon': instance.icon,
      'initpara': instance.initpara,
      'name': instance.name,
      'runn': instance.runn,
      'sys': instance.sys,
      'titlec': instance.titlec,
      'titlee': instance.titlee,
      'pname': instance.pname,
      'uitype': instance.uitype,
      'service': instance.service,
      'height': instance.height,
      'width': instance.width,
      'modal': instance.modal,
    };
