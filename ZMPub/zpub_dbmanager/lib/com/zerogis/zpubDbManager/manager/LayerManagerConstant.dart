/*
 * 类描述：整个应用程序的fld的管理类：主要针对fld做方法的封装
 * 作者：郑朝军 on 2019/5/10
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/10
 * 修改备注：
 */
abstract class LayerManagerConstant
{
  /*
   * 根据节点找孩子
   *
   * @param name 名称
   * @param glid 关联ID父节点
   * @return 符合条件的集合
   */
  List queryChild(String name, int glid);

  /*
   * 查询第一个layer孩子
   */
  dynamic queryFirstChildLayer();

  /*
   * 查询layer树且添加check=true
   */
  dynamic queryTreeLayerCheck();
}
