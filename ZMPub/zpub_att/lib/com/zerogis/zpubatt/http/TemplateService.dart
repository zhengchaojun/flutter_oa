import 'dart:convert';

import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';
import 'package:zpub_att/com/zerogis/zpubatt/bean/template/att/TemplateAtt.dart';
import 'package:zpub_att/com/zerogis/zpubatt/bean/template/process/CxCustfld.dart';
import 'package:zpub_att/com/zerogis/zpubatt/bean/template/process/Div.dart';
import 'package:zpub_att/com/zerogis/zpubatt/bean/template/process/TemplateProcess.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';
import 'package:zpub_att/com/zerogis/zpubatt/util/TemplateUtil.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';

/*
 * 功能：网络模版模块
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class TemplateService extends BaseService
{
  /*
   * 查询属性+模版条目模版+条目风格<br/>
   */
  static void queryTemplate(String fromKey, final HttpListener listener)
  {
    BaseService.showProgressBar(listener, text: "正在属性处理网页");
    HttpProtocol protocol = new HttpProtocol();
    protocol.setMethod(fromKey).get().then((response)
    {
      String template = response.data;
      template = TemplateUtil.xml2Json(template);
      TemplateAtt templates = TemplateAtt.fromJson(json.decode(template));
      return templates.elform.cxtmplfld;
    }).then((data)
    {
      BaseService.sendMessage(
          "queryTemplate", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          "queryTemplate", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询模版条目模版+条目风格<br/>
   */
  static void queryNetTemplates(String fromKey, String processDefinitionId,
      HttpListener listener, {Map<String, dynamic> variable})
  {
    BaseService.showProgressBar(listener, text: "正在属性处理网页");
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_GetProcDefForm)
        .addParam(HttpParamKeyValue.PARAM_KEY_ID, processDefinitionId)
        .addParam(HttpParamKeyValue.PARAM_KEY_FORM_KEY, TemplateUtil.html2Json(fromKey))
        .get()
        .then((response)
    {
      // 将模版转fld集合
      String template = response.data;
      template = TemplateUtil.xml2Json(template);
      List<Fld> result = <Fld>[];

      TemplateProcess templates = TemplateProcess.fromJson(
          json.decode(template));
      Object cxcustfld = templates.elform.cxcustfld;

      if (cxcustfld is List)
      {
        List<CxCustfld> list = cxcustfld?.map((e)
        =>
        e == null ? null : CxCustfld.fromJson(e as Map<String, dynamic>))
            ?.toList();

        list.forEach((custfld)
        {
          Fld fld = TemplateUtil.cfg2Fld(custfld.getCfg(), value: variable);
          result.add(fld);
        });
      }
      else
      {
        CxCustfld custfld = CxCustfld.fromJson(cxcustfld);
        Fld fld = TemplateUtil.cfg2Fld(custfld.getCfg(), value: variable);
        result.add(fld);
      }
      return result;
    }).then((data)
    {
      BaseService.sendMessage(
          "queryNetTemplates", data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          "queryNetTemplates", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询模版条目模版+按钮<br/>
   */
  static void queryNetTemplateBtn(String fromKey, String processDefinitionId,
      HttpListener listener)
  {
    BaseService.showProgressBar(listener, text: "正在属性处理网页");
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_GetProcDefForm)
        .addParam(HttpParamKeyValue.PARAM_KEY_ID, processDefinitionId)
        .addParam(HttpParamKeyValue.PARAM_KEY_FORM_KEY, TemplateUtil.html2Json(fromKey))
        .get()
        .then((response)
    {
      String template = response.data;

      template = TemplateUtil.xml2Json(template);
      TemplateProcess templates = TemplateProcess.fromJson(
          json.decode(template));
      String dataOptions = TemplateUtil.dataOptions(templates.elform.div.getDataOptions());
      return TemplateUtil.cfg2Json(dataOptions);
    }).then((data)
    {
      BaseService.sendMessage(
          "queryNetTemplateBtn", data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          "queryNetTemplateBtn", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询模版条目模版+按钮<br/>
   */
  static void queryNetTemplateProcBtn(String fromKey,
      String processDefinitionId,
      HttpListener listener)
  {
    BaseService.showProgressBar(listener, text: "正在属性处理网页");
    HttpProtocol protocol = new HttpProtocol();
    protocol.mUrl = UrlConstant.BASE_BPM_URL;
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD,
        HttpParamKeyValue.PARAM_VALUE_GetProcDefForm)
        .addParam(HttpParamKeyValue.PARAM_KEY_ID, processDefinitionId)
        .addParam(HttpParamKeyValue.PARAM_KEY_FORM_KEY, TemplateUtil.html2Json(fromKey))
        .get()
        .then((response)
    {
      String template = response.data;
      Div div = Div.fromJson(TemplateUtil.xml2JsonStartProcs(template));
      String dataOptions = TemplateUtil.dataOptions(div.getDataOptions());
      return TemplateUtil.cfg2Json(dataOptions);
    }).then((data)
    {
      BaseService.sendMessage(
          "queryNetTemplateProcBtn", data, WhatConstant.WHAT_NET_DATA_SUCCESS,
          listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          "queryNetTemplateProcBtn", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }
}
