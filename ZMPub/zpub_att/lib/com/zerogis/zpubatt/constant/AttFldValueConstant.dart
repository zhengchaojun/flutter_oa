/*
 * 类描述：数据库字段值相关
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class AttFldValueConstant
{
  // ----------------常用---------------------------
  static const String FLDVALUE_MS = "MS";

  static const String FLDVALUE_DISPC_FIEDL = "field";

  //----------------常用--DBValue相关-------------------------
  /*
   * 新增，录入
   */
  static const int ATT_STATE_NEW_EDIT = 0;

  /*
   * 查看
   */
  static const int ATT_STATE_DEFAULT = 1;

  /*
   * 编辑
   */
  static const int ATT_STATE_EDIT_ABLE = 2;


  /*
   * FLD表中字editable段的值
   */
  static const int EDIT_ABLE_NO = 0;// 不可以编辑

  static const int EDIT_ABLE_YES = 1;// 可以编辑

  /*
   * FLD表中newedit字段的值
   */
  static const int NEW_EDIT_NO = 0;// 新增不可以编辑

  static const int NEW_EDIT_YES = 1;// 新增可以编辑
}
