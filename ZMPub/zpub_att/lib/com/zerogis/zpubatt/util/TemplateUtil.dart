import 'dart:convert';

import 'package:xml2json/xml2json.dart';
import 'package:zpub_att/com/zerogis/zpubatt/bean/template/process/Cfg.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 类描述：模版处理工具类
 * 作者：郑朝军 on 2019/6/13
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/13
 * 修改备注：
 */
class TemplateUtil
{
  /*
   * xml转json字符串
   * @param template xml对象模版
   * retrun 返回json串
   */
  static String xml2Json(String template)
  {
    template = template.replaceAll(BasStringValueConstant.STR_COMMON_A, "");
    Xml2Json xml2json = Xml2Json();
    xml2json.parse(template);
    template = xml2json.toGData();
    return template;
  }

  /*
   * 将模版cfg字段转换成json
   * @param cfg [cfg=colname:state,title:状态,type:10,rows:2,editable:false,value:您的申请没有通过，请查阅相关意见!]
   * return {"colname":"state","title":"状态","type": "10","rows": "2","editable":false,"value":"您的申请没有通过，请查阅相关意见!"}
   */
  static Map<String, dynamic> cfg2Json(String cfg)
  {
    Map<String, dynamic> map = {};
    List<String> item = cfg.split(BasStringValueConstant.STR_COMMON_COMMA);
    item.forEach((result)
    {
      List<String> keyValue = result.split(BasStringValueConstant.STR_VALUE_COLON);
      String key = keyValue[0];
      dynamic value = keyValue[1];
      map[key] = value;
    });
    return map;
  }

  /*
   * 将模版cfg字段转换成Fld对象
   * @param cfg [cfg=colname:state,title:状态,type:10,rows:2,editable:false,value:您的申请没有通过，请查阅相关意见!]
   * return Fld对象
   */
  static Fld cfg2Fld(String cfgValue, {Map<String, dynamic> value})
  {
    Fld fld = new Fld();
    Map<String, dynamic> template = cfg2Json(cfgValue);

    Cfg cfg = Cfg.fromJson(template);
    fld.setTabname(value['table']);
    fld.setColname(cfg.getColname());
    fld.setNamec(cfg.getTitle());
    fld.setEditable(cfg.getEditables());
    fld.setDisptype(int.parse(cfg.getType()));
    fld.setNullable(cfg.getNullable());
    if (!CxTextUtil.isEmpty(cfg.getRows()))
    {
      fld.setDisprows(int.parse(cfg.getRows()));
    }

    if (!CxTextUtil.isEmptyMap(value) && !CxTextUtil.isEmpty(cfg.getValue()))
    {
      fld.setValue(
          StringUtil.checkValue(cfg.getValue()) ? value[StringUtil.extractValue(cfg.getValue())] : cfg.getValue());
    }
    else
    {
      fld.setValue(cfg.getValue());
    }
    return fld;
  }

  /*
   * 处理dataOptions
   */
  static String dataOptions(String dataOptions)
  {
    List<String> result = dataOptions.split(
        BasStringValueConstant.STR_COMMON_SINGLE_QUOTATION_MARK);
    dataOptions = "";
    result[DigitValueConstant.APP_DIGIT_VALUE_1] =
        result[DigitValueConstant.APP_DIGIT_VALUE_1].replaceAll(
            BasStringValueConstant.STR_COMMON_COMMA,
            BasStringValueConstant.STR_COMMON_COMMA_ZH);
    result.forEach((value)
    {
      dataOptions += value;
    });
    return dataOptions;
  }

  /*
   * xml转json字符串
   * @param template xml对象模版
   * retrun 返回json串
   */
  static Map<String, dynamic> xml2JsonStartProcs(String template)
  {
    template = xml2Json(template);
    return json.decode(template)["div"];
  }
  
  /*
   * html转json后缀
   * @param template 被替换的对象 patProc/ticketClose.html
   * retrun 返回json串 patProc/ticketClose.json
   */
  static String html2Json(String template)
  {
    template = template.replaceAll('form', 'json');
    return template.replaceAll('html', 'json');
  }

  static String html2JsonOld(String template)
  {
    return template;
  }
  
  static void test()
  {
//    String cfg = "{\"colname\":\"state\",\"title\":\"状态\",\"type\": \"10\",\"rows\": \"2\",\"editable\":false,\"value\":\"您的申请没有通过，请查阅相关意见!\"}";
//    String cfg = "{\"colname\":state,title:状态,type:10,rows:2,editable:false,value:您的申请没有通过，请查阅相关意见!}";
//    Map<String, dynamic> map = json.decode(cfg);
    Map<String, dynamic> map = cfg2Json(
        "colname:state,title:状态,type:10,rows:2,editable:false,value:您的申请没有通过，请查阅相关意见!");
    print(map);
  }
}
