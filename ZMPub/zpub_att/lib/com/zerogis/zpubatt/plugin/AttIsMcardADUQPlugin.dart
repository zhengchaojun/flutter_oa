import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:weui/weui.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'AttIsMcardADUQPlugin', 'OBD数据管理', 'OBD数据管理', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'AttIsMcardADUQPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * (待开发)属性页面，包含增删改查和定位的ismcard页 <br/>
 * 需要传入的键：<br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 *
 */
class AttIsMcardADUQPlugin extends AttIsMcardListBas
{
  AttIsMcardADUQPlugin({Key key, mInitPara, plugin}) :super(key: key, mInitPara: mInitPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new AttIsMcardADUQPluginState();
  }

  static String toStrings()
  {
    return "AttIsMcardADUQPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class AttIsMcardADUQPluginState extends AttIsMcardListBasState<AttIsMcardADUQPlugin>
{
  /*
   * 创建内容
   */
  List<Widget> createBodyWidget()
  {
    List<Widget> widgets = new List();
    Plugin plugin = new Plugin();
    mList.forEach((item)
    {
      widgets.add(Stack(children: <Widget>[
        GestureDetector(child: AttIsMCardWidget(widget.mInitPara[DBFldConstant.FLD_MAJOR],
          widget.mInitPara[DBFldConstant.FLD_MINOR], attVals: item, plugin: plugin,), onTap: ()
        {
          runPluginParam(AttFjPlugin.toStrings(), item);
        },),

        Positioned(child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
          WeButton(
            '定位',
            theme: WeButtonType.warn,
            onClick: ()
            {},
          ),
          WeButton(
            '删除',
            theme: WeButtonType.warn,
            onClick: ()
            {},
          ),
        ],), bottom: 10, right: 10,)
      ],));
    });
    return widgets;
  }


  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryBybuffer")
    {
      dealQuery(values);
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  void query(int page)
  {
    Map<String, Object> param = {
      HttpParamKeyValue.PARAM_KEY_MAJOR: widget.mInitPara[DBFldConstant.FLD_MAJOR],
      HttpParamKeyValue.PARAM_KEY_MINOR: widget.mInitPara[DBFldConstant.FLD_MINOR],
      HttpParamKeyValue.PARAM_KEY_LAYER: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_LAYER],
      HttpParamKeyValue.PARAM_KEY_XY: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_XY],
      HttpParamKeyValue.PARAM_KEY_R: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_R],

      HttpParamKeyValue.PARAM_KEY_GTYPE: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_GTYPE] ?? 1,
      HttpParamKeyValue.PARAM_KEY_GR: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_GR] ?? 0,
      'regids': widget.mInitPara['regids'] ?? 0,
      'rangeType': widget.mInitPara['rangeType'] ?? 1,
    };
    SvrStatisticService.queryBybuffer(param, m_pager, this, page: page, pageSize: BasSoftwareConstant.MAX_PAGER_SIZE);
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class AttIsMcardADUQPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new AttIsMcardADUQPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new AttIsMcardADUQPlugin();
  }
}
