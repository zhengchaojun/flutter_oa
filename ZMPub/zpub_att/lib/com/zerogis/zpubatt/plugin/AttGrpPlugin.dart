import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttFjPlugin.dart';
import 'package:zpub_att/com/zerogis/zpubatt/widget/RegDateAttWidget.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttConstant.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';


// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'AttGrpPlugin', '属性页', '属性页', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'AttGrpPlugin', '', '', '{"plugs":[{"name":"AttIsMcardOBDPlugin"}],"rightButtonIcon":"0xe7dd"}', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 1:属性页面，2:并且显示附件,3:添加属性的时候添加图形,4:属性默认生成名称，时间 5：spinner控件菜单做联动 举例：在茂名电信<br/>
 * 属性相关
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 选传} id    查询属性的ID
 * @param {Object 选传} att   属性值
 * @param {Object 选传} attState 状态(默认是查看[新增_录入=0],[查看=1],[编辑=2])
 * @param {Object 选传} valueChangedMethod 属性修改
 * @param {Object 选传} plugin 插件
 * @param {Object 选传} title  标题名称
 * @param {Object 选传} nullable 附件是否为可空
 * 附件组件相关(不做参考)
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 必传} id    查询属性的ID
 * @param {Object 选传} nullable 是否为可空
 * @param {Object 选传} edit 是否为编辑【true=编辑状态】
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class AttGrpPlugin extends AttFjPlugin
{
  AttGrpPlugin({Key key, mInitParam, plugin}) : super(key: key, plugin: plugin, mInitParam: mInitParam);

  State<StatefulWidget> createState()
  {
    return new AttGrpPluginState();
  }

  static String toStrings()
  {
    return "AttGrpPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class AttGrpPluginState extends AttFjPluginState<AttGrpPlugin>
{
  void onClick(Widget view)
  {
    if (view == btn_right)
    {
      String pluginName = (mPlugins[0] as Map) [BasMapKeyConstant.MAP_KEY_NAME];
      dynamic param = widget.mInitParam;
      runPluginParam(pluginName, param);
    }
  }

  /*
   * 创建属性组件
   */
  Widget createAttWidget()
  {
    Plugin plugin = widget.mInitParam[BasMapKeyConstant.MAP_KEY_PLUGIN] ?? widget.plugin;
    return RegDateAttWidget(widget.mInitParam[DBFldConstant.FLD_MAJOR], widget.mInitParam[DBFldConstant.FLD_MINOR],
        mId: widget.mInitParam[DBFldConstant.FLD_ID] ?? DigitValueConstant.APP_DIGIT_VALUE__1,
        mAttVals: widget.mInitParam[BasMapKeyConstant.MAP_KEY_ATT],
        attState: widget.mInitParam[AttConstant.ATT_STATE],
        mValueChangedMethod: widget.mInitParam[BasMapKeyConstant.MAP_KEY_VALUE_CHANGE_METHOD],
        plugin: plugin,
        key: key_btn_border);
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class AttGrpPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new AttGrpPlugin(mInitParam: initPara), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new AttGrpPlugin();
  }
}
