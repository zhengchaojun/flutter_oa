import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttFjPlugin.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/bean/Pager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:weui/weui.dart';

/*
 * 带有ismcard列表的分页的页面基类<br/>
 * 需要传入的键：<br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 *
 */
class AttIsMcardListBas extends PluginStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> mInitPara;

  AttIsMcardListBas({Key key, this.mInitPara, plugin}) :super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new AttIsMcardListBasState();
  }

  static String toStrings()
  {
    return "AttIsMcardListBas";
  }
}

/*
 * 页面功能 <br/>
 */
class AttIsMcardListBasState<T extends AttIsMcardListBas> extends PluginBaseState<T>
{
  /*
   * 分页对象
   */
  Pager m_pager = new Pager();

  /*
   * 查询过来的ismcard列表集合
   */
  List mList = new List();

  void initState()
  {
    super.initState();
    this.query(1);
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBodyWithRefresh(context, mEmptyView);
    return widget;
  }

  void query(int page)
  {
    if(page == DigitValueConstant.APP_DIGIT_VALUE_1)
    {
      mList.clear();
    }
  }

  void dealQuery(Object values)
  {
    if (values is Map && !CxTextUtil.isEmptyList(values[BasMapKeyConstant.MAP_KEY_LIST]))
    {
      setState(()
      {
        m_pager = values[BasMapKeyConstant.MAP_KEY_PAGER] ?? m_pager;
        mList.addAll(values[BasMapKeyConstant.MAP_KEY_LIST]);
        mEmptyView = SingleChildScrollView(child: Column(children: createBodyWidget()),);
      });
    }
  }

  /*
   * 创建内容
   */
  List<Widget> createBodyWidget()
  {
    List<Widget> widgets = new List();
    Plugin plugin = new Plugin();
    mList.forEach((item)
    {
      widgets.add(Stack(children: <Widget>[
        GestureDetector(child: AttIsMCardWidget(widget.mInitPara[DBFldConstant.FLD_MAJOR],
          widget.mInitPara[DBFldConstant.FLD_MINOR], attVals: item, plugin: plugin,), onTap: ()
        {
          runPluginParam(AttFjPlugin.toStrings(), item);
        },),

        Positioned(child: WeButton(
          '定位',
          theme: WeButtonType.warn,
          onClick: ()
          {},
        ), bottom: 10, right: 10,)
      ],));
    });
    return widgets;
  }

  /*
   * 根据名称启动插件且携带参数
   */
  @override
  Future<T> runPluginParam<T extends Object>(String pluginName, dynamic att)
  {
    mChildUniversalInitPara[DBFldConstant.FLD_MAJOR] = widget.mInitPara[DBFldConstant.FLD_MAJOR];
    mChildUniversalInitPara[DBFldConstant.FLD_MINOR] = widget.mInitPara[DBFldConstant.FLD_MINOR];
    mChildUniversalInitPara[DBFldConstant.FLD_ID] = att[DBFldConstant.FLD_ID];
    mChildUniversalInitPara[BasMapKeyConstant.MAP_KEY_ATT] = att;
    mChildUniversalInitPara[AttConstant.ATT_STATE] = AttFldValueConstant.ATT_STATE_DEFAULT;
    return super.runPluginName(pluginName);
  }


  @override
  Future<void> loadMore()
  {
    if (m_pager.getPageNo() != null && (m_pager.getPageNo() < (m_pager.getTotalPage() + 1)))
    {
      query(m_pager.getPageNo());
    }
    else
    {
      setState(()
      {
        mLoadingText = "已加载全部";
      });
    }
    return super.loadMore();
  }
}
