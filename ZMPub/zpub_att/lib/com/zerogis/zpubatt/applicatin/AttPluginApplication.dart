import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttFjPlugin.dart';
import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttGrpPlugin.dart';
import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttIsMcardADUQPlugin.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 类描述：属性模块启动注册模块：可以理解成Android中的AndroidManifest.xml
 * 作者：郑朝军 on 2019/6/1
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/1
 * 修改备注：
 */
class AttPluginApplication
{
  void onCreate()
  {
    List<Plugin> plugin = InitSvrMethod.getInitSvrPlugins();
    plugin.forEach((plugin)
    {
      if (plugin.classurl == AttFjPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 属性页面，并且显示附件
        PluginsFactory.getInstance().add(plugin.name, new AttFjPluginService());
      }
      else if (plugin.classurl == AttGrpPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 1:属性页面，2:并且显示附件,3:添加属性的时候添加图形,4:属性默认生成名称，时间 5：spinner控件菜单做联动
        PluginsFactory.getInstance().add(plugin.name, new AttGrpPluginService());
      }
      else if (plugin.classurl == AttIsMcardADUQPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 属性页面，包含增删改查和定位的ismcard页
        PluginsFactory.getInstance().add(plugin.name, new AttIsMcardADUQPluginService());
      }
    });
  }
}
