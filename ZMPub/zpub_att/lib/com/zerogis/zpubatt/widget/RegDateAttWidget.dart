import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:common_utils/common_utils.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';

/*
 * 1：根据主子类型生成属性组件:不传ID则不请求网络生成属性 2：区域，和时间做定制 <br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 选传} id  id值
 * @param {Object 选传} mAttVals 属性对象
 * @param {Object 选传} mValueChangedMethod 网络层返回数据之后结果回调对象
 * @param {Object 选传} plugin 插件对象
 * @param {Object 选传} attState 属性状态
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class RegDateAttWidget extends AttWidget
{
  RegDateAttWidget(major, minor,
      {Key key, mId = -1, mAttVals, plugin, ValueChanged<dynamic> mValueChangedMethod, attState: AttFldValueConstant
          .ATT_STATE_DEFAULT,})
      : super(
      major, minor, key: key,
      mId: mId,
      mAttVals: mAttVals,
      mValueChangedMethod: mValueChangedMethod,
      attState: attState,
      plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new RegDateAttWidgetState();
  }

  static String toStrings()
  {
    return "RegDateAttWidget";
  }
}

/*
 * 属性组件功能 <br/>
 */
class RegDateAttWidgetState extends AttWidgetState<RegDateAttWidget>
{
  /*
   * 初始化数据输入值
   * @method
   * @param   {Fld}       flds              所有字段信息
   */
  void initFlds(GlobalKey key, Fld fld)
  {
    if (fld.getColname() == 'xgrq')
    { // 修改日期
      mChildrenAttItem[key] = new AttItemWidget(fld, mValue: DateUtil.getNowDateStr(), key: key);
    }
    else if (fld.getColname() == 'cjrq')
    { // 采集日期
      mChildrenAttItem[key] = new AttItemWidget(fld, mValue: DateUtil.getNowDateStr(), key: key);
    }
    else if (fld.getColname() == 'cjr')
    { // 采集人
      mChildrenAttItem[key] = new AttItemWidget(fld, mValue: mAttWidgetValues[fld.getColname()], key: key);
    }
    else if (fld.getColname() == 'qymc')
    { // 区县分公司
      mChildrenAttItem[key] =
      new AttItemWidget(fld, mValue: mAttWidgetValues[fld.getColname()], key: key, mOnChanged: (value)
      {},);
    }
    else if (fld.getColname() == 'fwq')
    { // 营服中心
      mChildrenAttItem[key] =
      new AttItemWidget(fld, mValue: mAttWidgetValues[fld.getColname()], key: key, mOnChanged: (value)
      {},);
    }
    else if (fld.getColname() == 'wgmc')
    { // 网格单元
      mChildrenAttItem[key] =
      new AttItemWidget(fld, mValue: mAttWidgetValues[fld.getColname()], key: key, mOnChanged: (value)
      {},);
    }
    else
    {
      mChildrenAttItem[key] = new AttItemWidget(fld, mValue: mAttWidgetValues[fld.getColname()], key: key);
    }
  }

  /*
   * 计算属性修改
   * @param total 价格
   */
  void calcAttOnChanged(String total, String colname)
  {
    if (!CxTextUtil.isEmpty(total))
    {
      List<GlobalKey<State<StatefulWidget>>> keys = mChildrenAttItem.keys.toList();
      List<Widget> values = mChildrenAttItem.values.toList();
      for (int i = 0; i < keys.length; i ++)
      {
        GlobalKey<State<StatefulWidget>> key = keys[i];
        Widget widgetChild = values[i];
        if (widgetChild is AttItemWidget)
        {
          if (widgetChild.mFld.getColname() == colname)
          {
            if (key.currentState is AttItemWidgetState)
            {
              AttItemWidgetState state = key.currentState;
              state.setState(()
              {
                state.mController.text = total;
              });
              break;
            }
          }
        }
      }
    }
  }
}
