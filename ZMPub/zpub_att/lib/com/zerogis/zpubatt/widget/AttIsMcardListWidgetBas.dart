import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttFjPlugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:weui/weui.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';

/*
 * 带有ismcard列表的组件的页面基类<br/>
 * 需要传入的键：<br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 *
 */
class AttIsMcardListWidgetBas extends WidgetStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> mInitPara;

  AttIsMcardListWidgetBas({Key key, this.mInitPara, plugin}) :super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new AttIsMcardListWidgetBasState();
  }

  static String toStrings()
  {
    return "AttIsMcardListWidgetBas";
  }
}

/*
 * 页面功能 <br/>
 */
class AttIsMcardListWidgetBasState<T extends AttIsMcardListWidgetBas> extends WidgetBaseState<T>
{
  /*
   * 查询过来的ismcard列表集合
   */
  List mList = new List();

  void initState()
  {
    super.initState();
    this.query(1);
  }

  Widget build(BuildContext context)
  {
    return mEmptyView;
  }

  void query(int page)
  {
    if(page == DigitValueConstant.APP_DIGIT_VALUE_1)
    {
      mList.clear();
    }
  }

  void dealQuery(Object values)
  {
    if (values is Map)
    {
      setState(()
      {
        if(!CxTextUtil.isEmptyList(values[BasMapKeyConstant.MAP_KEY_DATA]))
        {
          mList.addAll(values[BasMapKeyConstant.MAP_KEY_DATA]);
        }
        else
        {
          mList.add(values);
        }
        mEmptyView = SingleChildScrollView(child: Column(children: createBodyWidget()),);
      });
    }
  }

  /*
   * 创建内容
   */
  List<Widget> createBodyWidget()
  {
    List<Widget> widgets = new List();
    Plugin plugin = new Plugin();
    mList.forEach((item)
    {
      widgets.add(Stack(children: <Widget>[
        GestureDetector(child: AttIsMCardWidget(widget.mInitPara[DBFldConstant.FLD_MAJOR],
          widget.mInitPara[DBFldConstant.FLD_MINOR], attVals: item, plugin: plugin,), onTap: ()
        {
          runPluginParam(AttFjPlugin.toStrings(), item);
        },),
      ],));
    });
    return widgets;
  }

  /*
   * 根据名称启动插件且携带参数
   */
  @override
  Future<T> runPluginParam<T extends Object>(String pluginName, dynamic att)
  {
    mChildUniversalInitPara[DBFldConstant.FLD_MAJOR] = widget.mInitPara[DBFldConstant.FLD_MAJOR];
    mChildUniversalInitPara[DBFldConstant.FLD_MINOR] = widget.mInitPara[DBFldConstant.FLD_MINOR];
    mChildUniversalInitPara[DBFldConstant.FLD_ID] = att[DBFldConstant.FLD_ID];
    mChildUniversalInitPara[BasMapKeyConstant.MAP_KEY_ATT] = att;
    mChildUniversalInitPara[AttConstant.ATT_STATE] = AttFldValueConstant.ATT_STATE_EDIT_ABLE;
    return super.runPluginName(pluginName);
  }
}
