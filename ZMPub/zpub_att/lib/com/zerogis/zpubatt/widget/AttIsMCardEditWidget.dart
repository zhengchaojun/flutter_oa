import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';

/*
 * 根据主子类型生成属性查看组件:不传ID则不请求网络生成IsMcard：例子：综合查询底部<br/>
 * 和AttWidget区别？
 * 1：是ismcard的属性
 * 2：AttIsMCardEditWidget全部允许编辑
 * 3：所有条目都必填项去掉
 * 4：重置条目数据重新调用一遍refresh方法
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 选传} plugin 插件对象
 * @param {Object 选传} attState 属性状态
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class AttIsMCardEditWidget extends AttWidget
{
  AttIsMCardEditWidget(major, minor,
      {Key key, id = -1, attVals, plugin, ValueChanged<dynamic> mValueChangedMethod, attState: AttFldValueConstant
          .ATT_STATE_NEW_EDIT,})
      : super(
      major, minor, key: key,
      mId: id,
      mAttVals: attVals,
      mValueChangedMethod: mValueChangedMethod,
      attState: attState,
      plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new AttIsMCardEditWidgetState();
  }

  static String toStrings()
  {
    return "AttIsMCardEditWidget";
  }
}

/*
 * 属性组件功能 <br/>
 */
class AttIsMCardEditWidgetState<T extends AttIsMCardEditWidget> extends AttWidgetState<T>
{
  /*
   * 是否过滤fld的条目
   * @param fld listview中一个条目生成的item值
   * return [false=过滤]
   */
  bool filterFldItem(Fld fld)
  {
    if (fld.getIsmcard() == DigitValueConstant.APP_DIGIT_VALUE_1 &&
        fld.getDisporder() > DigitValueConstant.APP_DIGIT_VALUE_0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /*
   * 编辑Fld字段的Editable值
   * @method
   * @param   {Fld}       flds              所有字段信息
   */
  void editableFld(Fld fld)
  {
    fld.setEditable(AttFldValueConstant.ATT_STATE_DEFAULT);
    fld.setNullable(AttFldValueConstant.ATT_STATE_DEFAULT);
  }

  /*
   * 刷新数据
   * @param {Object 必传} map 对象
   *
   * @param {Object 必传} major 主类型
   * @param {Object 必传} minor 子类型
   * @param {Object 选传} plugin 插件对象
   * @param {Object 选传} attState 属性状态
   */
  void refresh(Map<String, dynamic> map)
  {
    widget.mMajor = map[DBFldConstant.FLD_MAJOR] ?? widget.mMajor;
    widget.mMinor = map[DBFldConstant.FLD_MINOR] ?? widget.mMinor;
    initData();
  }
}
