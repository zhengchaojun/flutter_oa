import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/core/AttWidgetCreator.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasMapKeyConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/FldValue.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/FldValuesManager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/FldValuesManagerConstant.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

/*
 * 属性条目生成组件(条目兼容文本，日期，日期+时间，spinner，富文本） <br/>
 * @param {Object 必传} fld  fld对象生成条目对象
 * @param {Object 选传} value fld对应的value值
 * @param {Object 选传} onChanged  对应条目改变值的时候回调
 * @param {Object 选传} key 属性对象
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class AttItemWidget extends StatefulWidget
{
  /*
   * 生成条目的依据
   */
  Fld mFld;

  /*
   * 属性中的值
   */
  dynamic mValue;

  /*
   * 值改变回调
   */
  ValueChanged<dynamic> mOnChanged;

  AttItemWidget(this.mFld, {this.mValue, Key key, this.mOnChanged}) : super(key: key);

  State<StatefulWidget> createState()
  {
    return new AttItemWidgetState();
  }

  static String toStrings()
  {
    return "AttItemWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class AttItemWidgetState extends State<AttItemWidget>
{
  /*
   * 条目左边的文本
   */
  String mSText;

  /*
   * 输入框
   */
  TextEditingController mController;

  /*
   * 下拉菜单值
   */
  List<DropdownMenuItem> mDropdownMenuList;

  /*
   * 下拉菜单默认选择的值
   */
  dynamic mDropdownSelectMenuItem;

  /*
   * 动态参数按钮fldvalue表despc的值
   */
  dynamic mDynamicBtnDispc;

  /*
   * 当前条目是否可以编辑[true=可以编辑]
   */
  bool mEditable;

  void initState()
  {
    super.initState();
    mSText = widget.mFld.getNamec();
    mEditable = widget.mFld.getEditable() == DigitValueConstant.APP_DIGIT_VALUE_1 ? true : false;
    if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_1 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_3 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_4 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_10 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_12)
    {
      // 文本
      String defval = widget.mFld.getDefval();
      mController = new TextEditingController();
      if (!CxTextUtil.isEmptyObject(widget.mValue))
      {
        mController.text = widget.mValue.toString();
      }
      else if (!CxTextUtil.isEmptyObject(defval) && !defval.contains(BasStringValueConstant.STR_VALUE_DOOLAR))
      {
        mController.text = widget.mFld.getDefval();
      }
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_2)
    {
      // 下拉框
      initDropdownMenuList();
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_5)
    {
      // 文本
      String defval = widget.mFld.getDefval();
      mController = new TextEditingController();
      if (!CxTextUtil.isEmptyObject(widget.mValue))
      {
        widget.mValue = DateUtil.getDateStrByTimeStr(widget.mValue.toString());
        mController.text = widget.mValue;
      }
      else if (!CxTextUtil.isEmptyObject(defval) && !defval.contains(BasStringValueConstant.STR_VALUE_DOOLAR))
      {
        mController.text = widget.mFld.getDefval();
      }
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_6)
    {
      // 下拉(可改)
      mController = new TextEditingController();
      mController.text = widget.mValue ?? "";
      mDropdownSelectMenuItem = queryFldValueGetSpinnerDataSort();
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_8)
    {
      // 下拉框
      mDropdownMenuList = new List();
      mDropdownSelectMenuItem = widget.mValue ?? mSText;
      mDropdownMenuList.add(DropdownMenuItem(
          child: Text(widget.mValue ?? mSText),
          value: widget.mValue ?? mSText));
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_9 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_11 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_7)
    {
      // 下拉框
      mController = new TextEditingController();
      if (!CxTextUtil.isEmptyObject(widget.mValue))
      {
        mController.text = widget.mValue.toString();
      }
      initPluginParam();
    }
  }

  Widget build(BuildContext context)
  {
    Widget body;
    if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_1)
    {
      // 文本
      body = createText();
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_2)
    {
      // 下拉框
      body = createDropdownButton();
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_3)
    {
      // 日期
      body = AttWidgetCreator.createCommonDataPick(this, mSText,
          enabled: mEditable,
          controllerText: mController, nullable: widget.mFld.getNullable(), onChanged: widget.mOnChanged);
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_4)
    {
      // 富文本
      body = AttWidgetCreator.createCommonStrechTextField(
          mSText, nullable: widget.mFld.getNullable());
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_5)
    {
      // 日期+时间
      body = AttWidgetCreator.createCommonDataPick(this, mSText,
          controllerText: mController,
          enabled: mEditable,
          disptype: widget.mFld.disptype,
          nullable: widget.mFld.getNullable(),
          onChanged: widget.mOnChanged);
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_6)
    {
      // 下拉(可改)
      body = AttWidgetCreator.createCommonDropDownModify(this, mSText,
        controller: mController,
        options: mDropdownSelectMenuItem,
        enabled: mEditable,
        isnum: widget.mFld.getIsnum(),
        nullable: widget.mFld.getNullable(),
        disprows: widget.mFld.getDisprows(),
        maxval: widget.mFld.getMaxval(),
        minval: widget.mFld.getMinval(),
      );
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_7)
    {
      // 自定义按钮
      body = AttWidgetCreator.createCommonValButton(this,
          mSText, controllerText: mController,
          dispc: mDynamicBtnDispc,
          editable: mEditable,
          nullable: widget.mFld.getNullable(),
          onChanged: widget.mOnChanged);
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_8)
    {
      // 选择操作人的部门或职位
      body = createDropdownButton();
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_9)
    {
      // 动态选择
      body = AttWidgetCreator.createCommonDynamicButton(
          this, mSText, controllerText: mController,
          nullable: widget.mFld.getNullable(), dispc: mDynamicBtnDispc);
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_10)
    {
      // 高亮文本
      body = createText(color: Colors.red);
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_11)
    {
      // 自动编码
      body = AttWidgetCreator.createAutoCode(mSText,
          controller: mController,
          enabled: mEditable,
          isnum: widget.mFld.getIsnum(),
          nullable: widget.mFld.getNullable(),
          disprows: widget.mFld.getDisprows(),
          onChanged: widget.mOnChanged,
          dispc: mDynamicBtnDispc);
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_12)
    {
      // 时间(没有日期)
      body = AttWidgetCreator.createCommonDataPick(this, mSText,
          controllerText: mController,
          disptype: widget.mFld.disptype,
          nullable: widget.mFld.getNullable());
    }
    else
    {
      int disptype = widget.mFld.disptype;
      body = new Text("暂不支持此功能:disptype为{$disptype}");
    }
    return body;
  }

  /*
   * 初始化下拉菜单集合
   */
  void initDropdownMenuList()
  {
    mDropdownMenuList = new List();
    List<FldValue> result = queryFldValueGetSpinnerDataSort();
    result.forEach((fldvalue)
    {
      if (widget.mValue == null)
      {
        mDropdownSelectMenuItem = result.first;
      }
      else if (widget.mValue.toString() == fldvalue.getDbvalue().toString())
      {
        mDropdownSelectMenuItem = fldvalue;
      }
      mDropdownMenuList.add(
          DropdownMenuItem(child: Text(fldvalue.getDispc()), value: fldvalue));
    });
  }

  /*
   * 根据：表名和列名(tabName，colname) 查询到fldvalue集合(根据DbValue进行排好序的fldvalue集合)
   */
  List<FldValue> queryFldValueGetSpinnerDataSort()
  {
    FldValuesManagerConstant fldValuesManagerConstant = FldValuesManager
        .getInstance();
    return fldValuesManagerConstant.queryFldValueGetSpinnerDataSort(
        widget.mFld.getTabname(), widget.mFld.getColname());
  }

  /*
   * 初始化下拉菜单集合
   */
  void initPluginParam()
  {
    FldValuesManagerConstant fldValuesManagerConstant = FldValuesManager.getInstance();
    mDynamicBtnDispc = fldValuesManagerConstant.queryFldValueDispc(
        widget.mFld.getTabname(), widget.mFld.getColname(),
        AttFldValueConstant.FLDVALUE_MS);
  }

  /*
   * 创建公有文本
   */
  Widget createText({Color color})
  {
    return AttWidgetCreator.createCommonText(mSText,
      controller: mController,
      color: color,
      enabled: mEditable,
      isnum: widget.mFld.getIsnum(),
      nullable: widget.mFld.getNullable(),
      disprows: widget.mFld.getDisprows(),
      maxval: widget.mFld.getMaxval(),
      minval: widget.mFld.getMinval(),
      onChanged: widget.mOnChanged,
    );
  }

  /*
   * 创建公有下拉菜单
   */
  Widget createDropdownButton()
  {
    return AttWidgetCreator.createCommonDropdownButton(mSText,
        value: mDropdownSelectMenuItem,
        items: mDropdownMenuList,
        enabled: mEditable,
        valueChangedMethod: (fldvalue)
        {
          setState(()
          {
            mDropdownSelectMenuItem = fldvalue;
            if (widget.mOnChanged != null)
            {
              widget.mFld.tag = fldvalue;
              widget.mOnChanged(widget.mFld);
            }
          });
        },
        nullable: widget.mFld.getNullable());
  }

  /*
   * 查询当前条目的值
   * @retrun result = {fld: Instance of 'Fld', value: 测试}
   */
  Map queryItemKeyValue()
  {
    Map<String, dynamic> map = {};
    map[BasMapKeyConstant.MAP_KEY_FLD] = widget.mFld;
    if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_1 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_5 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_4 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_6 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_10 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_12 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_3)
    {
      // 文本
      map[BasMapKeyConstant.MAP_KEY_VALUE] = mController.text;
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_2)
    {
      // 下拉框
      map[BasMapKeyConstant.MAP_KEY_VALUE] = mDropdownSelectMenuItem;
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_8)
    {
      // 选择操作人的部门或职位
      map[BasMapKeyConstant.MAP_KEY_VALUE] = widget.mValue;
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_9 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_11 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_7)
    {
      // 动态选择
      map[BasMapKeyConstant.MAP_KEY_VALUE] = mController.text;
      map[BasMapKeyConstant.MAP_KEY_RESULT] = mDynamicBtnDispc;
    }
    return map;
  }

  /*
   * 查询当前条目的值
   * @retrun result = {fld: Instance of 'Fld', value: 测试, _exp: 'code like ?', types: 's', vals: '%1%'}
   */
  Map<String, dynamic> queryItemKeyExp()
  {
    Map<String, dynamic> map = queryItemKeyValue();

    // 校验
    if (CxTextUtil.isEmptyObject(map[BasMapKeyConstant.MAP_KEY_VALUE]))
    {
      return map;
    }

    //查询条件
    map[HttpParamKeyValue.PARAM_KEY_EXP] = widget.mFld.getColname() + DBExpConstant.EXP_LIKE_Q;
    if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_1 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_4 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_6 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_7 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_8 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_9 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_10 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_11)
    {// 普通文本
      bool isnum = widget.mFld.getIsnum() == DigitValueConstant.APP_DIGIT_VALUE_1;
      map[HttpParamKeyValue.PARAM_KEY_TYPES] = isnum ? DBExpTypesConstant.EXP_TYPES_I : DBExpTypesConstant.EXP_TYPES_S;
      map[HttpParamKeyValue.PARAM_KEY_VALS] =
      isnum ? map[BasMapKeyConstant.MAP_KEY_VALUE] : DBExpConstant.EXP_B + map[BasMapKeyConstant.MAP_KEY_VALUE] +
          DBExpConstant.EXP_B;
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_2)
    {// 下拉菜单
      bool isnum = widget.mFld.getIsnum() == DigitValueConstant.APP_DIGIT_VALUE_1;
      FldValue fldValue = map[BasMapKeyConstant.MAP_KEY_VALUE];
      map[HttpParamKeyValue.PARAM_KEY_TYPES] = isnum ? DBExpTypesConstant.EXP_TYPES_I : DBExpTypesConstant.EXP_TYPES_S;
      map[HttpParamKeyValue.PARAM_KEY_VALS] = isnum ? fldValue.getDispc() : DBExpConstant.EXP_B + fldValue.getDispc() + DBExpConstant.EXP_B;
    }
    else if (widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_3 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_5 ||
        widget.mFld.disptype == DigitValueConstant.APP_DIGIT_VALUE_12)
    {// 日期
      map[HttpParamKeyValue.PARAM_KEY_EXP] = widget.mFld.getColname() + DBExpConstant.EXP_EQUAL_Q;
      map[HttpParamKeyValue.PARAM_KEY_TYPES] = DBExpTypesConstant.EXP_TYPES_D;
      map[HttpParamKeyValue.PARAM_KEY_VALS] = map[BasMapKeyConstant.MAP_KEY_VALUE];
    }
    else
    {// 其他类型
      map[HttpParamKeyValue.PARAM_KEY_TYPES] = DBExpTypesConstant.EXP_TYPES_S;
      map[HttpParamKeyValue.PARAM_KEY_VALS] =
          DBExpConstant.EXP_B + map[BasMapKeyConstant.MAP_KEY_VALUE] + DBExpConstant.EXP_B;
    }
    return map;
  }
}
