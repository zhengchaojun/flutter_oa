import 'package:json_annotation/json_annotation.dart';
import 'package:zpub_att/com/zerogis/zpubatt/bean/template/att/Elform.dart';

part 'TemplateAtt.g.dart';

/*
 * 类描述：
 * 作者：郑朝军 on 2019/6/13
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/13
 * 修改备注：
 */
@JsonSerializable()
class TemplateAtt
{
  @JsonKey(name: 'el-form')
  Elform elform;

  TemplateAtt()
  {}

  factory TemplateAtt.fromJson(Map<String, dynamic> srcJson) =>
      _$TemplateAttFromJson(srcJson);

  Map<String, dynamic> toJson()
  => _$TemplateAttToJson(this);
}
