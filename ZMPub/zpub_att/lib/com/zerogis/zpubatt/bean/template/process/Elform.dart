
import 'package:json_annotation/json_annotation.dart';
import 'package:zpub_att/com/zerogis/zpubatt/bean/template/process/Div.dart';

part 'Elform.g.dart';

/*
 * 类描述：
 * 作者：郑朝军 on 2019/6/13
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/13
 * 修改备注：
 */
@JsonSerializable()
class Elform
{
  @JsonKey(name: 'model')
  String model;

  @JsonKey(name: 'rules')
  String rules;

  @JsonKey(name: 'class')
  String clazz;

  @JsonKey(name: 'label-position')
  String labelposition;

  @JsonKey(name: 'label-width')
  String labelwidth;

  @JsonKey(name: 'size')
  String size;

  @JsonKey(name: 'cx-custfld')
  Object cxcustfld;

  @JsonKey(name: 'div')
  Div div;

  Elform()
  {}

  factory Elform.fromJson(Map<String, dynamic> srcJson) =>
      _$ElformFromJson(srcJson);

  Map<String, dynamic> toJson()
  => _$ElformToJson(this);
}
