// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Cfg.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cfg _$CfgFromJson(Map<String, dynamic> json) {
  return Cfg()
    ..colname = json['colname'] as String
    ..title = json['title'] as String
    ..type = json['type'] as String
    ..rows = json['rows'] as String
    ..editable = json['editable'] as String
    ..value = json['value'] as String
    ..required = json['required'] as String;
}

Map<String, dynamic> _$CfgToJson(Cfg instance) => <String, dynamic>{
      'colname': instance.colname,
      'title': instance.title,
      'type': instance.type,
      'rows': instance.rows,
      'editable': instance.editable,
      'value': instance.value,
      'required': instance.required
    };
