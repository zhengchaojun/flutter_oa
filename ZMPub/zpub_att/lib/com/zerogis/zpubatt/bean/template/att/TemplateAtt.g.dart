// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TemplateAtt.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TemplateAtt _$TemplateAttFromJson(Map<String, dynamic> json) {
  return TemplateAtt()
    ..elform = json['el-form'] == null
        ? null
        : Elform.fromJson(json['el-form'] as Map<String, dynamic>);
}

Map<String, dynamic> _$TemplateAttToJson(TemplateAtt instance) =>
    <String, dynamic>{'el-form': instance.elform};
