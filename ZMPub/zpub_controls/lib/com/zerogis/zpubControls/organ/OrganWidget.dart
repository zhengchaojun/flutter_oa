import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_ui/zpub_ui.dart';

/*
 * 部门树组件 <br/>
 * @param {Object 必传} valueChangedMethod   点击事件
 * @param {Object 必传} treeHeight           树控件高度:默认值为屏幕高度的3/20
 * @param {Object 必传} treeData             树控件数据:[{'id':1,'glid':2,'childern':[{'id':1,'glid':1}]}]
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class OrganWidget extends WidgetStatefulBase
{
  /*
   * 点击事件
   */
  ValueChanged<dynamic> valueChangedMethod;

  /*
   * 树控件高度:默认值为屏幕高度的3/20
   */
  double treeHeight;

  /*
   * 树控件数据:[{'id':1,'glid':2,'childern':[{'id':1,'glid':1}]}]
   */
  List<dynamic> treeData;

  OrganWidget({Key key, plugin, @required this.valueChangedMethod, this.treeHeight, this.treeData})
      : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new OrganWidgetState();
  }

  static String toStrings()
  {
    return "OrganWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class OrganWidgetState extends WidgetBaseState<OrganWidget>
{
  void initState()
  {
    super.initState();
    initTreeHeight();
    query();
  }

  Widget build(BuildContext context)
  {
    return createTree();
  }

  @override
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryOrgan")
    {
      TreeService.queryUnionUserOrg(values, this);
    }
    else if (method == "queryUnionUserOrg")
    {
      setState(()
      {
        widget.treeData = values;
      });
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  /*
   * 初始化tree控件高度
   */
  void initTreeHeight()
  {
    if (widget.treeHeight == null)
    {
      widget.treeHeight = ScreenUtil.getInstance().getScreenHeight() * 0.2;
    }
  }

  /*
   * 创建树
   */
  Widget createTree()
  {
    if (CxTextUtil.isEmptyList(widget.treeData))
    {
      return mEmptyView;
    }
    else
    {
      return Tree(widget.treeData, onTap: (node)
      {
        node.mObject['context'] = context;
        widget.valueChangedMethod(node);
      }, height: widget.treeHeight, mTextSize: 18,);
    }
  }

  /*
   * 查询树数据
   */
  void query()
  {
    if (CxTextUtil.isEmptyList(widget.treeData))
    {
      TreeService.queryOrgan(this);
    }
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class OrganWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new OrganWidget(
      key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY],
      valueChangedMethod: initPara[BasMapKeyConstant
          .MAP_KEY_VALUE_CHANGE_METHOD], treeHeight: initPara['treeHeight'], treeData: initPara['treeData'],);
  }
}
