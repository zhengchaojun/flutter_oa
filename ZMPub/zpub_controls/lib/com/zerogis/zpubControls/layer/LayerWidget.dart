import 'package:flutter/material.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/core/UiWidgetCreator.dart';

/*
 * 图层组件(layer表) <br/>
 * @param {Object 必传} onClick   按钮点击事件
 * @param {Object 选传} width   按钮点击事件
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class LayerWidget extends WidgetStatefulBase
{
  /*
   * 按钮点击事件
   */
  ValueChanged<dynamic> onClick;

  /*
   * 图层宽度
   */
  double width;

  LayerWidget({Key key, plugin, this.onClick, this.width}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new LayerWidgetState();
  }

  static String toStrings()
  {
    return "LayerWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class LayerWidgetState extends WidgetBaseState<LayerWidget>
{
  /*
   * 下拉菜单默认选择的值
   */
  LayerManagerConstant mLayerManager;

  /*
   * 所有图层
   */
  List mLayers;

  void initState()
  {
    super.initState();
    mLayerManager = LayerManager.getInstance();
    mLayers = mLayerManager.queryChild('glid', 0);
  }

  Widget build(BuildContext context)
  {
    return Container(child: Column(children: createLayer(),), width: widget.width ?? MediaQuery
        .of(context)
        .size
        .width * 0.8);
  }

  /*
   * 创建所有菜单
   */
  List<Widget> createLayer()
  {
    List<Widget> list = <Widget>[];
    mLayers.forEach((value)
    {
      List child = mLayerManager.queryChild('glid', value['layer']);
      list.add(new Column(children: <Widget>[
        Text(
            value[DBFldConstant.FLD_NAMEC],
            textAlign: TextAlign.start,
            style: BasTextStyleRes.text_color_text1_larger_fontw900),
        SizedBox(height: 5,),
        createMenuGridView(child)
      ],));
    });
    return list;
  }

  /*
   * 创建一个菜单中的所有条目
   */
  Widget createMenuGridView(List<dynamic> menu)
  {
    List<Widget> list = <Widget>[];
    menu.forEach((value)
    {
      list.add(Padding(child: UiWidgetCreator.createCommonTopImgText2(
          value, param: {BasMapKeyConstant.MAP_KEY_WIDTH: 15.0, BasMapKeyConstant.MAP_KEY_HEIGHT: 15.0}, onTap: ()
      {
        if (widget.onClick != null)
        {
          widget.onClick(value);
        }
      }), padding: EdgeInsets.symmetric(horizontal: 10),));
    });

    return SingleChildScrollView(child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: list),
        scrollDirection: Axis.horizontal);
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class LayerWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new LayerWidget();
  }
}
