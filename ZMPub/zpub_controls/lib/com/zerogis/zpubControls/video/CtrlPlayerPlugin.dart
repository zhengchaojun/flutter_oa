import 'package:flutter/material.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_ui/zpub_ui.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'CtrlPlayerPlugin', 'CtrlPlayerPlugin', '视频播放', 1, 4, 1, 'package:repairordershlt/com/zerogis/repairordershlt/scene', 'CtrlPlayerPlugin',  '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

void main()
{
  runApp(new CtrlPlayerPlugin());
}

/*
 * 视频播放页 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CtrlPlayerPlugin extends PluginStatefulBase
{
  /*
   * 初始化参数{path:'http://119.3.70.172:8082/gissygr/temp/0/3/0/373.mp4'}
   */
  Map<String, dynamic> initPara;

  CtrlPlayerPlugin({Key key, plugin,this.initPara}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new CtrlPlayerPluginState();
  }

  static String toStrings()
  {
    return "CtrlPlayerPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class CtrlPlayerPluginState extends PluginBaseState<CtrlPlayerPlugin>
{
  void initState()
  {
    super.initState();
    widget.initPara['height'] = ScreenUtil.getInstance().getScreenHeight();
    widget.initPara['width'] = ScreenUtil.getInstance().getScreenWidth();
  }


  Widget build(BuildContext context)
  {
    return buildBody(context, ListPlayer(initParam: widget.initPara,));
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class CtrlPlayerPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new CtrlPlayerPlugin(initPara: initPara), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new CtrlPlayerPlugin();
  }
}
