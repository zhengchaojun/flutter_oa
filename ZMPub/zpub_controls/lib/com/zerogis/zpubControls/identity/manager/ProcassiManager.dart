import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';

/*
 * 类描述：流程人员管理类
 * 作者：郑朝军 on 2019/9/3
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/9/3
 * 修改备注：
 */
class ProcassiManager
{
  static ProcassiManager mInstance;

  List<dynamic> mProcassi;

  static ProcassiManager getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new ProcassiManager();
    }
    return mInstance;
  }

  ProcassiManager()
  {
    mProcassi = InitSvrMethod.getInitSvrMap()["procassi"];
  }

  /*
   * 查询流程人员条目
   */
  Map<String, dynamic> query(String name, String fld1, String val1)
  {
    Map<String, dynamic> map;
    mProcassi.forEach((value)
    {
      List<String> split = (value["val1"] as String).split(BasStringValueConstant.STR_COMMON_SEMICOLON);
      bool val = false;
      split.forEach((item)
      {
        if (item == val1)
        {
          val = true;
        }
      });
      if (name == value["name"] && fld1 == value["fld1"] && val)
      {
        map = value;
      }
    });
    return map;
  }
}
