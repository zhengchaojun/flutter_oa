import 'package:flutter/material.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/identity/manager/ProcassiManager.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/bean/UserDep.dart';

/*
 * 部门树组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class IdentitySvrWidget extends WidgetStatefulBase
{
  ValueChanged<UserDep> valueChangedMethod;

  Map<String, dynamic> mInitPara;

  IdentitySvrWidget({Key key, plugin, @required this.valueChangedMethod, this.mInitPara})
      : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new IdentitySvrWidgetState();
  }

  static String toStrings()
  {
    return "IdentitySvrWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class IdentitySvrWidgetState<T extends IdentitySvrWidget> extends WidgetBaseState<T>
{
  List<UserDep> mTree = <UserDep>[];

  String mCommonNoData = BasStringRes.progressbar_text;

  IdentitySvrWidgetState()
  {}

  void initState()
  {
    super.initState();
    query();
  }

  Widget build(BuildContext context)
  {
    return createTree();
  }

  /*
   * 创建树
   */
  Widget createTree()
  {
    if (CxTextUtil.isEmptyList(mTree))
    {
      return SysWidgetCreator.createCommonNoData(text: mCommonNoData);
    }
    else
    {
      return new Column(
        children: createGridViewItem(),);
    }
  }

  /*
   * 创建GridView条目
   */
  List<Widget> createGridViewItem()
  {
    List<Widget> listWidget = <Widget>[];
    mTree.forEach((item)
    {
      listWidget.add(new GestureDetector(onTap: ()
      {
        showToast("已选择:" + item.getName());
        widget.valueChangedMethod(item);
        Navigator.pop(context);
      }, child: new Padding(padding: EdgeInsets.all(
          MarginPaddingHeightConstant.APP_MARGIN_PADDING_8),
        child: new Text(item.getName()),),));
    });

    return listWidget;
  }

  void query()
  {
    IdentitySvrService.memberOfGroup(widget.mInitPara["assigneeParams"], this);
  }

  void queryOld()
  {
    ProcassiManager procassiManager = ProcassiManager.getInstance();
    Map<String, dynamic> reslut = procassiManager.query(
        widget.mInitPara["assigneeParams"], "position", widget.mInitPara["position"]);
    if (reslut["sel"] == 'group')
    {
      IdentitySvrService.memberOfGroup(reslut["params"], this);
    }
    else
    {
      IdentitySvrService.memberOfOrganPos(widget.mInitPara["depart"], reslut["params"], this);
    }
  }

  @override
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "memberOfGroup" || method == "memberOfOrganPos")
    {
      setState(()
      {
        mTree = values;
      });
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  void onNetWorkFaild(String method, Object values)
  {
    if (method == "memberOfGroup" || method == "memberOfOrganPos" && values == MessageConstant.MSG_SERVER_ENMPTY)
    {
      setState(()
      {
        mCommonNoData = BasStringRes.no_data;
      });
    }
    else
    {
      super.onNetWorkFaild(method, values);
    }
  }
}

/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class IdentitySvrWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new IdentitySvrWidget(
      key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY],
      valueChangedMethod: initPara[BasMapKeyConstant.MAP_KEY_VALUE_CHANGE_METHOD],
      mInitPara: initPara[BasMapKeyConstant.MAP_KEY_DATA],);
  }
}
