import 'package:flutter/material.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/step/constant/StepBPMConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_ui/zpub_ui.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';

/*
 * 消息步进组件 <br/>
 * @param {Object 必传} comments   步进的内容  comments=[{'message':'334','time':'334','userid':2}]
 * @param {Object 选传} id         意见ID
 * @param {Object 选传} mValueChangedMethod    查询回调
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class MessageStepperWidget extends WidgetStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> mInitPara;

  /*
   * 查询属性数据回调方法
   */
  ValueChanged<dynamic> mValueChangedMethod;

  MessageStepperWidget({this.mInitPara, Key key, plugin, this.mValueChangedMethod})
      : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new MessageStepperWidgetState();
  }

  static String toStrings()
  {
    return "MessageStepperWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class MessageStepperWidgetState extends WidgetBaseState<MessageStepperWidget>
{
  List<StepCus> mStepCus = <StepCus>[];

  void initState()
  {
    super.initState();
    initStepperCus();
    query();
  }

  Widget build(BuildContext context)
  {
    return CxTextUtil.isEmptyList(mStepCus) ? new Container(width: 0, height: 0,) : new StepperCus(
        physics: new NeverScrollableScrollPhysics(),
        currentStep: 0, steps: mStepCus);
  }

  @override
  void onNetWorkFaild(String method, Object values)
  {
    if (method == "queryHisComment")
    {
      if (values == MessageConstant.MSG_SERVER_ENMPTY)
      {
        if (widget.mValueChangedMethod != null)
        {
          widget.mValueChangedMethod(values);
        }
      }
    }
    else
    {
      super.onNetWorkFaild(method, values);
    }
  }

  @override
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryHisComment")
    {
      if (values is List)
      {
        setState(()
        {
          widget.mInitPara[StepBPMConstant.PAIR_COMMENTS] = values;
          initStepperCus();
        });

        if (widget.mValueChangedMethod != null)
        {
          widget.mValueChangedMethod(values);
        }
      }
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  /*
   * 初始化步进
   */
  void initStepperCus()
  {
    if (CxTextUtil.isEmptyList(widget.mInitPara[StepBPMConstant.PAIR_COMMENTS]))
    {
      return;
    }
    widget.mInitPara[StepBPMConstant.PAIR_COMMENTS].forEach((commentsBean)
    {
      mStepCus.add(createTableChildrenItem(commentsBean));
    });
  }

  /*
   * 需要优化：将来需要设置成可以远程控制
   * 创建表格控件中所有孩子(条目)
   */
  StepCus createTableChildrenItem(dynamic commentsBean)
  {
    String message = commentsBean[BasMapKeyConstant.MAP_KEY_MESSAGE];
    String time = commentsBean[BasMapKeyConstant.MAP_KEY_TIME];
    List<String> result = message.split(
        BasStringValueConstant.STR_COMMON_A_COLON);

    return new StepCus(
      title: new Row(children: <Widget>[
        new Text(result[result.length - 1], style: BasTextStyleRes.text_color_text1_larger,),
        new SizedBox(width: 30),
        new Text('审批人：' + result[0]),
      ],),
      subtitle: new Text(time),
      content: new Text(result[1], style: BasTextStyleRes.text_color_text1_larger,),
      state: StepStateCus.complete,
      isActive: true,
    );
  }

  /*
   * 查询历史公共建议
   */
  void query({int id})
  {
    if (id != null)
    {
      widget.mInitPara[BasMapKeyConstant.MAP_KEY_ID] = id;
    }
    if (widget.mInitPara[BasMapKeyConstant.MAP_KEY_ID] != DigitValueConstant.APP_DIGIT_VALUE__1)
    {
      Map<String, Object> param = {
        BasMapKeyConstant.MAP_KEY_ID: id != null ? id : widget.mInitPara[BasMapKeyConstant.MAP_KEY_ID]
      };
      SvrBpmService.queryHisComment(this, param: param);
    }
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class MessageStepperWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new MessageStepperWidget(mInitPara: initPara);
  }
}
