import 'package:flutter/material.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/step/constant/StepBPMConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_ui/zpub_ui.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';

/*
 * 消息步进组件 <br/>
 * @param {Object 必传} comments   步进的内容  comments=[{'message':'生态林修复工程上传','time':'2020-07-08','subtitle':'测试.mp4'}]
 * @param {Object 选传} id         意见ID
 * @param {Object 选传} mValueChangedMethod    查询回调
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CtrlStepper2 extends WidgetStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> mInitPara;

  /*
   * 查询属性数据回调方法
   */
  ValueChanged<dynamic> mValueChangedMethod;

  CtrlStepper2({this.mInitPara, Key key, plugin, this.mValueChangedMethod})
      : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new CtrlStepper2State();
  }

  static String toStrings()
  {
    return "CtrlStepper2";
  }
}

/*
 * 组件功能 <br/>
 */
class CtrlStepper2State extends WidgetBaseState<CtrlStepper2>
{
  //是否加载全部
  bool isAll = false;

  Widget build(BuildContext context)
  {
    return createStepperCus();
  }

  /*
   * 初始化步进
   */
  Widget createStepperCus()
  {
    if (CxTextUtil.isEmptyList(widget.mInitPara[StepBPMConstant.PAIR_COMMENTS]))
    {
      return mEmptyView;
    }

    // 生成组件
    List<StepCus> stepCus = [];
    widget.mInitPara[StepBPMConstant.PAIR_COMMENTS].forEach((commentsBean)
    {
      stepCus.add(createTableChildrenItem(commentsBean));
    });

    // 生成全部组件
    Widget stepperCus;
    if(isAll)
    {
      stepperCus = StepperCus(physics: new NeverScrollableScrollPhysics(), currentStep: 0, steps: stepCus, key: key_btn_right,);
      return Column(children: <Widget>[
        stepperCus,
      ],);
    }
    else
    {
      stepperCus = StepperCus(physics: new NeverScrollableScrollPhysics(), currentStep: 0, steps: stepCus.sublist(0, stepCus.length> 10? 10: stepCus.length));
      return Column(children: <Widget>[
        stepperCus,
        GestureDetector(child: Container(
          width: ScreenUtil.getInstance().getScreenWidth()*2,
          child: Padding(child: Center(child: Text(
            '加载全部',
            style: TextStyle(
              fontFamily: 'MicrosoftYaHeiUI',
              color: const Color(0xff8a8f9f),
            ),
            textAlign: TextAlign.left,
          ),),padding: EdgeInsets.symmetric(vertical: 10),),
          decoration: BoxDecoration(
            color: const Color(0xfffcfdff),
            border: Border.all(width: 1.0, color: const Color(0xffd2d2d2)),
          ),
        ),onTap: ()
        {
          setState(()
          {
            isAll = true;
          });
        },)
      ],);
    }
  }

  /*
   * 需要优化：将来需要设置成可以远程控制
   * 创建表格控件中所有孩子(条目)
   */
  StepCus createTableChildrenItem(dynamic commentsBean)
  {
    String message = commentsBean[BasMapKeyConstant.MAP_KEY_MESSAGE];
    String time = commentsBean[BasMapKeyConstant.MAP_KEY_TIME];
    String subtitle = commentsBean[BasMapKeyConstant.MAP_KEY_SUBTITLE];

    return StepCus(
      title: Text(time),
      content: GestureDetector(child: Text.rich(
        TextSpan(
          style: TextStyle(
            fontFamily: 'MicrosoftYaHeiUI',
            fontSize: BasSizeRes.textview_text_small,
            color: const Color(0xff333333),
          ),
          children: [
            TextSpan(
              text: message,
            ),
            TextSpan(
              text: subtitle,
              style: TextStyle(
                color: const Color(0xffff9000),
              ),
            ),
          ],
        ),
        textAlign: TextAlign.left,
      ),onTap: ()
      {
        if (widget.mValueChangedMethod != null)
        {
          widget.mValueChangedMethod(commentsBean);
        }
      },),
      state: StepStateCus.complete,
      isActive: true,
    );
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class CtrlStepper2Service extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new CtrlStepper2(mInitPara: initPara);
  }
}
