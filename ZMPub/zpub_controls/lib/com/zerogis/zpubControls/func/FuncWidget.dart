import 'package:flutter/material.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/func/core/FuncWidgetCreator.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/MenuMethod.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

/*
 * 功能菜单组件(func表) <br/>
 * @param {Object 必传} onClick   按钮点击事件
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class FuncWidget extends WidgetStatefulBase
{
  /*
   * 按钮点击事件
   */
  ValueChanged<dynamic> onClick;

  FuncWidget({Key key, plugin, this.onClick}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new FuncWidgetState();
  }

  static String toStrings()
  {
    return "FuncWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class FuncWidgetState extends WidgetBaseState<FuncWidget>
{
  /*
   * 所有菜单集合
   */
  List<dynamic> mMenu;

  void initState()
  {
    super.initState();
    MenuMethod menu = MenuMethod.getInstance();
    mMenu = menu.queryFunc();
  }

  Widget build(BuildContext context)
  {
    return createMenuGridView(mMenu);
  }

  /*
   * 创建一个菜单中的所有条目
   */
  GridView createMenuGridView(List<dynamic> menu)
  {
    List<Widget> list = <Widget>[];
    menu.forEach((value)
    {
      list.add(FuncWidgetCreator.createCommonTopImgText2(value, width: 50, height: 50, onTap: ()
      {
        if (widget.onClick != null)
        {
          widget.onClick(value);
        }
        else
        {
          runPluginName(value[BasMapKeyConstant.MAP_KEY_PLUGIN]);
        }
      }));
    });
    return GridView.count(
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        crossAxisCount: 4,
        padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
        children: list);
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class FuncWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new FuncWidget();
  }
}
