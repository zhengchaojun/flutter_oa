import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_ui/zpub_ui.dart';

/*
 * 功能：组件库,用于创建常用的组件：类似Android中layout文件中common_progressbar.xml相关
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class FuncWidgetCreator
{
  /*
   * 创建顶部是图标底部文字的组件<br/>
   * @param map 举例：{title:请假,icon:0x123}
   */
  static Widget createCommonTopImgText2(Map<String, dynamic> map,
      {GestureTapCallback onTap, double width: 35, double height: 35})
  {
    return new GestureDetector(
      child: new Column(
        children: <Widget>[
          UiWidgetCreator.createCommonImage(
              "assets/images/" + map[BasMapKeyConstant.MAP_ICON] + ".png",
              width: width,
              height: height,
              fit: BoxFit.fill),
          new Text(map[DBFldConstant.FLD_NAMEC],
              style: BasTextStyleRes.text_color_text1_larger)
        ],
      ),
      onTap: onTap,
    );
  }
}
