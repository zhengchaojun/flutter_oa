import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/charts/base/CtrlChartBas.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/constant/CtrlBigDataKeyConstant.dart';
import 'package:zpub_ui/zpub_ui.dart';
import 'package:common_utils/common_utils.dart';
import 'package:charts_flutter/flutter.dart' as charts;

/*
 * 数量统计-饼图,支持传数据(内存)和网络查数据<br/>
 * @param {Object 必传} initParam['uicfg'] ui配置项
 * @param {Object 选传} initParam['uicfg']['series'] text  总投资
 * @param {Object 选传} initParam['uicfg']['animate'] bool  动画效果
 * @param {Object 选传} initParam['uicfg']['vertical'] bool  垂直或水平
 * @param {Object 选传} initParam['uicfg']['width'] bool   宽度
 * @param {Object 选传} initParam['uicfg']['height'] bool  高度
 *
 *
 * 自己查数据
 * @param {Object 选传} initParam['major']  主类型
 * @param {Object 选传} initParam['minor']  子类型
 * @param {Object 选传} initParam['exp']    条件
 * @param {Object 选传} initParam['types']  类型
 * @param {Object 选传} initParam['vals']   值
 * @param {Object 选传} initParam['separator']  分隔符
 * @param {Object 选传} initParam['map']         键值对
 *
 * 否则:内存查
 * @param {Object 选传} initParam['data']
 * @param {Object 选传} initParam['data']['list']  被统计的集合对象
 * @param {Object 选传} initParam['data']['fld']   对哪个字段统计
 * @param {Object 选传} initParam['data']['fld2']   对哪个字段统计
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CtrlPieOutSideLabelChart extends CtrlChartBas
{
  CtrlPieOutSideLabelChart({initParam, plugin,Key key,}) : super(initParam: initParam,key: key, plugin: plugin);

  @override
  CtrlPieOutSideLabelChartState createState()
  => CtrlPieOutSideLabelChartState();
}

class CtrlPieOutSideLabelChartState extends CtrlChartBasState<CtrlPieOutSideLabelChart>
{
  void initView()
  {
    super.initView();
    Map map = widget.initParam[CtrlBigDataKeyConstant.UI_CFG];
    if(map[ChartKeyConstant.SERIES] == null)
    {
      map[ChartKeyConstant.SERIES] = PieOutSideLabelChart.createSampleData();
    }
  }

  @override
  Widget build(BuildContext context)
  {
    return PieOutSideLabelChart(initParam: widget.initParam[CtrlBigDataKeyConstant.UI_CFG],);
  }

  /*
   * 数量统计计算
   */
  void statistics()
  {
    if(CxTextUtil.isEmptyObject(widget.initParam[BasMapKeyConstant.MAP_KEY_DATA]))
    {
      return;
    }

    // 统计图表配置
    List list = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_LIST];
    String fld = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_FLD];
    String fld2 = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_FLD2];
    dynamic colors = widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.COLORS];

    List list2 = [];
    list.forEach((item)
    {
      if(item[fld] == item[fld2])
      {
        list2.add(item);
      }
    });
    Map map = ArrayUtil.gets2(list2,fld2);

    List<LinearSales> data = [];
    int total = list2.length;
    int index = 0;
    map.forEach((key,value)
    {
      double result = (value.length/total) *100;
      LinearSales linearSales = LinearSales(index,result.toInt());
      // 设置颜色
      dynamic color = colors[key];
      if(!CxTextUtil.isEmptyObject(color))
      {
        if(color is List)
        {
          linearSales.color = charts.ColorUtil.fromDartColor(Color.fromARGB(255, color[0], color[1], color[2]));
        }
        else
        {
          linearSales.color = charts.ColorUtil.fromDartColor(Color(color));
        }
      }
      data.add(linearSales);
      index++;
    });
    widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.SERIES] = PieOutSideLabelChart.createSampleData2(data);
  }

  /*
   * 数量统计计算
   */
  void statistics2(String repTime,String sum, String prjtype)
  {
    if(CxTextUtil.isEmptyObject(widget.initParam[BasMapKeyConstant.MAP_KEY_DATA]))
    {
      return;
    }

    // 统计图表配置
    List list = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_LIST];
    String fld = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_FLD];
    String fld2 = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_FLD2];
    dynamic colors = widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.COLORS];

    List list2 = [];
    list.forEach((item)
    {
      if(item[fld] == item[fld2])
      {
        list2.add(item);
      }
    });

    // 计算延期多少天
    String now = TimeUtil.getNowDateStrYMD();
    double total = 0.0;
    list2.forEach((value)
    {
      if(!CxTextUtil.isEmptyObject(value[repTime]))
      {
        String rep_time = TimeUtil.formatDateStr(value[repTime],format: DataFormats.y_mo_d);
        Duration duration = TimeUtil.calculatDuration(rep_time, now);
        double time = double.parse(TimeUtil.differenceDay(duration));
        value[sum] = time;
        total+=time;
      }
    });

    // 计算延期时间
    Map map = ArrayUtil.gets2(list2, prjtype);
    List<LinearSales> data = [];
    int index = 0;
    map.forEach((key,value)
    {
      double result = 0.0;
      value.forEach((value)
      {
        if(!CxTextUtil.isEmptyObject(value[sum]))
        {
          result+=value[sum];
        }
      });
      result = (result / total)*100;
      LinearSales linearSales = LinearSales(index,result.toInt());
      // 设置颜色
      dynamic color = colors[key];
      if(!CxTextUtil.isEmptyObject(color))
      {
        if(color is List)
        {
          linearSales.color = charts.ColorUtil.fromDartColor(Color.fromARGB(255, color[0], color[1], color[2]));
        }
        else
        {
          linearSales.color = charts.ColorUtil.fromDartColor(Color(color));
        }
      }
      data.add(linearSales);
      index++;
    });

    widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.SERIES] = PieOutSideLabelChart.createSampleData2(data);
  }

  /*
   * 外面计算，重新刷新数据
   */
  createSampleData2(data)
  {
    widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.SERIES] = PieOutSideLabelChart.createSampleData2(data);
  }
}
