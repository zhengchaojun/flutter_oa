import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/constant/CtrlBigDataKeyConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_ui/zpub_ui.dart';

/*
 * 数量统计基类,支持传数据(内存)和网络查数据<br/>
 * @param {Object 必传} initParam['uicfg'] ui配置项
 * @param {Object 选传} initParam['uicfg']['series'] text  总投资
 * @param {Object 选传} initParam['uicfg']['animate'] bool  动画效果
 * @param {Object 选传} initParam['uicfg']['vertical'] bool  垂直或水平
 * @param {Object 选传} initParam['uicfg']['width'] bool   宽度
 * @param {Object 选传} initParam['uicfg']['height'] bool  高度
 *
 *
 * 自己查数据
 * @param {Object 选传} initParam['major']  主类型
 * @param {Object 选传} initParam['minor']  子类型
 * @param {Object 选传} initParam['exp']    条件
 * @param {Object 选传} initParam['types']  类型
 * @param {Object 选传} initParam['vals']   值
 * @param {Object 选传} initParam['separator']  分隔符
 * @param {Object 选传} initParam['map']         键值对
 *
 * 否则:内存查
 * @param {Object 选传} initParam['data']
 * @param {Object 选传} initParam['data']['list']  被统计的集合对象
 * @param {Object 选传} initParam['data']['fld']   对哪个字段统计
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
abstract class CtrlChartBas extends WidgetStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> initParam;

  CtrlChartBas({this.initParam, plugin,Key key,}) : super(key: key, plugin: plugin);
}

abstract class CtrlChartBasState<T extends CtrlChartBas> extends WidgetBaseState<T>
{
  void initState()
  {
    super.initState();
    query();
  }

  void initView()
  {
    // 给柱状图设置一个默认的UI配置
    if(widget.initParam[CtrlBigDataKeyConstant.UI_CFG] == null)
    {
      return;
    }
    Map map = widget.initParam[CtrlBigDataKeyConstant.UI_CFG];
    if(map[ChartKeyConstant.ANIMATE] == null)
    {
      map[ChartKeyConstant.ANIMATE] = true;
    }
    if(map[BasMapKeyConstant.MAP_KEY_WIDTH] == null)
    {
      map[BasMapKeyConstant.MAP_KEY_WIDTH] = ScreenUtil.getInstance().getScreenWidth();
    }
    if(map[BasMapKeyConstant.MAP_KEY_HEIGHT] == null)
    {
      map[BasMapKeyConstant.MAP_KEY_HEIGHT] = 200.0;
    }
    if(map[ChartKeyConstant.VERTICAL] == null)
    {
      map[ChartKeyConstant.VERTICAL] = false;
    }
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "querys" && values is Map)
    {
      List list = values[BasMapKeyConstant.MAP_KEY_DATA];
      setState(()
      {
        widget.initParam[CtrlBigDataKeyConstant.UI_CFG][UIBigDataKeyConstant.FOOT][UIBigDataKeyConstant.TEXT] = list.length;
      });
    }
  }

  // 查询被统计的数据
  void query()
  {
    if(CxTextUtil.isEmptyObject(widget.initParam[DBFldConstant.FLD_MAJOR]))
    {
      return;
    }

    Map<String, Object> param =
    {
      HttpParamKeyValue.PARAM_KEY_MAJOR: widget.initParam[DBFldConstant.FLD_MAJOR],
      HttpParamKeyValue.PARAM_KEY_MINOR: widget.initParam[DBFldConstant.FLD_MINOR],
      HttpParamKeyValue.PARAM_KEY_EXP: widget.initParam[HttpParamKeyValue.PARAM_KEY_EXP],
      HttpParamKeyValue.PARAM_KEY_TYPES: widget.initParam[HttpParamKeyValue.PARAM_KEY_TYPES],
      HttpParamKeyValue.PARAM_KEY_VALS: widget.initParam[HttpParamKeyValue.PARAM_KEY_VALS],
      HttpParamKeyValue.PARAM_KEY_SEPARATOR: widget.initParam[HttpParamKeyValue.PARAM_KEY_SEPARATOR]??BasStringValueConstant.STR_COMMON_COLON_A_COLON,
    };
    SvrAreaSvrService.querys(this,param: param);
  }

  /*
   * 取满足条件数据
   * 计算fld中有多少个数量
   */
  Map gets2()
  {
    List list = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_LIST];
    String fld = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_FLD];
    return ArrayUtil.gets2(list, fld);
  }
}
