import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:flutter/widgets.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';

/*
 * 属性Item组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class AttItemParser extends WidgetParser {
  @override
  bool forWidget(String widgetName) {
    return "AttItemWidget" == widgetName;
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener listener) {
    Fld fld;
    if (map['fld'] is Fld) {
      fld = map['fld'];
    } else {
      fld = Fld.fromJson(map['fld']);
    }
    return AttItemWidget(fld, mValue: map['value'], key: map['key'], mOnChanged: map['OnChanged']);
  }
}
