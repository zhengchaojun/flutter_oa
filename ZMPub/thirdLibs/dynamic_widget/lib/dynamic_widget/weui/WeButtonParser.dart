import 'package:dynamic_widget/dynamic_widget/utils.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:flutter/widgets.dart';
import 'package:weui/weui.dart';

/*
 * weui按钮组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class WeButtonParser extends WidgetParser {
  @override
  bool forWidget(String widgetName) {
    return "WeButton" == widgetName;
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener listener) {
    WeButtonSize size = map.containsKey("size")
        ? (map["size"] == 'mini' ? WeButtonSize.mini : WeButtonSize.acquiescent)
        : WeButtonSize.acquiescent;
    bool hollow = map.containsKey("hollow")
        ? (map["hollow"] == '1' ? true : false)
        : false;
    bool disabled = map.containsKey("disabled")
        ? (map["disabled"] == '1' ? true : false)
        : false;
    bool loading = map.containsKey("loading")
        ? (map["loading"] == '1' ? true : false)
        : false;
    return WeButton(
      map['child'],
      onClick: () {
        listener.onClicked(map);
      },
      size: size,
      hollow: hollow,
      disabled: disabled,
      loading: loading,
    );
  }
}
