//
//  Generated file. Do not edit.
//

#import "GeneratedPluginRegistrant.h"

#if __has_include(<flutter_des/FlutterDesPlugin.h>)
#import <flutter_des/FlutterDesPlugin.h>
#else
@import flutter_des;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [FlutterDesPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterDesPlugin"]];
}

@end
