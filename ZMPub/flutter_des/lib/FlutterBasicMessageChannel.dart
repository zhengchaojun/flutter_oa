import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';

class FlutterBasicMessageChannel
{
  static const BasicMessageChannel<String> _basicMessageChannel = const BasicMessageChannel("FlutterBasicMessageChannel", StringCodec());

  /*
   * 发送参数
   *
   * @param param={'_major': 98,'_minor': 23,'cmd': query,'sessionid': 7D2907E8DF8E6A200CF985ED6AB5536D,'count': 2}
   */
  static void send(dynamic params)
  {
    _basicMessageChannel.send(json.encode(params));
  }

  /*
   * 接收参数
   *
   * @param param={'_major': 98,'_minor': 23,'cmd': query,'sessionid': 7D2907E8DF8E6A200CF985ED6AB5536D,'count': 2}
   */
  static void setMessageHandler(Future<String> handler(String message))
  {
    _basicMessageChannel.setMessageHandler(handler);
  }
}
