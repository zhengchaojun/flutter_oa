import 'package:flutter/material.dart';

import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_controls/zpub_controls.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_repair_order/com/zerogis/zpubrepair/constant/PairBPMConstant.dart';
import 'package:zpub_repair_order/com/zerogis/zpubrepair/widget/PairTemplateBtnWidget.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'PairDetailPlugin', '待办详情', '待办详情', 1, 4, 1, 'package:repairordershlt/com/zerogis/repairordershlt/scene', 'PairDetailPlugin', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 维修详情+属性页+网络模版+附件+反馈意见+底部按钮 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PairDetailPlugin extends PluginStatefulBase
{
  dynamic mInitPara;

  PairDetailPlugin({Key key, this.mInitPara, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new PairDetailPluginState();
  }

  static String toStrings()
  {
    return "PairDetailPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class PairDetailPluginState<T extends PairDetailPlugin> extends PluginBaseState<T>
{
  void initState()
  {
    super.initState();
    mTitle = widget.mInitPara[PairBPMConstant.PAIR_PROCESSNAME];
    createGlobalKeys(2);
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBody(
        context,
        SingleChildScrollView(child: Column(
          children: <Widget>[
            Card(child: Column(children: <Widget>[
              createAttAndTemplate(),
              createAttOnlyTemplateWidget(),
              createDuplicateDocWidget(),
            ],),),
            createStepperWidget(),

            createTempBtnWidget(),
          ],
        ),));
    return widget;
  }

  void initData()
  {
    super.initData();
    // 准备属性(att)生成参数 结果：{major:1,minor:1,id:1}
    Map<String, dynamic> param = {};
    EntityManagerConstant entityManagerConstant = EntityManager.getInstance();
    String tabbatt = widget.mInitPara[PairBPMConstant.PAIR_VARIABLES][BasMapKeyConstant.MAP_KEY_TABLE];
    List<int> list = entityManagerConstant.queryEntityMajorMinor(tabbatt);
    Object id = int.parse(widget.mInitPara[PairBPMConstant.PAIR_VARIABLES][PairBPMConstant.PAIR_BUSINESSKEY]);
    param[DBFldConstant.FLD_MAJOR] = list[0];
    param[DBFldConstant.FLD_MINOR] = list[1];
    param[DBFldConstant.FLD_ID] = id;
    widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT] = param;
    widget.mInitPara[BasMapKeyConstant.MAP_KEY_ENTITY] = entityManagerConstant.queryEntityWithTabbatt(tabbatt);
  }

  Widget createAttAndTemplate()
  {
    // 准备参数
    int major = widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT][DBFldConstant.FLD_MAJOR];
    int minor = widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT][DBFldConstant.FLD_MINOR];
    int id = widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT][DBFldConstant.FLD_ID];
    Map<String, dynamic> param = {
      BasMapKeyConstant.MAP_KEY_FROM: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ENTITY].form
    };
    widget.mInitPara.forEach((key, value)
    {
      param[key] = value;
    });

    return AttAndTemplateWidget(
      major, minor, initPara: param, id: id, attState: AttFldValueConstant.ATT_STATE_DEFAULT, plugin: widget.plugin,);
  }

  /*
   * 创建孩子网络模板组件
   */
  Widget createAttOnlyTemplateWidget()
  {
    // 准备参数
    Map<String, dynamic> param = {
      BasMapKeyConstant.MAP_KEY_FROMKEY: widget.mInitPara[BasMapKeyConstant.MAP_KEY_FROMKEY],
      BasMapKeyConstant.MAP_KEY_ID: widget.mInitPara[PairBPMConstant.PAIR_PROCESS_DEF_ID],
      BasMapKeyConstant.MAP_KEY_VARIABLE: widget.mInitPara[PairBPMConstant.PAIR_VARIABLE],
      BasMapKeyConstant.MAP_KEY_ATT: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT],
    };
    List<GlobalKey<State<StatefulWidget>>> list = mChildrenItem.keys.toList();
    Widget temp = AttOnlyTemplateWidget(mInitPara: param, key: list[0], plugin: widget.plugin,);
    mChildrenItem[list[0]] = temp;
    return temp;
  }

  /*
   * 创建孩子附件组件
   */
  Widget createDuplicateDocWidget()
  {
    // 准备参数
    List<GlobalKey<State<StatefulWidget>>> list = mChildrenItem.keys.toList();
    Widget dup = new DuplicateDocWidget(initPara: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT], key: list[1], plugin: widget.plugin,);
    mChildrenItem[list[1]] = dup;
    return dup;
  }

  /*
   * 创建孩子附件组件
   */
  Widget createStepperWidget()
  {
    // 准备参数
    Map<String, dynamic> param = {
      PairBPMConstant.PAIR_COMMENTS: widget.mInitPara[PairBPMConstant.PAIR_COMMENTS],
    };
    return Card(child: new MessageStepperWidget(mInitPara: param, plugin: widget.plugin,));
  }

  /*
   * 创建孩子附件组件
   */
  Widget createTempBtnWidget()
  {
    // 准备参数
    Map<String, dynamic> param = {
      BasMapKeyConstant.MAP_KEY_TABLE: mChildrenItem,
    };
    param.addAll(widget.mInitPara);

    return PairTemplateBtnWidget(initPara: param, plugin: widget.plugin,);
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PairDetailPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new PairDetailPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new PairDetailPlugin();
  }
}
