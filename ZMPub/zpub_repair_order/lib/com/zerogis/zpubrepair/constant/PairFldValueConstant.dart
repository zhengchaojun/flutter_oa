/*
 * 类描述：数据库字段值相关
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class PairFldValueConstant
{
  static final int PAT_PLANREC_PLANZT_2 = 2; // 未开始

  static final int PAT_PLANS_PST_0 = 0; // 已完结

  static final int QUERY_HIS_PROC_FINISH = 1; // 1-结案
}
