/*
 * 类描述：bpm字段值相关
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class PairBPMConstant
{
  static final String PAIR_PROCESSNAME = 'processName'; // 状态

  static final String PAIR_VARIABLES = 'variables'; // 变量

  static final String PAIR_BUSINESSKEY = 'businessKey'; // 计划key

  static final String PAIR_PRJID = "prjid"; // 项目ID

  static final String PAIR_PROCID = "procid"; // 流程ID

  static final String PAIR_PROCESS_DEF_ID = "processDefinitionId"; // 流程ID

  static final String PAIR_PROCESSID = "processId"; // 流程ID

  static final String PAIR_COMMENTS = "comments"; // 公共的消息数据

  static final String PAIR_PROCKEY = "prockey"; // 流程key

  static final String PAIR_VARIABLE = "variables"; // 变量

  static final String PAIR_ASSIGNEE = "assignee"; // 变量

  static final String PAIR_CREATETIME = "createTime"; // 变量

  static final String PAIR_DUEDATE = "dueDate"; // 变量

  static final String PAIR_FORMKEY = "formKey"; // 变量

  static final String PAIR_OWNER = "owner"; // 变量

  static final String PAIR_PRIORITY = "priority"; // 变量

  static final String PAIR_PARENTTASKID = "parentTaskId"; // 变量

  static final String PAIR_EXECUTIONID = "executionId"; // 变量

  static final String PAIR_PROCESSINSTANCEID = "processInstanceId"; // 变量

  static final String PAIR_TASKDEFINITIONKEY = "taskDefinitionKey"; // 变量
}