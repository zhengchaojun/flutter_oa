import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_repair_order/com/zerogis/zpubrepair/widget/PluginParamBas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_third/zpub_third.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'TodoWidget', '待办', '待办', 1, 3, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'TodoWidget', '', '', '{"listitem":{"name":"处理环节","processName":"流程名称","createTime":"创建时间","variables":{"bill":"工单编号","depart":"测试部","applyUserName":"发起人","reason":"事由"}}}', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 待办组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class TodoWidget extends PluginParamBas
{
  TodoWidget({Key key, plugin, mInitPara}) :super(key: key, mInitPara: mInitPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new TodoWidgetState();
  }

  static String toStrings()
  {
    return "TodoWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class TodoWidgetState<T extends TodoWidget> extends PluginParamBasState<T>
{
  /*
   * 是否使用入栈操作
   */
  bool usePushStack()
  {
    return false;
  }

  void initState()
  {
    super.initState();
  }

  /*
   * 初始化相关
   */
  void init()
  {
    initData();
    initListener();
  }

  /*
   * 初始化数据相关
   */
  void initData()
  {
    queryInitPara();
    queryInitParaPlugins();
    queryWidgetsList();
  }

  /*
   * 初始化监听相关
   */
  void initListener()
  {
    ThirdApplication.getInstance().getEventBus().on().listen((event)
    {
      if (event is BasMsgEvent && event.getType() == BasEvnBusKeyConstant.BAS_COM_TASK_REFRESH)
      {
        onRefresh();
      }
    });
  }

  Widget build(BuildContext context)
  {
    Widget widget = createCommonRefresh(mEmptyView);
    return widget;
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryMyTask")
    {
      dealQuery(values);
    }
  }

  void query(int page)
  {
    super.query(page);
    SvrBpmService.queryMyTask(m_pager, this, page: page);
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class TodoWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new TodoWidget(mInitPara: initPara, key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY],);
  }
}
