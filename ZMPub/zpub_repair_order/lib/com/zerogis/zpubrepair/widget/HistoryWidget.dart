import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttFjPlugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_repair_order/com/zerogis/zpubrepair/widget/PluginParamBas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

import '../constant/PairFldConstant.dart';
import '../constant/PairFldValueConstant.dart';

//INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'HistoryWidget', '已办箱', '已办箱', 1, 3, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'HistoryWidget', '', '', '{"listitem":{"processDefinitionName":"流程名称","endTime":"结束时间","variables":{"bill":"工单编号","depart":"测试部","applyUserName":"发起人","reason":"事由"}}}', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 历史组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class HistoryWidget extends PluginParamBas
{
  HistoryWidget({Key key, plugin, mInitPara}) :super(key: key, mInitPara: mInitPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new HistoryWidgetState();
  }

  static String toStrings()
  {
    return "HistoryWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class HistoryWidgetState<T extends HistoryWidget> extends PluginParamBasState<T>
{
  /*
   * 是否使用入栈操作
   */
  bool usePushStack()
  {
    return false;
  }

  void initState()
  {
    super.initState();
  }

  /*
   * 初始化相关
   */
  void init()
  {
    initData();
  }

  /*
   * 初始化数据相关
   */
  void initData()
  {
    queryInitPara();
    queryInitParaPlugins();
    queryWidgetsList();
  }

  Widget build(BuildContext context)
  {
    Widget widget = createCommonRefresh(mEmptyView);
    return widget;
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryHisProc")
    {
      dealQuery(values);
    }
  }

  void query(int page)
  {
    Map<String, dynamic> param = {'finished': PairFldValueConstant.QUERY_HIS_PROC_FINISH, 'involvedUser': UserMethod.getUserNo()};
    SvrBpmService.queryHisProc(m_pager, this, page: page, param: param);
  }

  void doClickListItem(item)
  {
    Map<String, dynamic> param = {};
    EntityManagerConstant entityManagerConstant = EntityManager.getInstance();
    String tabbatt = item[SvrBPMConstant.SVR_VARIABLES][BasMapKeyConstant.MAP_KEY_TABLE];
    List<int> list = entityManagerConstant.queryEntityMajorMinor(tabbatt);
    param[DBFldConstant.FLD_MAJOR] = list[0];
    param[DBFldConstant.FLD_MINOR] = list[1];
    param[BasMapKeyConstant.MAP_KEY_ATT] = item[SvrBPMConstant.SVR_VARIABLES];
    param[BasMapKeyConstant.MAP_KEY_VALUE_TITLE] = mTitle;
    param[AttConstant.ATT_STATE] = AttFldValueConstant.ATT_STATE_DEFAULT;
    runPluginParam(AttFjPlugin.toStrings(), param);
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class HistoryWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new HistoryWidget(mInitPara: initPara, key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY],);
  }
}
