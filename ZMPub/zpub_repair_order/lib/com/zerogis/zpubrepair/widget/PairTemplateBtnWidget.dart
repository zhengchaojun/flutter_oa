import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/widget/AttOnlyTemplateWidget.dart';
import 'package:zpub_att/com/zerogis/zpubatt/widget/TemplateBtnBas.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_controls/zpub_controls.dart';
import 'package:zpub_repair_order/com/zerogis/zpubrepair/procs/factory/ProcFactory.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

import '../procs/ProcsBasePlugin.dart';

/*
 * 维修网络模板按钮组件 <br/>
 * @param {Object 必传} mId
 * @param {Object 选传} mSfromKey
 * @param {Object 选传} mTableItemData 表格条目数据
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PairTemplateBtnWidget extends TemplateBtnBas
{
  PairTemplateBtnWidget({initPara, Key key, plugin})
      : super(key: key, initPara: initPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new PairTemplateBtnWidgetState();
  }

  static String toStrings()
  {
    return "PairTemplateBtnWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class PairTemplateBtnWidgetState<T extends PairTemplateBtnWidget> extends TemplateBtnBasState<T>
{
  void doClickTemp0Button(int key, event)
  {
    // 数据校验
    AttOnlyTemplateWidgetState tempState = getState<AttOnlyTemplateWidgetState>();
    Map<String, String> param = queryAttKeyValue(tempState);
    if (CxTextUtil.isEmptyMap(param) && !CxTextUtil.isEmptyList(tempState.fldList))
    {
      return;
    }
    if (!CxTextUtil.isEmpty(param["COMMENT_"]) && param["COMMENT_"].contains("不同意"))
    {
      showToast("反馈意见可能不合适");
      return;
    }
    else if (CxTextUtil.isEmpty(param["COMMENT_"]))
    {
      param["COMMENT_"] = UserMethod.getUser().getName() +
          BasStringValueConstant.STR_COMMON_A_COLON
          + BasStringValueConstant.STR_COMMON_A_COLON +
          widget.initPara[BasMapKeyConstant.MAP_KEY_NAME];
    }
    if (CxTextUtil.isEmpty(mNo) && mAssignee)
    {
      showToast("没有指定办理人");
      return;
    }

    // 保存附件
    State dupState = getState<DuplicateDocWidgetState>();
    saveDuplicateDoc(dupState);

    // 提交任务
    getProcVariables(param, widget.initPara[BasMapKeyConstant.MAP_KEY_ID], key);
    SvrBpmService.completeTask(this, id: widget.initPara[BasMapKeyConstant.MAP_KEY_ID], approved: key, param: param);
  }

  void doClickTemp1Button(int key, event)
  {
    // 数据校验
    State tempState = getState<AttOnlyTemplateWidgetState>();
    Map<String, String> param = queryAttKeyValue(tempState);
    if (CxTextUtil.isEmptyMap(param))
    { // 不同意
      return;
    }
    if (!CxTextUtil.isEmpty(param["COMMENT_"]) && param["COMMENT_"].split('@:')[1] == "同意")
    {
      showToast("反馈意见可能不合适");
      return;
    }

    // 保存附件
    State dupState = getState<DuplicateDocWidgetState>();
    saveDuplicateDoc(dupState);

    // 提交任务
    getProcVariables(param, widget.initPara[BasMapKeyConstant.MAP_KEY_ID], key);
    SvrBpmService.completeTask(this, id: widget.initPara[BasMapKeyConstant.MAP_KEY_ID], approved: key, param: param);
  }

  void doClickTemp2Button(int key, event)
  {
    runPluginModel((item)
    {
      dynamic node = item.mObject;
      if (CxTextUtil.isEmptyList(node[BasMapKeyConstant.MAP_KEY_CHILDREN]))
      {
        dynamic variables = widget.initPara[BasMapKeyConstant.MAP_KEY_ID];
        if (variables[SvrBPMConstant.SVR_APPLY_USERNAME] != node[BasMapKeyConstant.MAP_KEY_USERNAME])
        {
          SvrBpmService.assignTask(
              widget.initPara[SvrBPMConstant.SVR_PROCESSID], node[BasMapKeyConstant.MAP_KEY_NO], this);
        }
        else
        {
          showToast("不能指派给发起人!");
        }
        Navigator.pop(node['context']);
      }
    });
  }

  void doClickTemp3Button(int key, event)
  {
    Map<String,Object> idetityParam = {};
    idetityParam.addAll(widget.initPara);
    idetityParam.addAll(mButtons);
    idetityParam.addAll(widget.initPara[SvrBPMConstant.SVR_VARIABLES]);
    Map<String,Object> param = {BasMapKeyConstant.MAP_KEY_VALUE_TITLE : "选择组成员", BasMapKeyConstant.MAP_KEY_CHILDREN :[
      IdentitySvrWidget(
        mInitPara: idetityParam,
        valueChangedMethod: (item)
        {
          mNo = item.getNo();
        }, plugin: widget.plugin,)
    ]};
    runModelParam(param);
  }

  void doClickTemp_1Button(int key, event)
  {
    // 数据校验
    State tempState = getState<AttOnlyTemplateWidgetState>();
    Map<String, String> param = queryAttKeyValue(tempState);
    if (CxTextUtil.isEmptyMap(param))
    {
      return;
    }

    // 提交任务
    getProcVariables(param, widget.initPara[BasMapKeyConstant.MAP_KEY_ID], key);
    SvrBpmService.returnTask(this, id: widget.initPara[BasMapKeyConstant.MAP_KEY_ID], approved: key,
        backto: this.mButtons[HttpParamKeyValue.PARAM_KEY_BACKTO], param: param);
  }

  /*
   * 返回上一场景
   */
  void finish()
  {
    Navigator.pop(StateManager
        .getInstance()
        .currentState()
        .context);
  }

  /*
   * 取流程参数
   * @method
   * @param   {Object}    att               属性(当前需要发起流程的属性)
   */
  void getProcVariables(Map<String, String> mProcVariables, dynamic variables, int key)
  {
    ProcBase procBase = ProcFactory.getInstance().get(widget.initPara[BasMapKeyConstant.MAP_KEY_FROM]);
    if(!CxTextUtil.isEmptyObject(procBase))
    {
      procBase.getProcVariables(mProcVariables, widget.initPara);
    }
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PairTemplateBtnWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new PairTemplateBtnWidget(initPara: initPara, key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY],);
  }
}

