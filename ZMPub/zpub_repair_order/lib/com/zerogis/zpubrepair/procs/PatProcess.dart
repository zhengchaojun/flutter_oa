import 'ProcsBasePlugin.dart';

/*
 * 类描述：巡检流程处理
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PatProcess extends ProcBase
{
  static String toStrings()
  {
    return "PatProcess";
  }

  /*
   * 流程节点取流程变量
   * @param attValues 属性key和value值
   * @param proc 流程对象
   */
  void execute(Map<String, dynamic> attValues,dynamic proc)
  {
    if(proc['taskDefinitionKey'] == 'manAudit')
    {
      mProcVariables['FK_confirm'] = attValues['confirm'];
    }
  }

  /*
   * 取流程对应节点变量
   * @param procVariables 提交complete流程变量
   * @param proc 流程对象
   */
  void getProcVariables(Map<String, String> procVariables, dynamic proc)
  {
    if(proc['taskDefinitionKey'] == 'manAudit')
    {
      mProcVariables.forEach((key, value)
      {
        procVariables[key] = value.toString();
      });
    }
  }
}