import 'package:zpub_repair_order/com/zerogis/zpubrepair/procs/ProcsBasePlugin.dart';

/*
 * 类描述：流程处理集合类
 * 为什么要流程处理类？
 *  1：工作流中每一个节点点击办理按钮的时候，可能需要一个类或者一个方法去处理那个节点的数据，InitSvr中已经定义好了
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class ProcFactory
{
  static ProcFactory instance;

  Map<String, ProcBase> stateListStack = {};

  static ProcFactory getInstance()
  {
    if (instance == null)
    {
      instance = new ProcFactory();
    }
    return instance;
  }

  void add(String key, ProcBase listener)
  {
    stateListStack[key] = listener;
  }

  ProcBase get(String key)
  {
    return stateListStack[key];
  }

  void remove(ProcBase listener)
  {
    stateListStack.remove(listener);
  }
}
