import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_repair_order/com/zerogis/zpubrepair/procs/PatProcess.dart';
import 'package:zpub_repair_order/com/zerogis/zpubrepair/procs/factory/ProcFactory.dart';
import 'package:zpub_bas/zpub_bas.dart';


/*
 * 类描述：流程注册中心，将来按照模块的方式依赖的时候再分开
 * 为什么要流程处理类？
 *  1：工作流中每一个节点点击办理按钮的时候，可能需要一个类或者一个方法去处理那个节点的数据，InitSvr中已经定义好了
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class ProcApplication
{
  void onCreate()
  {
    List<dynamic> procs = InitSvrMethod.getInitSvrMap()["procs"];
    if(!CxTextUtil.isEmptyList(procs))
    {
      procs.forEach((item)
      {
        if (item['prockey'] == PatProcess.toStrings())
        { // 巡检流程处理
          ProcFactory.getInstance().add(item['form'], new PatProcess());
        }
      });
    }
  }
}
