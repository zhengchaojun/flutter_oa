import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasMapKeyConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/util/ScreenUtil.dart';
import 'package:zpub_ui/com/zerogis/zpubui/charts/constant/ChartKeyConstant.dart';
import 'package:common_utils/common_utils.dart';

/*
 * 柱状图 <br/>
 * @param {Object 必传} initParam['series'] text  总投资
 * @param {Object 必传} initParam['animate'] bool  动画效果
 * @param {Object 必传} initParam['vertical'] bool  垂直或水平
 * @param {Object 必传} initParam['width'] bool  宽度
 * @param {Object 必传} initParam['height'] bool  高度
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class BarChart extends StatefulWidget
{
  /*
   * 初始化参数
   */
  Map<dynamic, dynamic> initParam;

  BarChart({
    this.initParam,
    Key key,
  }) : super(key: key);

  @override
  BarChartState createState() => BarChartState();

  /*
   * 例子
   */
  static Widget withSampleData({List<charts.Series<OrdinalSales, String>> series})
  {
    Map<String, dynamic> param = {
      ChartKeyConstant.ANIMATE: true,
      ChartKeyConstant.SERIES: series?? createSampleData(),
      BasMapKeyConstant.MAP_KEY_WIDTH: ScreenUtil.getInstance().getScreenWidth(),
      BasMapKeyConstant.MAP_KEY_HEIGHT: 200.0,
      ChartKeyConstant.VERTICAL: false,
    };
    return BarChart(
      initParam: param,
    );
  }

  /*
   * 例子数据
   */
  static List<charts.Series<OrdinalSales, String>> createSampleData()
  {
    final data = [
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
    ];
    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
  
  /*
   * 例子数据
   * @param List data 
   */
  static List<charts.Series<OrdinalSales, String>> createSampleData2(data)
  {
    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (sales, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => substring(sales.year),
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }

  /*
   * 避免文字过长
   * @param year x轴
   * @param length  长度
   */
  static String substring(String year,{int length:ChartKeyConstant.BAR_X_LENGTH})
  {
    if(!RegexUtil.isZh(year))
    {
      return year;
    }

    if(year.length > length)
    {
      return year.substring(0,length)+'...';
    }
    else
    {
      return year;
    }
  }
}

class BarChartState<T extends BarChart> extends State<T>
{
  @override
  Widget build(BuildContext context)
  {
    return Container(
      child: charts.BarChart(
        widget.initParam[ChartKeyConstant.SERIES],
        animate: widget.initParam[ChartKeyConstant.ANIMATE],
        vertical: widget.initParam[ChartKeyConstant.VERTICAL],
        barGroupingType: widget.initParam[ChartKeyConstant.GROUP_TYPE],
      ),
      height: widget.initParam[BasMapKeyConstant.MAP_KEY_HEIGHT] ?? ScreenUtil.getInstance().getScreenWidth() * 0.5,
      width: widget.initParam[BasMapKeyConstant.MAP_KEY_WIDTH] ?? ScreenUtil.getInstance().getScreenWidth(),
    );
  }
}

/// 模型数据
class OrdinalSales
{
  final String year;
  final int sales;
  String id;
  Color color;

  OrdinalSales(this.year, this.sales);
}
