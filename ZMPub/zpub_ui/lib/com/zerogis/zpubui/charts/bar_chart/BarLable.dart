import 'package:flutter/material.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_ui/com/zerogis/zpubui/charts/constant/ChartKeyConstant.dart';

/*
 * 柱状图，各种图标注 <br/>
 * @param {Object 必传} initParam['text']      中央资金
 * @param {Object 选传} initParam['fontSize']  字体大小
 * @param {Object 选传} initParam['textColor'] 字体颜色
 * @param {Object 选传} initParam['color']     左边颜色
 * @param {Object 选传} initParam['width']     宽度
 * @param {Object 选传} initParam['height']    高度
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class BarLable extends StatefulWidget
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> initParam;

  BarLable({this.initParam, Key key,}) :super(key: key);

  @override
  BarLableState createState()
  => BarLableState();
}

class BarLableState extends State<BarLable>
{
  @override
  Widget build(BuildContext context)
  {
    Color color2;
    if(widget.initParam[ChartKeyConstant.COLOR] == null)
    {
      color2 = Color(0xff666666);
    }
    else if(widget.initParam[ChartKeyConstant.COLOR] is List)
    {
      color2 = Color.fromARGB(255, widget.initParam[ChartKeyConstant.COLOR][0], widget.initParam[ChartKeyConstant.COLOR][1], widget.initParam[ChartKeyConstant.COLOR][2]);
    }
    else
    {
      color2 = Color(widget.initParam[ChartKeyConstant.COLOR]);
    }
    
    return Row(children: <Widget>[
      Container(
        width: widget.initParam[BasMapKeyConstant.MAP_KEY_WIDTH]??BasSizeRes.textview_text_small,
        height: widget.initParam[BasMapKeyConstant.MAP_KEY_HEIGHT]??BasSizeRes.textview_text_small,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2.0),
          color: color2,
        ),
      ),
      SizedBox(width: 5,),
      Text(
        '${widget.initParam[ChartKeyConstant.TEXT]}',
        style: TextStyle(
          fontFamily: 'MicrosoftYaHeiUI',
          fontSize: widget.initParam[ChartKeyConstant.FONT_SIZE]??BasSizeRes.textview_text_small,
          color: Color(widget.initParam[ChartKeyConstant.TEXT_COLOR]??0xffffc261),
        ),
        textAlign: TextAlign.left,
      )
    ],);
  }
}
