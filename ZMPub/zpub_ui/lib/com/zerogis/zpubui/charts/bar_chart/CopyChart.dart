import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasMapKeyConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/util/ScreenUtil.dart';
import 'package:zpub_ui/com/zerogis/zpubui/charts/constant/ChartKeyConstant.dart';

/*
 * 柱状图 <br/>
 * @param {Object 必传} initParam['series'] text  总投资
 * @param {Object 必传} initParam['animate'] bool  动画效果
 * @param {Object 必传} initParam['vertical'] bool  垂直水平
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CopyChart extends StatefulWidget
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> initParam;

  CopyChart({
    this.initParam,
    Key key,
  }) : super(key: key);

  @override
  CopyChartState createState() => CopyChartState();

  /*
   * 例子
   */
  static Widget withSampleData({List<charts.Series<OrdinalSales, String>> series})
  {
    Map<String, dynamic> param = {
      ChartKeyConstant.ANIMATE: true,
      ChartKeyConstant.SERIES: _createSampleData(),
      BasMapKeyConstant.MAP_KEY_WIDTH: ScreenUtil.getInstance().getScreenWidth(),
      BasMapKeyConstant.MAP_KEY_HEIGHT: 200.0,
      ChartKeyConstant.VERTICAL: false,
    };
    return CopyChart(
      initParam: param,
    );
  }

  /*
   * 例子数据
   */
  static List<charts.Series<OrdinalSales, String>> _createSampleData()
  {
    final data = [
      new OrdinalSales('2014', 5),
      new OrdinalSales('2015', 25),
      new OrdinalSales('2016', 100),
      new OrdinalSales('2017', 75),
    ];
    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
}

class CopyChartState<T extends CopyChart> extends State<T>
{
  @override
  Widget build(BuildContext context)
  {
    return Container(
      child: charts.BarChart(
        widget.initParam[ChartKeyConstant.SERIES],
        animate: widget.initParam[ChartKeyConstant.ANIMATE],
        vertical: widget.initParam[ChartKeyConstant.VERTICAL],
      ),
      height: widget.initParam[BasMapKeyConstant.MAP_KEY_HEIGHT] ?? ScreenUtil.getInstance().getScreenWidth() * 0.5,
      width: widget.initParam[BasMapKeyConstant.MAP_KEY_WIDTH] ?? ScreenUtil.getInstance().getScreenWidth(),
    );
  }
}

/// 模型数据
class OrdinalSales
{
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
