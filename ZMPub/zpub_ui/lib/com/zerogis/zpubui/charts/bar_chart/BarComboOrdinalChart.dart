import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasMapKeyConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/util/ScreenUtil.dart';
import 'package:zpub_ui/com/zerogis/zpubui/charts/bar_chart/BarChart.dart';
import 'package:zpub_ui/com/zerogis/zpubui/charts/constant/ChartKeyConstant.dart';

/*
 * 柱状组合，依次，有序图 <br/>
 * @param {Object 必传} initParam['series'] text  总投资
 * @param {Object 必传} initParam['animate'] bool  动画效果
 * @param {Object 必传} initParam['vertical'] bool  垂直水平
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class BarComboOrdinalChart extends BarChart
{
  BarComboOrdinalChart({initParam, Key key,}) : super(initParam:initParam,key: key);

  @override
  BarComboOrdinalChartState createState() => BarComboOrdinalChartState();

  /*
   * 例子
   */
  static Widget withSampleData({List<charts.Series<OrdinalSales, String>> series})
  {
    Map<String, dynamic> param = {
      ChartKeyConstant.ANIMATE: true,
      ChartKeyConstant.SERIES: createSampleData(),
      BasMapKeyConstant.MAP_KEY_WIDTH: ScreenUtil.getInstance().getScreenWidth(),
      BasMapKeyConstant.MAP_KEY_HEIGHT: 200.0,
    };
    return BarComboOrdinalChart(
      initParam: param,
    );
  }

  /*
   * 例子数据
   *  desktopSalesData，tableSalesData，tableSalesData等多个数组，表示一个柱子里面有多少个小柱子：
   *  某个集合里面表示x轴有多少个大柱子：有多少份：比如：2014，2015，2016
   *  大柱子看一个集合里面：小柱子看有多少个数组
   */
  static List<charts.Series<OrdinalSales, String>> createSampleData()
  {
    final desktopSalesData = [
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
    ];

    final tableSalesData = [
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
    ];

    final mobileSalesData = [
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
      new OrdinalSales('正在加载', 0),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
          id: 'Desktop',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (OrdinalSales sales, _) => sales.year,
          measureFn: (OrdinalSales sales, _) => sales.sales,
          data: desktopSalesData),
      new charts.Series<OrdinalSales, String>(
          id: 'Tablet',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (OrdinalSales sales, _) => sales.year,
          measureFn: (OrdinalSales sales, _) => sales.sales,
          data: tableSalesData),
      new charts.Series<OrdinalSales, String>(
          id: 'Mobile ',
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
          domainFn: (OrdinalSales sales, _) => sales.year,
          measureFn: (OrdinalSales sales, _) => sales.sales,
          data: mobileSalesData)
        ..setAttribute(charts.rendererIdKey, 'customLine'),
    ];
  }

  /*
   * 例子数据
   * @param data = [[Instance of 'OrdinalSales', Instance of 'OrdinalSales', Instance of 'OrdinalSales'],
   *  [Instance of 'OrdinalSales', Instance of 'OrdinalSales', Instance of 'OrdinalSales'],
   *  [Instance of 'OrdinalSales', Instance of 'OrdinalSales', Instance of 'OrdinalSales']]
   */
  static List<charts.Series<OrdinalSales, String>> createSampleData2(List data)
  {
    List<charts.Series<OrdinalSales, String>> list = [];
    data.forEach((item)
    {
      list.add(charts.Series<OrdinalSales, String>(
          colorFn: (sales, __) => sales.color == null?charts.MaterialPalette.green.shadeDefault:sales.color,
          domainFn: (OrdinalSales sales, _) => sales.year,
          measureFn: (OrdinalSales sales, _) => sales.sales,
          data: item));
    });
    return list;
  }
  
  static List<charts.Series<OrdinalSales, String>> createSampleData2Old(List data)
  {
    List<charts.Series<OrdinalSales, String>> list = [];
    data.forEach((item)
    {
      item.forEach((key,value)
      {
        // key = 方案设计
        list.add(charts.Series<OrdinalSales, String>(
            id: key,
            colorFn: (sales, __) => sales.color,
            domainFn: (OrdinalSales sales, _) => sales.year,
            measureFn: (OrdinalSales sales, _) => sales.sales,
            data: value)..setAttribute(charts.rendererIdKey, 'customLine'));
      });
    });
    return list;
  }
}

class BarComboOrdinalChartState extends BarChartState<BarComboOrdinalChart>
{
  @override
  Widget build(BuildContext context)
  {
    return Container(
      child: charts.OrdinalComboChart(
          widget.initParam[ChartKeyConstant.SERIES],
          animate: widget.initParam[ChartKeyConstant.ANIMATE],
          defaultRenderer: new charts.BarRendererConfig(groupingType: charts.BarGroupingType.grouped),
          customSeriesRenderers: [charts.LineRendererConfig(customRendererId: 'customLine')]),
      height: widget.initParam[BasMapKeyConstant.MAP_KEY_HEIGHT] ?? ScreenUtil.getInstance().getScreenWidth() * 0.5,
      width: widget.initParam[BasMapKeyConstant.MAP_KEY_WIDTH] ?? ScreenUtil.getInstance().getScreenWidth(),
    );
  }
}
