class Node<T>
{
  static int typeOrgan = 10000;
  static int typeMember = 10001;

  bool mExpand;
  int mDepth;
  int mType;
  int mNodeId;
  int mFatherId;
  T mObject;

  Node(this.mExpand, this.mDepth, this.mType, this.mNodeId, this.mFatherId, this.mObject);

  @override
  String toString()
  {
    return 'Node{mExpand: $mExpand, mDepth: $mDepth, mType: $mType, mNodeId: $mNodeId, mFatherId: $mFatherId, mObject: $mObject}';
  }
}