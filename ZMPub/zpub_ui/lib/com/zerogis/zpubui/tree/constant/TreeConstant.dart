/*
 * 功能：树(tree)相关定义的常量相关
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class TreeConstant
{
  static const String NAME = "name";
  static const String NAMEC = "namec";
  static const String CHECK = "check";
  static const String CHILDREN = "children";
  static const String ICON = "icon";
}
