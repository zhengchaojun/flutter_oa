import 'package:flutter/material.dart';

/*
 * 类描述：滑动组件
 * 作者：郑朝军 on 2019/7/24
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/7/24
 * 修改备注：
 */
class TabBarViewWidget extends StatefulWidget
{
  List<Widget> tabs = [];
  List<Widget> tabsView = [];

  TabBarViewWidget(this.tabs, this.tabsView, {Key key}) : super(key: key);

  State<StatefulWidget> createState()
  {
    return new TabBarViewWidgetState();
  }
}

/*
 * 组件功能 <br/>
 */
class TabBarViewWidgetState extends State<TabBarViewWidget>
    with TickerProviderStateMixin
{
  TabController _controller;

  @override
  void initState()
  {
    _controller = TabController(length: widget.tabs.length, vsync: this);
    super.initState();
  }

  @override
  void dispose()
  {
    _controller.dispose();
    super.dispose();
  }

  Widget build(BuildContext context)
  {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TabBar(
          controller: _controller,
          isScrollable: true,
          labelColor: Colors.black,
          labelStyle: TextStyle(fontSize: 17),
          indicator: UnderlineTabIndicator(
              borderSide: BorderSide(color: Colors.deepPurpleAccent, width: 3),
              insets: EdgeInsets.symmetric(horizontal: 15)
          ),
          tabs: widget.tabs,
        ),
        Divider(height: 1, color: Colors.grey.withOpacity(0.5)),
        Flexible(child: TabBarView(controller: _controller, children: widget.tabsView)),
      ],
    );
  }

  int get index
  => _controller.index;
}
