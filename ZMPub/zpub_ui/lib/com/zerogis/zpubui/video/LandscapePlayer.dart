import 'dart:io';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:zpub_bas/zpub_bas.dart';

import 'LandscapePlayerControls.dart';

/*
 * 大屏播放-支持网络播放和本地播放 <br/>
 * @param {Object 必传} initParam['url'] url        播放地址
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class LandscapePlayer extends StatefulWidget {
  /*
   * 初始化参数
   */
  Map<dynamic, dynamic> initParam;

  LandscapePlayer({this.initParam, Key key}) : super(key: key);

  @override
  _LandscapePlayerState createState() => _LandscapePlayerState();
}

class _LandscapePlayerState extends State<LandscapePlayer> {
  FlickManager flickManager;

  @override
  void initState() {
    super.initState();
    VideoPlayerController videoPlayerController;
    dynamic url = widget.initParam[BasMapKeyConstant.MAP_KEY_URL];
    if (url is String && (url.contains(BasSoftwareConstant.SOFTWARE_ASSETS))) {
      videoPlayerController = VideoPlayerController.asset(widget.initParam[BasMapKeyConstant.MAP_KEY_URL]);
    } else if (url is String &&
        (url.contains(BasSoftwareConstant.NETWORK_HTTP) || url.contains(BasSoftwareConstant.NETWORK_HTTPS))) {
      videoPlayerController = VideoPlayerController.network(widget.initParam[BasMapKeyConstant.MAP_KEY_URL]);
    } else if (url is String && (url.contains(BasSoftwareConstant.SDCARD_FLAG) || url.startsWith(BasStringValueConstant.STR_COMMON_SLASH))) {
      videoPlayerController = VideoPlayerController.file(url is File ? url: File(url));
    }
    else if (url is File) {
      videoPlayerController = VideoPlayerController.file(url);
    }
    flickManager = FlickManager(videoPlayerController: videoPlayerController);
  }

  @override
  void dispose() {
    flickManager.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlickVideoPlayer(
        flickManager: flickManager,
        preferredDeviceOrientation: [DeviceOrientation.landscapeRight, DeviceOrientation.landscapeLeft],
        systemUIOverlay: [],
        flickVideoWithControls: FlickVideoWithControls(
          controls: LandscapePlayerControls(),
        ),
      ),
    );
  }
}
