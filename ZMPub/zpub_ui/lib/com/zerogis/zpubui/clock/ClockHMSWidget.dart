import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';

/*
 * 十分钟秒计时组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class ClockHMSWidget extends StatefulWidget
{
  TextStyle mStyle;

  ClockHMSWidget({Key key, this.mStyle}) : super(key: key);

  State<StatefulWidget> createState()
  {
    return new ClockHMSWidgetState();
  }
}

/*
 * 组件功能 <br/>
 */
class ClockHMSWidgetState extends State<ClockHMSWidget>
{
  TimerUtil mTimerUtil;

  void initState()
  {
    super.initState();
    mTimerUtil = new TimerUtil();
    mTimerUtil.setOnTimerTickCallback((callback)
    {
      setState(()
      {});
    });
    mTimerUtil.startTimer();
  }

  Widget build(BuildContext context)
  {
//    return new Text(
//      "${DateUtil.formatDate(
//          DateTime.now(), format: DateFormats.h_m_s)}",
//      style: widget.mStyle,
//    );
  }

  @override
  void dispose()
  {
    mTimerUtil.cancel();
    super.dispose();
  }
}
