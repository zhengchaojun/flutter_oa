import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/font/IconFont.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/resource/UiStringRes.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/resource/UiTextStyleRes.dart';

/*
 * 功能：属性相关的动态创建
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class UiWidgetCreator
{
  /*
   * 创建顶部是图标底部文字的组件<br/>
   * @param map 举例：{namec:请假,icon:terequire}
   */
  static Widget createCommonTopImgText2(Map<String, dynamic> map,
      {GestureTapCallback onTap, Map<String, dynamic> param})
  {
    return new GestureDetector(
      child: new Column(
        children: <Widget>[
          createCommonImage(
              "assets/images/dev/" + map[BasMapKeyConstant.MAP_ICON] + ".png",
              width: param[BasMapKeyConstant.MAP_KEY_WIDTH] ?? 35,
              height: param[BasMapKeyConstant.MAP_KEY_HEIGHT] ?? 35,
              fit: BoxFit.fill),
          new Text(map[BasMapKeyConstant.MAP_KEY_NAMEC],
              style: param[BasMapKeyConstant.MAP_KEY_STYLE] ?? BasTextStyleRes.text_color_text1_smallest)
        ],
      ),
      onTap: onTap,
    );
  }

  /*
   * 创建公共网络图片:支持本地，iconfont，和网络图片<br/>
   */
  static Widget createCommonImage(dynamic imageUrl,
      {BoxFit fit,
        double scale: 1.0,
        int third: 1,
        AlignmentGeometry alignment: Alignment.center,
        LoadingErrorWidgetBuilder errorWidget,
        PlaceholderWidgetBuilder placeholder,
        double width,
        Color color,
        double size,
        IconData icon,
        double height})
  {
    Widget image;
    if (icon != null || imageUrl.length < DigitValueConstant.APP_DIGIT_VALUE_10)
    {
      image = new Icon(
        icon ?? IconData(int.parse(imageUrl), fontFamily: IconFont.getFamily()),
        color: color, size: size,);
    }
    else if (imageUrl.contains(BasSoftwareConstant.SOFTWARE_ASSETS))
    {
      image = new Image(
        fit: fit,
        alignment: alignment,
        width: width,
        height: height,
        color: color,
        image: AssetImage(imageUrl),
      );
    }
    else
    {
      if (third == DigitValueConstant.APP_DIGIT_VALUE_1)
      {
        image = new CachedNetworkImage(
          fit: fit,
          alignment: alignment,
          imageUrl: imageUrl,
          color: color,
          errorWidget: errorWidget ?? (BuildContext context, String url, Object error)
          {
            return Image(
              image: AssetImage('assets/images/ic_default.png'),
            );
          },
          placeholder: placeholder ?? (BuildContext context, String url)
          {
            return Image(
              image: AssetImage('assets/images/ic_default.png'),
            );
          },
        );
      }
      else
      {
        image = FadeInImage(
          placeholder: AssetImage('assets/images/ic_default.png'),
          image: new NetworkImage(
            imageUrl,
            scale: scale,
          ),
          fit: fit,
          alignment: alignment,
          width: width,
          height: height,);
      }
    }
    return image;
  }

  /*
   * 创建公用附件组件
   */
  static Widget createCommonDuplicate(List<Widget> childrens,
      {String text = UiStringRes.duplicate, int nullable})
  {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Container(
          margin: EdgeInsets.only(
            left: MarginPaddingHeightConstant.APP_MARGIN_PADDING_15,
            top: MarginPaddingHeightConstant.APP_MARGIN_PADDING_15,
          ),
          child: Align(
              alignment: Alignment.centerLeft, child: Text.rich(new TextSpan(
              text: nullable == DigitValueConstant.APP_DIGIT_VALUE_1
                  ? "*"
                  : null,
              style: UiTextStyleRes.text_red_color_text1_small,
              children: [
                new TextSpan(
                    text: text, style: UiTextStyleRes.text_color_text1_small),
                new TextSpan(
                    text: UiStringRes.duplicate_delete, style:  TextStyle(fontSize: 10, color: Colors.red)),
                new TextSpan(
                    text: BasStringValueConstant.STR_VALUE_COLON, style: UiTextStyleRes.text_color_text1_small),
              ]))),
          height: MarginPaddingHeightConstant.APP_MARGIN_PADDING_40,
        ),
        createCommonWrapGridView(childrens)
      ],
    );
  }

  /*
   * 创建公有伸展的不允许滑动的GridView组件
   */
  static Widget createCommonWrapGridView(List<Widget> children)
  {
    return new GridView.count(
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        crossAxisCount: DigitValueConstant.APP_DIGIT_VALUE_4,
        padding: EdgeInsets.fromLTRB(
            MarginPaddingHeightConstant.APP_MARGIN_PADDING_15,
            MarginPaddingHeightConstant.APP_MARGIN_PADDING_0,
            MarginPaddingHeightConstant.APP_MARGIN_PADDING_15,
            MarginPaddingHeightConstant.APP_MARGIN_PADDING_0),
        children: children);
  }
}
