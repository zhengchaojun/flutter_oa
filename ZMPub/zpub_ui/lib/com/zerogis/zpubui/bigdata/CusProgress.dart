import 'dart:async';

import 'package:flutter/material.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/zpub_bas.dart';

import '../constant/UIBigDataKeyConstant.dart';

/*
 * 进度条 <br/>
 * @param {Object 必传} initParam['text']   文本
 * @param {Object 必传} initParam['ratio']  比率
 * @param {Object 选传} initParam['speed']  速度
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CusProgress extends StatefulWidget
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> initParam;

  CusProgress({
    this.initParam,
    Key key,
  }) : super(key: key);

  @override
  CusProgressState createState()
  => CusProgressState();
}

class CusProgressState extends State<CusProgress>
{
  double value = 50.0;

  Timer timer;

  void initState()
  {
    super.initState();
    startUpload();
  }

  startUpload()
  {
    timer?.cancel();
    setState(()
    {
      value = 0;
    });
    timer = Timer.periodic(Duration(milliseconds: 100), (Timer time)
    {
      if (value >= double.parse(widget.initParam[UIBigDataKeyConstant.RATIO]))
      {
        timer?.cancel();
        timer = null;
        return;
      }
      setState(()
      {
        value = value + (widget.initParam['speed'] == null ? 1 : widget.initParam['speed']);
      });
    });
  }

  @override
  void dispose()
  {
    super.dispose();
    timer?.cancel();
  }

  @override
  Widget build(BuildContext context)
  {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SafeArea(child: Text(widget.initParam[UIBigDataKeyConstant.TEXT],style: TextStyle(
              fontFamily: 'SourceHanSansCN-Regular',
              fontSize: BasSizeRes.middle,
              color: const Color(0xff666666),
            ),overflow: TextOverflow.ellipsis,),),

            Text.rich(
              TextSpan(
                style: TextStyle(
                  fontFamily: 'SourceHanSansCN-Regular',
                  fontSize: BasSizeRes.middle,
                  color: const Color(0xff222222),
                ),
                children: [
                  TextSpan(
                    text: '${widget.initParam[UIBigDataKeyConstant.RATIO]}%',
                  ),
                ],
              ),
              textAlign: TextAlign.right,
            )
          ],
        ),
        SizedBox(
          height: 5,
        ),
        WeProgress(
            value: value,
            height: BasSizeRes.small_middle,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0), color: Color(0xffebebeb)),
            higDecoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0), color: Color(0xff26a2ff)))
      ],
    );
  }
}
