import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_ui/com/zerogis/zpubui/constant/UIBigDataKeyConstant.dart';

/*
 * 折叠模版(!基本信息) <br/>
 * @param {Object 必传} initParam['collapse'] collapse       折叠集合List
 * @param {Object 选传} initParam['expandIndex']             展开第几个
 * @param {Object 必传} initParam['0']['text']              总投资
 * @param {Object 选传} initParam['0']['fontSize']          大小
 * @param {Object 选传} initParam['0']['color']             颜色：默认=0xff222222
 * @param {Object 选传} initParam['0']['leftColor']         颜色：默认=0xff6485e8
 * @param {Object 必传} initParam['0']['content']            内容：折叠的孩子布局
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CollapseModel extends StatefulWidget
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> initParam;

  CollapseModel({
    this.initParam,
    Key key,
  }) : super(key: key);

  @override
  CollapseModelState createState() => CollapseModelState();
}

class CollapseModelState extends State<CollapseModel>
{
  @override
  Widget build(BuildContext context)
  {
    List<WeCollapseItem> options = [];

    widget.initParam[UIBigDataKeyConstant.COOLAPSE].forEach((item)
    {
      options.add(WeCollapseItem(
          title: Row(
            children: <Widget>[
              Container(
                width: 4.0,
                height: item[UIBigDataKeyConstant.FONT_SIZE] ?? BasSizeRes.textview_text_small_middle,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.0),
                  color: Color(item[UIBigDataKeyConstant.LEFT_COLOR] ?? 0xff6485e8),
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                '${item[UIBigDataKeyConstant.TEXT]}',
                style: TextStyle(
                  fontFamily: 'SourceHanSansCN-Regular',
                  fontSize: item[UIBigDataKeyConstant.FONT_SIZE] ?? BasSizeRes.textview_text_small_middle,
                  color: Color(item[UIBigDataKeyConstant.COLOR] ?? 0xff222222),
                ),
                textAlign: TextAlign.left,
              )
            ],
          ),
          child: item[UIBigDataKeyConstant.CONTENT]??Text('内容')));
    });

    return WeCollapse(
      children: options,
      defaultActive: widget.initParam[UIBigDataKeyConstant.EXPEND_INDEX],
    );
  }
}
