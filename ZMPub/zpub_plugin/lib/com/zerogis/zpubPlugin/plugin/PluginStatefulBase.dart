import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasMapKeyConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/constant/InitParamKeyConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/font/IconFont.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/util/ParamInitUtil.dart';

import 'PullToRefreshPluginState.dart';

/*
 * 插件基类 <br/>
 * plugin对象可以传，可以不传
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
abstract class PluginStatefulBase extends StatefulWidget
{
  /*
   * 插件表中对应子插件的一条数据
   */
  Plugin plugin;

  PluginStatefulBase({Key key, @required this.plugin}) : super(key: key)
  {
    if (plugin == null)
    {
      List<Plugin> list = InitSvrMethod.getInitSvrPlugins();
      Iterator<Plugin> iterator = list.iterator;
      while (iterator.moveNext())
      {
        if (iterator.current.classurl == runtimeType.toString())
        {
          this.plugin = iterator.current;
          break;
        }
      }
    }
  }
}

/*
 * 页面功能 <br/>
 */
abstract class PluginBaseState<T extends PluginStatefulBase> extends PullToRefreshPluginState<T>
{
  /*
   * 组件名称
   */
  List<dynamic> mWidgetNameList = <dynamic>[];

  /*
   * 初始化参数
   */
  Map<String, dynamic> mInitParaMap = {};

  /*
   * initpara插件集合
   */
  List<dynamic> mPlugins = <dynamic>[];

  /*
   * 禁用集合
   */
  Map<String, dynamic> mDisableds = {};

  /*
   * 当前属性所有的组件
   */
  Map<GlobalKey<State<StatefulWidget>>, Widget> mChildrenItem = {};

  /*
   * 通用参数孩子组件的或者下一个组件的初始化参数
   */
  Map<String, dynamic> mChildUniversalInitPara = {};

  /*
   * 参数初始化工具类
   */
  ParamInitUtil mParamInitUtil = new ParamInitUtil();

  /*
   * 是否使用入栈操作插件默认入栈
   */
  bool usePushStack()
  {
    if (CxTextUtil.isEmptyMap(mInitParaMap))
    {
      return super.usePushStack();
    }
    else
    {
      return mInitParaMap[
      InitParamKeyConstant.PUSH_STACK] ==
          DigitValueConstant.APP_DIGIT_VALUE_0
          ? false
          : true;
    }
  }

  /*
   * 是否启用下拉刷新默认启用
   */
  bool useRefresh()
  {
    if (CxTextUtil.isEmptyMap(mInitParaMap))
    {
      return super.useRefresh();
    }
    else
    {
      return mInitParaMap[
      InitParamKeyConstant.REFRESH] ==
          DigitValueConstant.APP_DIGIT_VALUE_0
          ? false
          : true;
    }
  }

  /*
   * 是否启用上拉加载更多默认启用
   */
  bool useLoadMore()
  {
    if (CxTextUtil.isEmptyMap(mInitParaMap))
    {
      return super.useLoadMore();
    }
    else
    {
      return mInitParaMap[
      InitParamKeyConstant.LOADMORE] ==
          DigitValueConstant.APP_DIGIT_VALUE_0
          ? false
          : true;
    }
  }

  /*
   * 构造方法
   */
  PluginBaseState()
  {}

  void initState()
  {
    super.initState();
    init();
  }

  Widget build(BuildContext context)
  {
    Widget body;
    if (!CxTextUtil.isEmptyMap(mInitParaMap))
    {
      int widtype = mInitParaMap[InitParamKeyConstant.WIDGET_TYPE];
      if (widtype == DigitValueConstant.APP_DIGIT_VALUE_1)
      {
        body = createWidgetsListView(context);
      }
      else if (widtype == DigitValueConstant.APP_DIGIT_VALUE_2)
      {
        body = createWidgetsCardListView(context);
      }
      else if (widtype == DigitValueConstant.APP_DIGIT_VALUE_3)
      {
        body = createWidgetsRefresh(context);
      }
    }
    return buildBody(context, body);
  }

  /*
   * 初始化相关
   */
  void init()
  {
    initView();
    initData();
    initListener();
  }

  /*
   * 初始化视图相关
   */
  void initView()
  {
    if (!CxTextUtil.isEmpty(widget.plugin.getIcon()))
    {
      // 设置左边按钮
      setLeadingButtonFromIcon(
          new IconData(int.parse(widget.plugin.getIcon()),
              fontFamily: IconFont.getFamily()));
    }
    if (!CxTextUtil.isEmpty(widget.plugin.getTitlec()) && CxTextUtil.isEmpty(mTitle))
    {
      mTitle = widget.plugin.getTitlec();
    }
  }

  /*
   * 初始化数据相关
   */
  void initData()
  {
    queryInitPara();
    queryInitParaPlugins();
    queryWidgetsList();
    initTitleProgressBar();
  }

  /*
   * 初始化监听相关
   */
  void initListener()
  {
  }

  /*
   * 初始化组件参数名称
   */
  void queryWidgetsList()
  {
    if (!CxTextUtil.isEmptyMap(mInitParaMap))
    {
      mWidgetNameList = mInitParaMap[InitParamKeyConstant.WIDGETS];
    }
  }

  /*
   * 初始化参数
   */
  void queryInitPara()
  {
    dynamic initPara = widget.plugin.getInitpara();
    if (initPara is Map)
    {
      mInitParaMap = initPara;
    }
    else if (initPara is String && !CxTextUtil.isEmpty(initPara))
    {
      mInitParaMap = json.decode(initPara);
    }
  }

  /*
   * 初始化插件参数
   */
  void queryInitParaPlugins()
  {
    if (!CxTextUtil.isEmptyMap(mInitParaMap))
    {
      mPlugins = mInitParaMap[InitParamKeyConstant.PLUGS];
    }
  }

  /*
   * 初始化titleBar和ProgressBar相关
   */
  void initTitleProgressBar()
  {
    if (CxTextUtil.isEmptyMap(mInitParaMap))
    {
      return;
    }

    int navigationBar = mInitParaMap[
    InitParamKeyConstant.NAVIGATION_BAR];
    if (navigationBar == DigitValueConstant.APP_DIGIT_VALUE_1)
    {
      // 插件是否显示底部导航栏[1=是,0=否]
      mShowNavigationBar = true;
    }
    int showProgress = mInitParaMap[
    InitParamKeyConstant.SHOW_PROGRESS];
    if (showProgress == DigitValueConstant.APP_DIGIT_VALUE_1)
    {
      // 插件是否显示进度条[1=是,0=否]
      resetProgressBar(
          text: mInitParaMap[InitParamKeyConstant
              .PROGRESS_BAR_TEXT]);
    }
    int titleBar =
    mInitParaMap[InitParamKeyConstant.TITLE_BAR];
    if (titleBar == DigitValueConstant.APP_DIGIT_VALUE_1)
    {
      // 是否隐藏标题栏[1=是,0=否]
      hideTitleBar();
    }

    int hideBackBtn =
    mInitParaMap[InitParamKeyConstant
        .HIDE_BACK_BTN];
    if (hideBackBtn == DigitValueConstant.APP_DIGIT_VALUE_1)
    {
      // 是否隐藏标题栏的返回按钮[1=是,0=否]
      hideBackButton();
    }

    String rightButtonIcon = mInitParaMap[
    InitParamKeyConstant.RIGHT_BUTTON_ICON];
    String rightButtonText = mInitParaMap[
    InitParamKeyConstant.RIGHT_BUTTON_TEXT];
    if (!CxTextUtil.isEmpty(rightButtonIcon))
    {
      // 插件中右边按钮图标
      setRightButtonFromIcon(new IconData(int.parse(rightButtonIcon),
          fontFamily: IconFont.getFamily()));
    }
    else if (!CxTextUtil.isEmpty(rightButtonText))
    {
      // 插件中右边文本
      setRightButtonFromText(rightButtonText);
    }

    String borderButtonIcon = mInitParaMap[
    InitParamKeyConstant.BORDER_BUTTON_ICON];
    String borderButtonText = mInitParaMap[
    InitParamKeyConstant.BORDER_BUTTON_TEXT];
    if (!CxTextUtil.isEmpty(borderButtonIcon))
    {
      // 插件中侧边按钮图标
      setRightButtonFromIcon(new IconData(int.parse(borderButtonIcon),
          fontFamily: IconFont.getFamily()));
    }
    else if (!CxTextUtil.isEmpty(borderButtonText))
    {
      // 插件中侧边文本
      setRightButtonFromText(borderButtonText);
    }

    String contentBackground = mInitParaMap[
    InitParamKeyConstant.CONTENT_BACKGROUND];
    if (!CxTextUtil.isEmpty(borderButtonText))
    {
      // 设置内容背景颜色
      List<String> list =
      contentBackground.split(BasStringValueConstant.STR_COMMON_COMMA);
      setContentBackgroundARGB(
          int.parse(list[DigitValueConstant.APP_DIGIT_VALUE_0]),
          int.parse(list[DigitValueConstant.APP_DIGIT_VALUE_1]),
          int.parse(list[DigitValueConstant.APP_DIGIT_VALUE_2]),
          int.parse(list[DigitValueConstant.APP_DIGIT_VALUE_3]));
    }

    String titleBarBg = mInitParaMap[
    InitParamKeyConstant.TITLE_BAR_BG];
    if (!CxTextUtil.isEmpty(titleBarBg))
    {
      // 设置标题栏背景颜色
      List<String> list =
      contentBackground.split(BasStringValueConstant.STR_COMMON_COMMA);
      setTitleBarGgARGB(
          int.parse(list[DigitValueConstant.APP_DIGIT_VALUE_0]),
          int.parse(list[DigitValueConstant.APP_DIGIT_VALUE_1]),
          int.parse(list[DigitValueConstant.APP_DIGIT_VALUE_2]),
          int.parse(list[DigitValueConstant.APP_DIGIT_VALUE_3]));
    }

    dynamic pluginitpara = mInitParaMap[
    InitParamKeyConstant.PLUG_INITPARA];
    if (!CxTextUtil.isEmptyObject(pluginitpara))
    {
      // 设置配置孩子组件,插件初始化参数到内存中
      mChildUniversalInitPara.addAll(pluginitpara);
    }
  }

  /*
   * 创建listview组件
   */
  Widget createWidgetsListView(BuildContext context)
  {
    return new ListView(children: createChildrenWidget());
  }

  /*
   * 创建listview带有卡片组件
   */
  Widget createWidgetsCardListView(BuildContext context)
  {
    return new Card(child: new ListView(children: createChildrenWidget()));
  }

  /*
   * 创建下拉刷新带有listview组件
   */
  Widget createWidgetsRefresh(BuildContext context)
  {
    return buildBodyWithRefresh(
        context,
        new ListView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: createChildrenWidget()));
  }

  /*
   * 创建孩子组件集合的条目
   */
  List<Widget> createChildrenWidget()
  {
    List<Widget> list = <Widget>[];
    mWidgetNameList.forEach((widgetName)
    {
      list.add(WidgetsFactory.getInstance().get(widgetName).runWidget(
          initPara: mParamInitUtil.isEmptyMap()
              ? mChildUniversalInitPara
              : mParamInitUtil.getChildNextInitPara(widgetName)));
    });
    return list;
  }

  /*
   * 创建孩子组件集合的条目
   */
  List<Widget> createChildrenKeyWidget()
  {
    mChildrenItem.clear();
    mWidgetNameList.forEach((widgetName)
    {
      GlobalKey key = new GlobalKey<State<StatefulWidget>>();
      dynamic initPara = null;
      if (mParamInitUtil.isEmptyMap())
      {
        mChildUniversalInitPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY] = key;
        initPara = mChildUniversalInitPara;
      }
      else
      {
        initPara = mParamInitUtil.getChildNextInitPara(widgetName);
        initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY] = key;
      }
      mChildrenItem[key] =
          WidgetsFactory.getInstance().get(widgetName).runWidget(
              initPara: initPara);
    });
    return mChildrenItem.values.toList();
  }

  /*
   * 启动初始化参数中的插件
   * @param index 对应插件的位置
   */
  Future<T> runPlugin<T extends Object>(int index)
  {
    String pluginName = (mPlugins[index] as Map) [BasMapKeyConstant.MAP_KEY_NAME];
    return PluginsFactory.getInstance().get(pluginName).runPlugin(
        this, initPara: mParamInitUtil.isEmptyMap()
        ? mChildUniversalInitPara
        : mParamInitUtil.getChildNextInitPara(pluginName));
  }

  /*
   * 启动初始化参数中的插件
   * @param pluginName 插件名称
   */
  Future<T> runPluginName<T extends Object>(String pluginName)
  {
    return PluginsFactory.getInstance().get(pluginName).runPlugin(
        this, initPara: mParamInitUtil.isEmptyMap()
        ? mChildUniversalInitPara
        : mParamInitUtil.getChildNextInitPara(pluginName));
  }

  /*
   * 启动初始化参数中的插件
   * @param pluginName 插件名称
   */
  Future<T> runPluginParam<T extends Object>(String pluginName, dynamic initPara)
  {
    return PluginsFactory.getInstance().get(pluginName).runPlugin(this, initPara: initPara);
  }

  /*
   * 启动初始化参数中的插件模态对话框
   * @param valueChangedMethod 回调事件：举例：模态对话框中的点击事件需要调用当前界面的方法
   */
  Future<T> runPluginModel<T extends Object>(Object valueChangedMethod)
  {
    Map<String, Object> map = mInitParaMap[InitParamKeyConstant
        .PLUG_MODEL];
    return runPluginModels(map, valueChangedMethod);
  }


  /*
   * 启动初始化参数中的插件模态对话框
   * @param map 模态对话框值
   * @param valueChangedMethod 回调事件：举例：模态对话框中的点击事件需要调用当前界面的方法
   */
  Future<T> runPluginModels<T extends Object>(Map<String, Object> map, Object valueChangedMethod)
  {
    mChildUniversalInitPara[BasMapKeyConstant.MAP_KEY_VALUE_CHANGE_METHOD] =
        valueChangedMethod;
    return showDialog<T>(
        context: context,
        child: new SimpleDialog(
          title: Center(child: new Text(map[BasMapKeyConstant.MAP_KEY_VALUE_TITLE]),),
          children: <Widget>[
            WidgetsFactory.getInstance()
                .get(map[BasMapKeyConstant.MAP_KEY_WIDGET])
                .runWidget(
                initPara: mChildUniversalInitPara),
          ],
        ));
  }

  /*
   * 启动初始化参数中的插件模态对话框
   * @param map 模态对话框值
   * @param title 标题：模态对话框名称
   * @param children 孩子组件
   *
   * @param plugin 插件：名称
   * @param widget 组件：组件名称
   * @param valueChangedMethod 回调事件：举例：模态对话框中的点击事件需要调用当前界面的方法
   */
  Future<T> runModelParam<T extends Object>(Map<String, Object> param)
  {
    return showDialog<T>(
        context: context,
        child: new SimpleDialog(
          title: Center(child: Text(param[BasMapKeyConstant.MAP_KEY_VALUE_TITLE]),),
          children: param[BasMapKeyConstant.MAP_KEY_CHILDREN] ?? <Widget>[
            WidgetsFactory.getInstance().get(param[BasMapKeyConstant.MAP_KEY_WIDGET]).runWidget(initPara: param,),
          ],
        ));
  }

  /*
   * 加入孩子通用初始化参数中：启动孩子组件，插件的时候会将参数带入进去给孩子组件，插件
   * @param key 键
   * @param value 值
   */
  void putChildParaInit(String key, dynamic value)
  {
    mChildUniversalInitPara[key] = value;
  }

  /*
   * 根据数据库的配置构造孩子组件需要哪些参数：将填充进来的参数转换成 孩子组件，插件需要的Map集合
   */
  void convertMap()
  {
    if (!CxTextUtil.isEmptyMap(mInitParaMap) &&
        !CxTextUtil.isEmptyMap(mChildUniversalInitPara))
    {
      if (!CxTextUtil.isEmptyMap(mInitParaMap[
      InitParamKeyConstant.CHILD_INITPARA]))
      {
        mParamInitUtil.convertMap(mInitParaMap, mChildUniversalInitPara);
      }
      else
      {
        showToast(
            InitParamKeyConstant.CHILD_INITPARA);
      }
    }
  }

  /*
   * 设置禁用状态
   * @param key 禁用的key
   * @param value 禁用的值
   */
  void createDis(String key, dynamic value)
  {
    mDisableds[key] = value;
  }

  /*
   * 设置禁用状态
   * @param key 禁用的key
   * @param value 禁用的值
   */
  dynamic createDisNotNull(String key, dynamic value)
  {
    dynamic item = mDisableds[key];
    bool isEmpty = CxTextUtil.isEmptyObject(item);
    if (isEmpty)
    {
      mDisableds[key] = value;
    }
    return isEmpty ? value : item;
  }

  /*
   * 更新禁用状态
   * @param key 禁用的key
   * @param value 禁用的值
   */
  void updateDisAll(dynamic value)
  {
    mDisableds.keys.toList().forEach((key)
    {
      createDis(key, value);
    });
  }

  /*
   * 查找禁用状态控件
   * @param value 禁用的key
   */
  queryDis(String key)
  {
    return mDisableds[key];
  }

  /*
   * 提前产生GlobalKey
   * @param num  数量
   */
  createGlobalKeys(int num)
  {
    for (int i = 0; i < num; i ++)
    {
      mChildrenItem[GlobalKey<State<StatefulWidget>>()] = null;
    }
  }
}
