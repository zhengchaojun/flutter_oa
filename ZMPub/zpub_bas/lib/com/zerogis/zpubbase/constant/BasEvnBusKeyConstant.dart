/*
 * 类描述：evnetbus事件key定义对象
 * 规则：zpub层之间发送消息，或者zpub层向app层发送消息，统一定义在zpubbas层
 *
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class BasEvnBusKeyConstant
{
  /*
   * att层向zpub_repair_order发消息
   * 办理成功-待办箱刷新
   */
  static const int BAS_COM_TASK_REFRESH = 99;

  /*
   * 发送list集合信息
   */
  static const int BAS_COM_LIST = 98;
}
