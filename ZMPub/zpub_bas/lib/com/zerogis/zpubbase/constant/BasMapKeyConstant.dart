/*
 * 类描述：数据库map集合的key，value定义对象
 * 作者：郑朝军 on 2018/7/27
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2018/7/27
 * 修改备注：
 */
class BasMapKeyConstant
{
  /*
   * ----------------------常用-----------------------------------------------------
   */
  static const String MAP_KEY = "map"; // map
  static const String MAP_KEY_USERID = "userid"; // 用户ID
  static const String MAP_KEY_USERNAME = "username"; // 用户ID
  static const String MAP_KEY_DEPARTMENT = "department"; // 用户部门
  static const String MAP_ICON = "icon"; // 图标
  static const String MAP_ARGB = "argb"; // argb
  static const String MAP_KEY_OBJECT = "object"; // 对象
  static const String MAP_KEY_LIST = "list"; //列表
  static const String MAP_KEY_TEXT = "text"; //列表
  static const String MAP_KEY_VALUE = "value"; //列表
  static const String MAP_KEY_CONFIRM = "confirm"; //是否确认
  static const String MAP_KEY_CONTENT = "content"; //内容
  static const String MAP_KEY_ID = "id"; // id
  static const String MAP_KEY_GRA = "gra"; // 图形
  static const String MAP_KEY_ATT = "att"; // 属性
  static const String MAP_KEY_ENTITY = "entity"; // entity表
  static const String MAP_KEY_TOTAL = "total"; // 条数
  static const String MAP_KEY_PAGER = "pager"; // 条数
  static const String MAP_KEY_DATA = "data"; // 数据
  static const String MAP_KEY_DATE = "date"; // 日期
  static const String MAP_KEY_WIDGET = "widget"; // 组件
  static const String MAP_KEY_SIZE = "size"; // 大小
  static const String MAP_KEY_PATH = "path"; // 路径
  static const String MAP_KEY_POSITION = "position"; // 位置
  static const String MAP_KEY_FIRST = "first"; // 第一
  static const String MAP_KEY_SECOND = "second"; // 第二
  static const String MAP_KEY_THREE = "three"; // 第三
  static const String MAP_KEY_ADDRESS = "address"; // 地址
  static const String MAP_KEY_RET = "ret"; // 返回
  static const String MAP_KEY_TYPE = "type"; // 类型
  static const String MAP_KEY_NAME = "name"; // 名称
  static const String MAP_KEY_NAMEC = "namec"; // 名称
  static const String MAP_KEY_INDEX = "index"; // 位置
  static const String MAP_KEY_PLUGIN = "plugin"; // 插件
  static const String MAP_KEY_ITEM = "item"; // 条目
  static const String MAP_KEY_RESULT = "result"; // 结果
  static const String MAP_KEY_VALUE_TITLE = "title"; // 标题
  static const String MAP_KEY_ISEDIT = "isEdit"; // 是否编辑[1=编辑,0=不编辑]
  static const String MAP_KEY_BUTTONS = "buttons"; // 按钮
  static const String MAP_KEY_INITPARA = "initpara"; // 初始化参数
  static const String MAP_KEY_GLOBAL_KEY = "GlobalKey"; // 全局KEY
  static const String MAP_KEY_VALUE_CHANGE_METHOD = "valueChangedMethod"; // 回调事件KEY
  static const String MAP_KEY_TEXT_EDITING = "textEditing"; // 文本输入法控制器
  static const String MAP_KEY_AUDIO = "audio"; // 音频
  static const String MAP_KEY_COLOR = "color"; // 颜色
  static const String MAP_KEY_COLORS = "colors"; // 颜色
  static const String MAP_KEY_NULLABLE = "nullable"; // 是否可空
  static const String MAP_KEY_COUNT = "count"; // 数量
  static const String MAP_KEY_TAG = "tag"; // 标志
  static const String MAP_KEY_SELECT = "select"; // 选中
  static const String MAP_KEY_WIDTH = "width"; // 宽度
  static const String MAP_KEY_HEIGHT = "height"; // 高度
  static const String MAP_KEY_STYLE = "style"; // 样式
  static const String MAP_KEY_LIST_ITEM = "listitem"; // 条目
  static const String MAP_KEY_FROM = "from"; // 来自
  static const String MAP_KEY_TABLE = "table"; // 表格
  static const String MAP_KEY_VARIABLE = "variable"; // 变量
  static const String MAP_KEY_FROMKEY = "formKey"; // 变量
  static const String MAP_KEY_MESSAGE = "message"; // 变量
  static const String MAP_KEY_TIME = "time"; // 变量
  static const String MAP_KEY_NO = "no"; // 变量
  static const String MAP_KEY_SUBTITLE = "subtitle"; // 副标题
  static const String MAP_KEY_CHILDREN = "children"; // 变量
  static const String MAP_KEY_URL = "url"; // 地址
  /*
   * ----------------------常用-----------------------------------------------------
   */
  static const String MAP_KEY_FLD = "fld"; //fld字段
  static const String MAP_KEY_FLD2 = "fld2"; //fld字段
  static const String MAP_KEY_FLDVALUE = "fldValue"; //fldValue字段
  static const String MAP_KEY_DBVALUE = "DbValue"; //DbValue字段
  static const String MAP_KEY_DISPE = "dispe"; //dispe字段
  static const String MAP_KEY_MAIN_ATT = "mainAtt"; // 主表属性
  static const String MAP_KEY_CHILD_ATT = "childAtt"; // 孩子属性
  static const String MAP_KEY_PARAMS = "params"; // 参数
}
