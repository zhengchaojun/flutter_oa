import 'dart:ui';

import 'package:flutter/material.dart';

/*
 * 功能：颜色类似Android资源dimen.xml文件相关
 * 设置透明度：backgroundColor.withAlpha(112)
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class BasSizeRes
{
//  <dimen name="fab_margin">16dp</dimen>
//  <dimen name="actionBarSize">45dp</dimen>
//  <!-- 分界线 -->
//  <dimen name="dimen_right_button_text">15sp</dimen>
//
//  <!-- 分界线 -->
//  <dimen name="dimen_grid_item_text">15sp</dimen>
//  <dimen name="dimen_list_item_text">16sp</dimen>
//
//  <!-- 分界线 -->
//  <dimen name="rect_activity_top">0sp</dimen>
//  <dimen name="rect_activity_bottom">0sp</dimen>
//  <dimen name="rect_activity_left">0sp</dimen>
//  <dimen name="rect_activity_right">0sp</dimen>
//
//  <!-- 分界线 -->
//  <dimen name="slidingmenu_offset">60dp</dimen>
//  <dimen name="list_padding">10dp</dimen>
//  <dimen name="shadow_width">15dp</dimen>
//
//  <!-- 分界线 -->
//  <dimen name="dimen_smallest">2dp</dimen>
//  <dimen name="dimen_smaller">3dp</dimen>
//  <dimen name="dimen_small">5dp</dimen>
//  <dimen name="dimen_small_middle">7dp</dimen>
//  <dimen name="dimen_middle">10dp</dimen>
//  <dimen name="dimen_middle_large">12dp</dimen>
//  <dimen name="dimen_large">15dp</dimen>
//  <dimen name="dimen_larger">17dp</dimen>
//  <dimen name="dimen_largest">20dp</dimen>
//
//  <!-- 分界线 -->
//  <dimen name="dimen_tabwidget_text">12sp</dimen>
//  <dimen name="dimen_textview_text">16sp</dimen>
//  <dimen name="dimen_textview_text_small">14sp</dimen>
//  <dimen name="dimen_textview_text_smaller">13sp</dimen>
//  <dimen name="dimen_textview_text_small_middle">16sp</dimen>
//  <dimen name="dimen_textview_text_small_middler">17sp</dimen>
//  <dimen name="dimen_textview_text_smallest">12sp</dimen>
//  <dimen name="dimen_textview_text_middle">18sp</dimen>
//  <dimen name="dimen_textview_text_large">19sp</dimen>
//  <dimen name="dimen_edittext_text">15sp</dimen>
//  <dimen name="dimen_button_text">12sp</dimen>
//  <dimen name="dimen_title_text">18sp</dimen>


  // <!-- 分界线 -->
  static const double right_button_text = 10;

  //  <!-- 分界线 -->
  static const double grid_item_text = 10;
  static const double list_item_text = 10;

  // <!-- 分界线 -->
  static const double rect_activity_top = 10;
  static const double rect_activity_bottom = 10;
  static const double rect_activity_left = 10;
  static const double rect_activity_right = 10;

//  <!-- 分界线 -->
  static const double slidingmenu_offset = 10;
  static const double list_padding = 10;
  static const double shadow_width = 10;


//  <!-- 分界线 -->
  static const double smallest = 2;
  static const double smaller = 5;
  static const double small_middle = 10;
  static const double middle = 15;
  static const double middle_large = 20;
  static const double large = 25;
  static const double larger = 30;
  static const double largest = 35;


//  <!-- 分界线 -->
  static const double tabwidget_text = 10;
  static const double textview_text = 10;
  static const double textview_text_smallest = 8;
  static const double textview_text_small = 15;
  static const double textview_text_smaller = 18;
  static const double textview_text_small_middle = 15.5;
  static const double textview_text_small_middler = 22;
  static const double textview_text_middle = 24;
  static const double textview_text_large = 28;
  static const double edittext_text = 10;
  static const double button_text = 10;
  static const double title_text = 10;
}
