import 'package:zpub_bas/zpub_bas.dart';

/*
 * 类描述：eventbus传递对象
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class BasMsgEvent
{
  int m_iType;
  Map<String, dynamic> m_Messages;

  BasMsgEvent(int type)
  {
    this.m_iType = type;
    this.m_Messages = new Map();
  }

  int getType()
  {
    return this.m_iType;
  }

  void setType(int type)
  {
    this.m_iType = type;
  }

  Map<String, Object> getMessages()
  {
    return this.m_Messages;
  }

  void put(String key, dynamic message)
  {
    if (!CxTextUtil.isEmpty(key) && message != null)
    {
      this.m_Messages[key] = message;
    }
  }

  dynamic get(String key)
  {
    return this.m_Messages[key];
  }
}
