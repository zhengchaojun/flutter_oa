import 'dart:convert';

/*
 * 类描述：对象工具类
 * 作者：郑朝军 on 2019/7/4
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/7/4
 * 修改备注：
 */
class ObjUtil
{
  /*
   * 深度拷贝
   * @param {Object} obj 深度拷贝对象
   */
  static dynamic assign(dynamic obj)
  {
    return json.decode(json.encode(obj));
  }
}
