import 'package:common_utils/common_utils.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';

/*
 * 类描述：时间相关工具类
 * 作者：郑朝军 on 2019/5/16
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/16
 * 修改备注：
 */
class TimeUtil
{
  /*
   * 1：判断当前时间在时间hhmmss[当前规定打卡时间]之前吗？
   *
   * @param hhmmss 字符串时间= 09:00[当前规定打卡时间]
   * @return （true 当前时间在规定打卡时间之前）举例：打卡时间为9：00 现在手机时间为8:30 当前时间8:30在9:00之前吗？等于true
   */
  static bool isNowBeforeHHmmss(String hhmmss)
  {
    String date = DateUtil.getDateStrByTimeStr(DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY) +
        BasStringValueConstant.STR_VALUE_SPACE +
        hhmmss;
    DateTime dateTime = DateUtil.getDateTime(date);
    return DateTime.now().isBefore(dateTime);
  }

  /*
   * 1：判断afTimeHhmmss时间在beTimehhmmss时间[当前规定打卡时间]之前吗？
   *
   * @param afTimeHhmmss = 2019-05-20 08:30
   * @param beTimehhmmss = 2019-05-10 08:30
   * @return （true afTimeHhmmss时间在beTimehhmmss时间之前）举例：打卡时间为9：00 现在手机时间为8:30 当前时间8:30在9:00之前吗？等于true
   */
  static bool isNowBeforeTimeHHmmss(String afTimeHhmmss, String beTimehhmmss)
  {
    DateTime afDateTime = DateUtil.getDateTime(afTimeHhmmss);
    DateTime bfDateTime = DateUtil.getDateTime(beTimehhmmss);
    return afDateTime.isBefore(bfDateTime);
  }

  /*
   * 1：判断给定时间在时间hhmmss[当前规定打卡时间]之前吗？
   *
   * @param timeHhmmss 时间 = 2019-05-20 08:30
   * @param hhmmss 字符串时间= 09:00[当前规定打卡时间]
   * @return （true 当前时间在规定打卡时间之前）举例：打卡时间为9：00 现在手机时间为8:30 当前时间8:30在9:00之前吗？等于true
   */
  static bool isTimeBeforeHHmmss(String timeHhmmss, String hhmmss)
  {
    String date = DateUtil.getDateStrByTimeStr(DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY) +
        BasStringValueConstant.STR_VALUE_SPACE +
        hhmmss;
    DateTime dateTime = DateUtil.getDateTime(date);
    DateTime time = DateUtil.getDateTime(timeHhmmss);
    return time.isBefore(dateTime);
  }

  /*
   * 1：判断给定时间在时间hhmmss[当前规定打卡时间]之前吗？
   *
   * @param timeHhmmss 时间 = 08:30
   * @param hhmmss 字符串时间= 09:00[当前规定打卡时间]
   * @return （true 当前时间在规定打卡时间之前）举例：打卡时间为9：00 现在手机时间为8:30 当前时间8:30在9:00之前吗？等于true
   */
  static bool isHhMsBeforeHHmmss(String hhmmss, String workHhmmss)
  {
    workHhmmss = DateUtil.getDateStrByTimeStr(DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY) +
        BasStringValueConstant.STR_VALUE_SPACE +
        workHhmmss;
    hhmmss = DateUtil.getDateStrByTimeStr(DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY) +
        BasStringValueConstant.STR_VALUE_SPACE +
        hhmmss;

    DateTime dateTime = DateUtil.getDateTime(workHhmmss);
    DateTime time = DateUtil.getDateTime(hhmmss);

    return time.isBefore(dateTime) || time.isAtSameMomentAs(dateTime);
  }

  /*
   * 判断当前日期是否在给定日期之前
   * @param date 日期 = 2019-05-18 02:00
   * @return 【true 给定时间(date)在当前时间之前,可以上下班打卡】
   */
  static bool isNowBeforeData(String date)
  {
    DateTime dateTime = DateUtil.getDateTime(date);
    DateTime currentTimes = DateUtil.getDateTime(DateUtil.getDateStrByTimeStr(
        DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY));
    return dateTime.isBefore(currentTimes);
  }

  /*
   * 判断当前日期是否在给定日期之前或等于
   *
   * @param date 日期 = 2019-05-18
   * @return 【true 给定时间(date)在当前时间之前或等于,可以上下班打卡】
   */
  static bool isNowBeforeYYYYMMdd(String yyyyMMdd)
  {
    DateTime dateTime = DateUtil.getDateTime(yyyyMMdd);
    DateTime currentTimes = DateUtil.getDateTime(DateUtil.getDateStrByTimeStr(
        DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY));
    return dateTime.isBefore(currentTimes) ||
        dateTime.isAtSameMomentAs(currentTimes);
  }

  /*
   * 判断当前日期是否在给定日期之前或等于
   *
   * @param yyyyMMdd 日期 = 2019-05-18
   * @param yyyyMMdd2 日期 = 2019-05-19
   * @return 【true 给定时间(yyyyMMdd)在当前时间(yyyyMMdd2)之前或等于】
   */
  static bool isBeforeYYYYMMdd(String yyyyMMdd, String yyyyMMdd2)
  {
    DateTime dateTime = DateUtil.getDateTime(yyyyMMdd);
    DateTime currentTimes = DateUtil.getDateTime(yyyyMMdd2);
    return dateTime.isBefore(currentTimes) ||
        dateTime.isAtSameMomentAs(currentTimes);
  }

  /*
   * 比较时间差
   * @param yyyyMMddHhMmStart 开始时间
   * @param yyyyMMddHhMmEnd 结束时间
   * @return Duration对象
   */
  static Duration difference(String yyyyMMddHhMmStart, String yyyyMMddHhMmEnd)
  {
    DateTime startTime = DateUtil.getDateTime(yyyyMMddHhMmStart);
    DateTime entTime = DateUtil.getDateTime(yyyyMMddHhMmEnd);
    return startTime.difference(entTime);
  }

  /*
   * 在给定日期添加多少年月日时分秒
   */
  static String add(String data, {Duration duration, int year, int month, String format})
  {
    DateTime dateTime = DateUtil.getDateTime(data);
    return addDateTime(dateTime, duration: duration, year: year, month: month, format: format);
  }

  /*
   * 在当前日期添加多少年月日时分秒
   */
  static String addNow({Duration duration, int year, int month, String format})
  {
    DateTime dateTime = DateTime.now();
    return addDateTime(dateTime, duration: duration, year: year, month: month, format: format);
  }

  /*
   * 在当前日期添加多少年月日时分秒
   */
  static String addDateTime(DateTime dateTime, {Duration duration, int year, int month, String format})
  {
    if (duration != null)
    {
      dateTime.add(duration);
    }
    if (year != null)
    {
      year = dateTime.year + year;
    }
    if (month != null)
    {
      month = dateTime.month + month;
    }
    return formatDate(
        year ?? dateTime.year,
        month ?? dateTime.month,
        dateTime.day,
        dateTime.hour,
        dateTime.minute,
        dateTime.second,
        dateTime.millisecond,
        format: format);
  }

  /*
   * 格式化日期
   */
  static String formatDateStr(String dateStr, {bool isUtc, String format}) 
  {
    return DateUtil.formatDateStr(dateStr,format: format,isUtc: isUtc);
  }
  
  /*
   * 格式化日期
   */
  static String formatDate(int year, int month, int day, int hour, int minute, int second, int millisecond,
      {String format})
  {
    format = format ?? DataFormats.full;
    if (format.contains("yy"))
    {
      String years = year.toString();
      if (format.contains("yyyy"))
      {
        format = format.replaceAll("yyyy", years);
      }
      else
      {
        format = format.replaceAll(
            "yy", years.substring(years.length - 2, years.length));
      }
    }

    format = _comFormat(month, format, 'M', 'MM');
    format = _comFormat(day, format, 'd', 'dd');
    format = _comFormat(hour, format, 'H', 'HH');
    format = _comFormat(minute, format, 'm', 'mm');
    format = _comFormat(second, format, 's', 'ss');
    format = _comFormat(millisecond, format, 'S', 'SSS');
    return format;
  }


  /*
   * 格式化日期
   */
  static String _comFormat(int value, String format, String single, String full)
  {
    if (format.contains(single))
    {
      if (format.contains(full))
      {
        format =
            format.replaceAll(full, value < 10 ? '0$value' : value.toString());
      }
      else
      {
        format = format.replaceAll(single, value.toString());
      }
    }
    return format;
  }

  /*
   * 根据时间差计算几天和半天 开始时间和结束时间都是上午
   * @param duration 时间差
   */
  static String differenceDay(Duration duration)
  {
    int day = duration.inDays.abs();
    double hours = duration.inHours.abs().toDouble();
    hours = hours - (24 * day);
    hours = hours <= 0 ? 0 : hours > 4 ? 1 : 0.5;
    return (day + hours).toString();
  }

  /*
   * 终止时间是周末：根据时间差计算几天和半天 开始时间和结束时间都是上午
   * @param duration 时间差
   */
  static String differenceDay2(Duration duration)
  {
    int day = duration.inDays.abs();
    return (day + 1).toString();
  }


  /*
   * 根据时间差计算几天和半天 开始时间和结束时间都是下午
   * @param duration 时间差
   */
  static String differenceDayAft(Duration duration)
  {
    int day = duration.inDays.abs();
    double hours = duration.inHours.abs().toDouble();
    hours = hours - (24 * day);
    hours = hours <= 0 ? 0 : hours > 7 ? 1 : 0.5;
    return (day + hours).toString();
  }

  /*
   * 根据时间差计算几天和半天 开始时间和结束时间都是下午
   * @param duration 时间差
   */
  static String differenceDayNum(Duration duration)
  {
    int day = duration.inDays.abs();
    return day.toString();
  }

  /*
   * 判断是否是今天
   * @param yyyyMMdd 时间 = 2019-05-20
   */
  static bool isToday(String yyyyMMdd)
  {
    DateTime old = DateUtil.getDateTime(yyyyMMdd);
    DateTime now = DateTime.now();
    return old.year == now.year && old.month == now.month && old.day == now.day;
  }


  /*
   * 判断是否是下午
   * @param timeHhmmss 时间 = 2019-05-20 08:30
   * @param standHHmmss 标准时间 13:30 考勤时间
   */
  static bool isAft(String timeHhmmss, String standHHmmss)
  {
    String standStr = DateUtil.getDateStrByTimeStr(DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY) +
        BasStringValueConstant.STR_VALUE_SPACE +
        standHHmmss;

    DateTime data = DateUtil.getDateTime(timeHhmmss);
    DateTime stand = DateUtil.getDateTime(standStr);

    if (data.hour >= stand.hour)
    {
      return true;
    }
    return false;
  }

  /*
   * 判断是否是下午
   * @param timeHhmmss 时间 = 08:30
   * @param standHHmmss 标准时间 13:30 考勤时间
   */
  static bool isAftTime(String timeHhmmss, String standHHmmss)
  {
    String standStr = DateUtil.getDateStrByTimeStr(DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY) +
        BasStringValueConstant.STR_VALUE_SPACE +
        standHHmmss;

    timeHhmmss = DateUtil.getDateStrByTimeStr(DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY) +
        BasStringValueConstant.STR_VALUE_SPACE +
        timeHhmmss;

    DateTime data = DateUtil.getDateTime(timeHhmmss);
    DateTime stand = DateUtil.getDateTime(standStr);

    if (data.hour >= stand.hour)
    {
      return true;
    }
    return false;
  }

  /*
   * 获取当前日期年月日(yyyy-MM-dd)
   */
  static String getNowDateStrYMD()
  {
    return DateUtil.getDateStrByTimeStr(DateUtil.getNowDateStr(),
        format: DateFormat.YEAR_MONTH_DAY);
  }

  /*
   * 获取当前日期年(yyyy)
   */
  static String getNowDateStrY()
  {
    DateTime dateTime = DateTime.now();
    return dateTime.year.toString();
  }

  /*
   * 获取当前上一年(yyyy)
   */
  static String getPreviousDateStrY()
  {
    DateTime dateTime = DateTime.now();
    return (dateTime.year - 1).toString();
  }

  /*
   * 计算时间差(出差)
   */
  static Duration calculatDuration(String starttime, String endtime)
  {
    DateTime endDateTime = DateUtil.getDateTime(endtime);
    DateTime startTime = DateUtil.getDateTime(starttime);
    return startTime.difference(endDateTime);
  }
}
