import 'package:flutter/material.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatMajorMinorConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatDetailPlugin.dart';
import 'package:zpub_svr/zpub_svr.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'PatSelectDayPlugin', '当天任务', '当天任务', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'PatSelectDayPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * 当天任务 <br/>
 * 需要传入的键：<br/>
 * @param {Object 必传} major  主类型
 * @param {Object 必传} minor  子类型
 * @param {Object 必传} exp    条件
 * @param {Object 必传} types  类型
 * @param {Object 必传} vals   值
 * @param {Object 必传} date   日期参数
 * @param {Object 选传} separator 分隔符
 * @param {Object 选传} map 键值对
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PatSelectDayPlugin extends AttIsMcardListBas
{
  PatSelectDayPlugin({Key key, mInitPara, plugin}) :super(key: key, mInitPara: mInitPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new PatSelectDayPluginState();
  }

  static String toStrings()
  {
    return "PatSelectDayPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class PatSelectDayPluginState extends AttIsMcardListBasState<PatSelectDayPlugin>
{
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryPage")
    {
      dealQuery(values);
    }
    else if (method == 'query')
    {
      print('values===${values}');
    }
  }

  void query(int page)
  {
    SvrAreaSvrService.queryPage(
        widget.mInitPara[DBFldConstant.FLD_MAJOR],
        widget.mInitPara[DBFldConstant.FLD_MINOR],
        widget.mInitPara[HttpParamKeyValue.PARAM_KEY_EXP],
        widget.mInitPara[HttpParamKeyValue.PARAM_KEY_TYPES],
        widget.mInitPara[HttpParamKeyValue.PARAM_KEY_VALS],
        m_pager,
        this,
        page: page,
        separator: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_SEPARATOR] ??
            BasStringValueConstant.STR_COMMON_COMMA,
        pageSize: BasSoftwareConstant.MAX_PAGER_SIZE);
  }

  /*
   * 根据名称启动插件且携带参数
   */
  @override
  Future<T> runPluginParam<T extends Object>(String pluginName, dynamic att)
  {
    mChildUniversalInitPara[DBFldConstant.FLD_MAJOR] = widget.mInitPara[DBFldConstant.FLD_MAJOR];
    mChildUniversalInitPara[DBFldConstant.FLD_MINOR] = widget.mInitPara[DBFldConstant.FLD_MINOR];
    mChildUniversalInitPara[BasMapKeyConstant.MAP_KEY_ATT] = att;
    return super.runPluginName(PatDetailPlugin.toStrings());
  }


  void onClick(Widget view)
  {
    if (view == btn_right)
    {
      SvrAreaSvrService.query(
          widget.mInitPara[DBFldConstant.FLD_MAJOR],
          PatMajorMinorConstant.MINOR_PAT_PLANREC,
          'userid=? and qsrq >=? and zzrq <=?',
          widget.mInitPara[HttpParamKeyValue.PARAM_KEY_TYPES],
          '${UserMethod.getUserId()},${ widget.mInitPara[BasMapKeyConstant.MAP_KEY_DATE]} 00:00:00,${widget
              .mInitPara[BasMapKeyConstant.MAP_KEY_DATE]} 23:59:59',
          this);
    }
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PatSelectDayPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new PatSelectDayPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new PatSelectDayPlugin();
  }
}
