import 'package:flutter/material.dart';
import 'package:mini_calendar/mini_calendar.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatMajorMinorConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatSelectDayPlugin.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatTodoTaskPlugin.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_svr/zpub_svr.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'PatCalendarPlugin', '计划任务', '计划任务', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'PatCalendarPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * 巡检任务日历页面 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 *
 */
class PatCalendarPlugin extends PluginStatefulBase
{
  PatCalendarPlugin({Key key, plugin}) :super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new PatCalendarPluginState();
  }

  static String toStrings()
  {
    return "PatCalendarPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class PatCalendarPluginState extends PluginBaseState<PatCalendarPlugin>
{
  PatCalendarPluginState()
  {
    setRightButtonFromIcon(Icons.menu);
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBody(context, Column(children: <Widget>[
      Text('5月13日 周三', style: TextStyle(
          fontSize: 30,
          color: BasColorRes.text_color_text1, fontWeight: FontWeight.w900),),
      Chip(avatar: CircleAvatar(backgroundColor: Colors.greenAccent,), label: Text('今日任务：0个'),),
      Chip(avatar: CircleAvatar(backgroundColor: Colors.redAccent,), label: Text('待办任务：0个'),),

      MonthPageView(
        padding: EdgeInsets.all(1),
        scrollDirection: Axis.horizontal,
        option: MonthOption(
          currentDay: DateDay.now(),
          enableContinuous: false,
          minDay: DateDay.now().copyWith(month: 1, day: 13),
          maxDay: DateDay.now().copyWith(month: 12, day: 21),
          enableMultiple: false,
        ),
        onDaySelected: (day, data)
        {
          runDaySelectedPlugin(day);
        },
        showWeekHead: true,
        localeType: LocaleType.zh,
      ),

    ],));
    return widget;
  }

  /*
   * 启动初始化参数中的插件
   * @param day 日期
   */
  Future<T> runDaySelectedPlugin<T extends Object>(day)
  {
    Map<String, dynamic> param = {
      DBFldConstant.FLD_MAJOR: SysMajMinConstant.MAJOR_PAT,
      DBFldConstant.FLD_MINOR: PatMajorMinorConstant.MINOR_PAT_PLANS,
      HttpParamKeyValue.PARAM_KEY_EXP: 'userid=? and pst>0 and startday<=? and endday>=?',
      HttpParamKeyValue.PARAM_KEY_TYPES: 'i,t,t',
      HttpParamKeyValue.PARAM_KEY_VALS: '${UserMethod.getUserId()},${day} 12:00:00,${day} 12:00:00',
      BasMapKeyConstant.MAP_KEY_DATE: day,
    };
    return PluginsFactory.getInstance().get(PatSelectDayPlugin.toStrings()).runPlugin(this, initPara: param);
  }

  void onClick(Widget view)
  {
    if (view == btn_right)
    {
      runPluginName(PatTodoTaskPlugin.toStrings());
    }
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PatCalendarPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new PatCalendarPlugin(), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new PatCalendarPlugin();
  }
}
