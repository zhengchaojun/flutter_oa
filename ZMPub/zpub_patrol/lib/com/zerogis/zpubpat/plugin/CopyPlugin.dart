import 'package:flutter/material.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'CopyPlugin', '计划任务', '计划任务', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'CopyPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * 模板页 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CopyPlugin extends PluginStatefulBase
{
  CopyPlugin({Key key, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new CopyPluginState();
  }

  static String toStrings()
  {
    return "CopyPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class CopyPluginState extends PluginBaseState<CopyPlugin>
{
  CopyPluginState()
  {
    mTitle = '模板页';
    setRightButtonFromIcon(Icons.menu);
  }

  void initState()
  {
    super.initState();
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBody(
        context,
        new Column(
          children: <Widget>[
            new IconButton(
              icon: new Icon(Icons.print),
              onPressed: ()
              {
                showToast("hellow world");
              },
            ),
          ],
        ));

    return widget;
  }

  void onClick(Widget widget)
  {

  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class CopyPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new CopyPlugin(), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new CopyPlugin();
  }
}
