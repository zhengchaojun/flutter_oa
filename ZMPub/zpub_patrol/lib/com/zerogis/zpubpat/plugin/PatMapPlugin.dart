import 'package:flutter/material.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatFldConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatMajorMinorConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/widget/CardMapWidget.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'PatMapPlugin', '巡检', '巡检', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'PatMapPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * 巡检地图页面 <br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 选传} att   属性值(PAT_PLANS表属性)
 * @param {Object 选传} plugin 插件
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PatMapPlugin extends PluginStatefulBase
{
  dynamic mInitPara;

  PatMapPlugin({Key key, this.mInitPara, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new PatMapPluginState();
  }

  static String toStrings()
  {
    return "PatMapPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class PatMapPluginState extends PluginBaseState<PatMapPlugin>
{
  void initState()
  {
    super.initState();
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBody(
        context,
        Stack(children: <Widget>[
          Positioned(child: createCard(), bottom: 10,right: 10,left: 10,)
        ],));
    return widget;
  }

  Widget createCard()
  {
    return CardMapWidget(plugin: widget.plugin, mInitPara: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT],);
  }

  void onClick(Widget widget)
  {

  }

  /*
   * 线路巡检，查询线路
   */
  queryLine()
  {
//   PARAM_KEY_VALS =  Patplans patEvent.getId()
    Map<String, Object> param = {
      HttpParamKeyValue.PARAM_KEY_MAJOR: PatMajorMinorConstant.MAJOR_PAT,
      HttpParamKeyValue.PARAM_KEY_MINOR: PatMajorMinorConstant.MINOR_PAT_PLANREC,
      HttpParamKeyValue.PARAM_KEY_EXP: '${PatFldConstant.PAT_PLANREC_PLANID}=? and ${DBFldConstant.FLD_MAJOR}=?',
      HttpParamKeyValue.PARAM_KEY_TYPES: 'i,s',
      HttpParamKeyValue.PARAM_KEY_VALS: '${widget.mInitPara[BasMapKeyConstant.MAP_KEY_ID]},${SysMajMinConstant
          .MAJOR_LINE}',
    };
    SvrAreaSvrService.querys(this, param: param);
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PatMapPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new PatMapPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new PatMapPlugin();
  }
}
