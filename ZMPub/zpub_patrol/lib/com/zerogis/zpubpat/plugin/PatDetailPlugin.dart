import 'package:flutter/material.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatFldConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatFldValueConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatMajorMinorConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatMapPlugin.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:weui/weui.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'PatDetailPlugin', '计划任务', '任务详情', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'PatDetailPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * 巡检任务详情页面 <br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 选传} id    查询属性的ID
 * @param {Object 选传} att   属性值(id和att选其一)
 * @param {Object 选传} plugin 插件
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PatDetailPlugin extends PluginStatefulBase
{
  dynamic mInitPara;

  PatDetailPlugin({Key key, this.mInitPara, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new PatDetailPluginState();
  }

  static String toStrings()
  {
    return "PatDetailPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class PatDetailPluginState extends PluginBaseState<PatDetailPlugin>
{
  Widget build(BuildContext context)
  {
    Widget widget = buildBody(
        context,
        SingleChildScrollView(child: Column(
          children: <Widget>[
            Stack(children: <Widget>[
              Column(children: <Widget>[
                Card(child: createAttWidget(),),
                test(),
              ],),
              Positioned(
                child: IconButton(iconSize: 50, icon: Image.asset('assets/images/att_commit.png',), onPressed: ()
                {
                  doClickPat();
                }), bottom: 10, right: 10,)
            ],),

            Padding(child: WeButton(
              '提交审核',
              theme: WeButtonType.warn,
              onClick: ()
              {
                doClickOk();
              },
            ), padding: EdgeInsets.symmetric(vertical: 10, horizontal: 4),)
          ],
        ),));
    return widget;
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == 'querys' && values is Map)
    {
      dealQuerys(values);
    }
    else if (method == 'updateAtt')
    {

    }
  }

  /*
   * 创建属性组件
   */
  Widget createAttWidget()
  {
    Plugin plugin = widget.mInitPara[BasMapKeyConstant.MAP_KEY_PLUGIN] ?? widget.plugin;
    return AttWidget(widget.mInitPara[DBFldConstant.FLD_MAJOR], widget.mInitPara[DBFldConstant.FLD_MINOR],
        mAttVals: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT],
        mValueChangedMethod: widget.mInitPara[BasMapKeyConstant.MAP_KEY_VALUE_CHANGE_METHOD],
        plugin: plugin,
        key: key_btn_border);
  }

  void dealQuerys(Map values)
  {
    if (values[PatFldConstant.PAT_PLANREC_PLANZT] != PatFldValueConstant.PAT_PLANREC_PLANZT_2)
    {
      Map<String, Object> param = {
        HttpParamKeyValue.PARAM_KEY_MAJOR: SysMajMinConstant.MAJOR_PAT,
        HttpParamKeyValue.PARAM_KEY_MINOR: PatMajorMinorConstant.MINOR_PAT_PLANS,
        PatFldConstant.PAT_PLANS_PST: PatFldValueConstant.PAT_PLANS_PST_0
      };
      SvrAreaSvrService.updateAtt(this, param: param);
    }
    else
    {
      showToast("请您先完成巡检，并提交巡检报告！");
    }
  }

  /*
   * 跳转到巡检页面
   */
  doClickPat()
  {
    mChildUniversalInitPara.addAll(widget.mInitPara);
    print(mChildUniversalInitPara);
    runPluginName(PatMapPlugin.toStrings()).then((result)
    {
      print('更新状态');
    });
  }

  doClickOk()
  {
    // 准备参数
    Map<String, dynamic> param = {
      HttpParamKeyValue.PARAM_KEY_MAJOR: SysMajMinConstant.MAJOR_PAT,
      HttpParamKeyValue.PARAM_KEY_MINOR: PatMajorMinorConstant.MINOR_PAT_PLANREC,
      HttpParamKeyValue.PARAM_KEY_EXP: 'planid=? and major=?',
      HttpParamKeyValue.PARAM_KEY_TYPES: 'i,s',
      HttpParamKeyValue.PARAM_KEY_VALS: '${widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT][DBFldConstant
          .FLD_ID]},${SysMajMinConstant.MAJOR_LINE}',
    };

    WeDialog.confirm(context)(
        '确定要提交审核?',
        title: '温馨提示',
        onConfirm: ()
        {
          SvrAreaSvrService.querys(this, param: param);
        }
    );
  }

  Widget test()
  {
//    deviceId	设备uuid
//    gjPeople	告警提交人
//  gjlx	告警类型
//  glid	关联id
//  status	告警状态
//  time	告警推送时间
    List list = [];
    Map<String, dynamic> value = {
      'deviceId': '3434-3434',
      'gjPeople': 'zcj',
      'gjlx': '1',
      'glid': '1',
      'status': '1',
      'time': '2018-12',
    };
    list.add(value);
    Map<String, dynamic> value2 = {
      'deviceId': '454545-3434',
      'gjPeople': '5656rgrgrgt',
      'gjlx': 'sdd',
      'glid': 'erer',
      'status': 'dfdf',
      'time': '2018-2',
    };
    list.add(value2);
    Map<String, dynamic> value3 = {
      'deviceId': '989898',
      'gjPeople': 'qqqq',
      'gjlx': 'ddd',
      'glid': '1',
      'status': '3',
      'time': '2018-11',
    };
    list.add(value3);
    return new AttDataTable(plugin: widget.plugin, mInitPara: {'major': 6, 'minor': 55, 'list': list},);
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PatDetailPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new PatDetailPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new PatDetailPlugin();
  }
}
