import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/widget/HistoryWidget.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/widget/TodoWidget.dart';

/*
 * 类描述：巡检程序启动注册模块：插件中需要更换对应到组件需要把组件进行注册，如果对应功能到组件一定不会更换可以不用注册
 * 作者：郑朝军 on 2019/6/1
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/1
 * 修改备注：
 */
class PatWidgetApplication
{
  void onCreate()
  {
    List<Plugin> plugin = InitSvrMethod.getInitSvrPlugins();
    plugin.forEach((plugin)
    {
      if (plugin.classurl == TodoWidget.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
      { // 待办组件
        WidgetsFactory.getInstance().add(plugin.name, new TodoWidgetService());
      }
      else if (plugin.classurl == HistoryWidget.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
      { // 历史组件
        WidgetsFactory.getInstance().add(plugin.name, new HistoryWidgetService());
      }
    });
  }
}
