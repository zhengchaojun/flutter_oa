/*
 * 类描述：巡检主子类型定义
 * 作者：郑朝军 on 2019/6/14
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/14
 * 修改备注：
 */
class PatMajorMinorConstant
{
  /*
   * -------------------------巡检业务级别相关------------------------------------
   */
  /*
   * 业务相关主类型
   */
  static final int MAJOR_PAT = 7;

  /*
   * 以下主类型为7
   */
  static final int MINOR_PAT_PLANS = 4; // 巡检任务表
  static final int MINOR_PAT_PLANREC = 5; // 巡检任务记录表
  static final int MINOR_PAT_LOCATEREC = 6; // 巡检轨迹记录表
}
