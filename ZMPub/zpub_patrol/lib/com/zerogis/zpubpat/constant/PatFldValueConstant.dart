/*
 * 类描述：数据库字段值相关
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class PatFldValueConstant
{
  // PAT_PLANREC表
  static final int PAT_PLANREC_PLANZT_2 = 2; // 未开始

  // PAT_PLANS字段：Pst
  static final int PAT_PLANS_PST_0 = 0; // 已完结

  // PAT_PLANS字段：lx
  static final int PAT_PLANS_LX_1 = 1; // 线路巡检
  static final int PAT_PLANS_LX_2 = 2; // 看护巡检
  static final int PAT_PLANS_LX_3 = 3; // 设备巡检
}
