import 'package:flutter/material.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_bas/zpub_bas.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'MapSeeCardWidget', '查看巡检卡片', '查看巡检卡片', 1, 3, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'MapSeeCardWidget', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 地图巡检查看卡片组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class MapSeeCardWidget extends WidgetStatefulBase
{
  MapSeeCardWidget({Key key, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new MapSeeCardWidgetState();
  }

  static String toStrings()
  {
    return "MapSeeCardWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class MapSeeCardWidgetState extends WidgetBaseState<MapSeeCardWidget>
{
  void initState()
  {
    super.initState();
  }

  Widget build(BuildContext context)
  {
    Widget widget = new Container();
    return widget;
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class MapSeeCardWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new MapSeeCardWidget();
  }
}
