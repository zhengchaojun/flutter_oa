/*
 * 类描述：主子类型定义
 * 作者：郑朝军 on 2019/6/14
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/14
 * 修改备注：
 */
class MajorMinorConstant
{
  /*
   * -------------------------系统级别相关------------------------------------
   */
  /*
   * 系统号相关
   */
  static final int SYS_NO_99 = 99;
  static final int SYS_NO_11 = 11;
  static final int SYS_NO_21 = 21;
  static final int SYS_NO_31 = 31;

  /*
   * 主类型相关
   */
  static final int MAJOR_SYS = 99;
  static final int MAJOR_PNT = 1; //点
  static final int MAJOR_LINE = 2; //线
  static final int MAJOR_REG = 3; //面
  static final int MAJOR_TEXT = 4; //注记
  static final int MAJOR_PAT = 7; //巡检主类型
  static final int MAJOR_MAPPNT = 81; //map点
  static final int MAJOR_MAPLIN = 82; //map线
  static final int MAJOR_MAPPOL = 83; //map面
  static final int MAJOR_REGPNT = 84; //reg点
  static final int MAJOR_REGLIN = 85; //reg线

  /*
   * 子类型相关
   */
  static final int MINOR_SYS_DEF = 1;
  static final int MINOR_SYS_CFG = 2;
  static final int MINOR_SYS_CACHE = 3;
  static final int MINOR_SYS_ENT = 4;
  static final int MINOR_SYS_FLD = 5;
  static final int MINOR_SYS_FLDVAL = 6;
  static final int MINOR_SYS_SERVICE = 7;
  static final int MINOR_SYS_PLUGIN = 8;
  static final int MINOR_SYS_FUNCGRP = 9;
  static final int MINOR_SYS_FUNC = 10;
  static final int MINOR_SYS_FACE = 11;
  static final int MINOR_SYS_ORGAN = 12;
  static final int MINOR_SYS_USER1 = 13;
  static final int MINOR_SYS_SYSUSER = 14;
  static final int MINOR_SYS_SYSROLE = 15;
  static final int MINOR_SYS_USERROLE = 16;
  static final int MINOR_SYS_USERDEP = 17;
  static final int MINOR_SYS_SYSROLEOBJ = 18;
  static final int MINOR_SYS_WMS = 31;
  static final int MINOR_SYS_WFS = 32;
  static final int MINOR_SYS_VECTOR = 33;
  static final int MINOR_SYS_VIEWPORT = 34;
  static final int MINOR_SYS_REGION = 35;
  static final int MINOR_SYS_LAYER = 36;
  static final int MINOR_SYS_MAPARA = 37;
  static final int MINOR_SYS_LOGINLOG = 38;
  static final int MINOR_SYS_OPLOG = 39;
  static final int MINOR_SYS_GROUP = 40;
  static final int MINOR_SYS_USERGROUP = 41;
  static final int MINOR_SYS_DS = 42;
  static final int MINOR_SYS_RESDIR = 43;
  static final int MINOR_SYS_RESITEM = 44;
  static final int MINOR_SYS_MEDIA = 45;
  static final int MINOR_SYS_URLMAP = 46;

  /*
   * -------------------------业务级别相关------------------------------------
   */
  /*
   * 业务相关主类型
   */
  static final int MAJOR_BUSINESS = 98;

  /*
   * 以下主类型为99
   */
  static final int MINOR_USERDEP = 17; //用户与部门关联
  static final int MINOR_MEDIA = 45; //附件表
  static final int MINOR_DELEGATE = 62; //委办表
  static final int MINOR_SEQBILL = 64; //序号编码
  static final int MINOR_PROCCOMP = 65; //完结通知

  /*
   * 以下主类型为98
   */
  static final int MINOR_OA_WORKDAY = 4; //工作日
  static final int MINOR_OA_WEEK = 5; //周次
  static final int MINOR_OA_ASSET = 11; //资产表
  static final int MINOR_OA_ASSETUSE = 12; //资产领用记录
  static final int MINOR_OA_GIFT = 13; //礼品
  static final int MINOR_OA_GIFTIN = 14; //礼品入库
  static final int MINOR_OA_GIFTUSE = 15; //礼品领用
  static final int MINOR_OA_GIFTD = 16; //礼品领用申请单
  static final int MINOR_OA_BJBDN = 17; //笔记本电脑申请单
  static final int MINOR_OA_DEV_BUY = 18; //设备申请单
  static final int MINOR_OA_TASK = 21; //任务
  static final int MINOR_OA_CUSTOMER = 31; //客户
  static final int MINOR_OA_CONTACT = 32; //客户联系人
  static final int MINOR_OA_PRJ = 41; //项目
  static final int MINOR_OA_PRJMEM = 42; //项目成员
  static final int MINOR_OA_PRJMS = 43; //项目里程碑
  static final int MINOR_OA_PRJPROB = 44; //项目问题
  static final int MINOR_OA_PRJTASK = 45; //项目任务
  static final int MINOR_OA_PRJTIME = 46; //项目工时
  static final int MINOR_OA_PRJCONT = 47; //项目联系人
  static final int MINOR_HT_INFO = 51; //合同信息
  static final int MINOR_HT_INVOICE = 53; //发票信息
  static final int MINOR_HT_PAY = 54; //付款信息
  static final int MINOR_F_DDBXD = 61; //地大报销单
  static final int MINOR_OA_TXFBX = 62; //通信费报销单
  static final int MINOR_HR_LEAVE = 71; //请假单
  static final int MINOR_HR_BUSTRIP = 72; //出差单
  static final int MINOR_HR_OUT = 73; //外出登记
  static final int MINOR_HR_PUNCH = 74; //补卡
  static final int MINOR_HR_CLOCKIN = 75; //打卡
  static final int MINOR_WM_MEET = 81; //周会
  static final int MINOR_WM_MEETMEM = 82; //周会参与人
  static final int MINOR_WM_MEETREC = 83; //周会记录
  static final int MINOR_OA_SEAL = 91; //用印申请单
  static final int MINOR_OA_HTPS = 92; //合同评审单
  static final int MINOR_OA_HTPSCG = 93; //采购合同评审单
  static final int MINOR_OA_LIC  = 94; //License申请

  static final int MINOR_OA_FBORROW = 101; //借支单1
  static final int MINOR_OA_FOFFSET = 102; //借支冲抵1
  static final int MINOR_OA_CLYTJT = 113; //远途交通费1
  static final int MINOR_OA_CLZAS = 114; //住宿费19
  static final int MINOR_OA_CLSNJT = 115; //市内交通费1
  static final int MINOR_OA_CLQT = 116; //其他费用1
  static final int MINOR_OA_FYBXD = 121; //费用报销单1
  static final int MINOR_OA_FYDTL = 122; //费用明细单1
  static final int MINOR_OA_HI_USER_1 = 171; //用户(历史)
  static final int MINOR_OA_WAGSTD = 181; //工资标准1
  static final int MINOR_OA_HI_WAGSTD = 182; //工资标准(历史)

  static final int MINOR_OA_LINK = 151; //网址
  static final int MINOR_OA_AREA = 161; //区域
}
