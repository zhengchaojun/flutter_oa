/*
 * 类描述：网络，数据库的条件相关常量
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class ExpConstant
{
  //-------------常用------------------------------------------
  static const String EXP_USERID = "userid=?";

  static const String EXP_ID_MORE_THAN_QUESTION = "id>?";

  static const String EXP_ID_MORE_THAN_ZERO = "id>0";

  static const String EXP_ID_MORE_THAN_ZERO_ = "_id>0";

  static const String EXP_LESS_THAN_EQUAL = "<=";

  static const String EXP_MORE_THAN_EQUAL = ">=";

  static const String EXP_AND = " and ";

  static const String EXP_TO_DATE = "to_date";

  static const String EXP_YYYY_MM_DD = "'YYYY-MM-DD'";

  //-------------业务相关------------------------------------------

  /*
   * 通用业务相关
   */
  static const String EXP_USERID_AND_RQ = "userid=? AND rq=?";
  static const String EXP_MAJOR_MINOR_ENTITYID = "major=? AND minor=? AND entityid=?";

  static const String EXP_USERID_AND_RQ_RQ = "userid=? AND rq>=? AND rq<=?";

  //-------------考勤业务相关------------------------------------------


  static const String EXP_USERID_AND_EFFECT_AND_ENDTIME = "userid=? AND effect=? AND !(endtime<? OR starttime>?)";
  static const String EXP_USERID_AND_EFFECT_AND_RQ = "userid=? AND effect=? AND rq>=? AND rq<=?";

  //-------------工单中心业务相关------------------------------------------
  static const String EXP_USERID_INUSE = "userid=? AND inuse=?";
}
