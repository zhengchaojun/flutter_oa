/*
 * 类描述：数据库字段值相关
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class FldValueConstant
{
  // ----------------常用---------------------------
  static final String SYSROLEOBJ_TYPE_DISPE_FUNC = "func";

  static final String SYSROLEOBJ_TYPE_DISPE_BTN = "btn";

  static final String SYSROLEOBJ_TYPE_DISPE_LAYER = "layer";

  static final String SYSROLEOBJ_TYPE_DISPE_REGION = "region";

  static final String SYSROLEOBJ_TYPE_DISPE_ETTCATVIEW = "ettCatView";

  static final String SYSROLEOBJ_TYPE_DISPE_ETTCATEDIT = "ettCatEdit";

  static final String SYSROLEOBJ_TYPE_DISPE_ORGAN = "organ";



  static final String FLDVALUE_MS = "MS";

  static final String FLDVALUE_DISPC_FIEDL = "field";

  //---------------自动选人，分管，部门负责人------------------------------
  static final int OA_DEPADM_TYPE_FG  = 0;// 分管理
  static final int OA_DEPADM_TYPE_FZR  = 1;// 部门负责人

  //---------------LIC------------------------------
  static final int OA_LIC_MAC_TYPE_KH  = 0;// 客户使用
  static final int OA_LIC_MAC_TYPE_YG  = 1;// 员工使用
}
