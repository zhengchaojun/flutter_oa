import 'package:flutter/widgets.dart';

/*
 * 类描述：图标，字体相关
 * 作者：郑朝军 on 2019/5/9
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/9
 * 修改备注：
 */
class IconFont
{
  static const String _family = 'iconfont';

  IconFont._();

  static const IconData icon_statistics = IconData(0xe6f6, fontFamily: _family);
  static const IconData icon_details = IconData(0xe6f7, fontFamily: _family);
  static const IconData icon_histogram = IconData(0xe6f8, fontFamily: _family);
  static const IconData icon_linechart = IconData(0xe6f9, fontFamily: _family);
  static const IconData icon_piechart = IconData(0xe6fa, fontFamily: _family);
  static const IconData icon_base_zuixiaohua = IconData(0xe6e6, fontFamily: _family);
  static const IconData icon_base_zuidahua = IconData(0xe6e8, fontFamily: _family);
  static const IconData icon_base_guanbi = IconData(0xe6e9, fontFamily: _family);
  static const IconData icon_SelOrganPerm = IconData(0xe73a, fontFamily: _family);
  static const IconData icon_AssignFunc = IconData(0xe73b, fontFamily: _family);
  static const IconData icon_Annex = IconData(0xe73c, fontFamily: _family);
  static const IconData icon_AttView = IconData(0xe73d, fontFamily: _family);
  static const IconData icon_PswSet = IconData(0xe73f, fontFamily: _family);
  static const IconData icon_ManOrgan = IconData(0xe740, fontFamily: _family);
  static const IconData icon_AttInput = IconData(0xe741, fontFamily: _family);
  static const IconData icon_HelpPreview = IconData(0xe742, fontFamily: _family);
  static const IconData icon_ManUserOrgan = IconData(0xe743, fontFamily: _family);
  static const IconData icon_SelOrganUserPerm = IconData(0xe744, fontFamily: _family);
  static const IconData icon_ManRole = IconData(0xe745, fontFamily: _family);
  static const IconData icon_xtmanage = IconData(0xe746, fontFamily: _family);
  static const IconData icon_UserInfo = IconData(0xe747, fontFamily: _family);
  static const IconData icon_ManUserRole = IconData(0xe748, fontFamily: _family);
  static const IconData icon_AttsTab = IconData(0xe749, fontFamily: _family);
  static const IconData icon_update = IconData(0xe74a, fontFamily: _family);
  static const IconData icon_cleanup = IconData(0xe74b, fontFamily: _family);
  static const IconData icon_parameter = IconData(0xe74c, fontFamily: _family);
  static const IconData icon_querylocation = IconData(0xe74e, fontFamily: _family);
  static const IconData icon_import1 = IconData(0xe74f, fontFamily: _family);
  static const IconData icon_import = IconData(0xe750, fontFamily: _family);
  static const IconData icon_click = IconData(0xe751, fontFamily: _family);
  static const IconData icon_query = IconData(0xe752, fontFamily: _family);
  static const IconData icon_label = IconData(0xe753, fontFamily: _family);
  static const IconData icon_cancel = IconData(0xe754, fontFamily: _family);
  static const IconData icon_password = IconData(0xe756, fontFamily: _family);
  static const IconData icon_rolemanage = IconData(0xe757, fontFamily: _family);
  static const IconData icon_date = IconData(0xe758, fontFamily: _family);
  static const IconData icon_property = IconData(0xe759, fontFamily: _family);
  static const IconData icon_scavenging = IconData(0xe75a, fontFamily: _family);
  static const IconData icon_user = IconData(0xe75b, fontFamily: _family);
  static const IconData icon_project = IconData(0xe75c, fontFamily: _family);
  static const IconData icon_rightslip = IconData(0xe75d, fontFamily: _family);
  static const IconData icon_rightarrow = IconData(0xe75e, fontFamily: _family);
  static const IconData icon_resetting = IconData(0xe75f, fontFamily: _family);
  static const IconData icon_apply = IconData(0xe760, fontFamily: _family);
  static const IconData icon_browse = IconData(0xe761, fontFamily: _family);
  static const IconData icon_leftarrow = IconData(0xe762, fontFamily: _family);
  static const IconData icon_integrat = IconData(0xe763, fontFamily: _family);
  static const IconData icon_folderclose = IconData(0xe764, fontFamily: _family);
  static const IconData icon_xiangqing = IconData(0xe765, fontFamily: _family);
  static const IconData icon_subscriber = IconData(0xe766, fontFamily: _family);
  static const IconData icon_delete = IconData(0xe767, fontFamily: _family);
  static const IconData icon_preview = IconData(0xe768, fontFamily: _family);
  static const IconData icon_leftslip = IconData(0xe769, fontFamily: _family);
  static const IconData icon_screenshot = IconData(0xe76a, fontFamily: _family);
  static const IconData icon_bookmark = IconData(0xe76b, fontFamily: _family);
  static const IconData icon_tool = IconData(0xe76c, fontFamily: _family);
  static const IconData icon_analysis = IconData(0xe76d, fontFamily: _family);
  static const IconData icon_role = IconData(0xe76e, fontFamily: _family);
  static const IconData icon_folderopen = IconData(0xe76f, fontFamily: _family);
  static const IconData icon_xuanzeliebiao = IconData(0xe770, fontFamily: _family);
  static const IconData icon_FldFilter = IconData(0xe794, fontFamily: _family);
  static const IconData icon_NavResourceRst = IconData(0xe795, fontFamily: _family);
  static const IconData icon_userregister = IconData(0xe796, fontFamily: _family);
  static const IconData icon_projectprocess = IconData(0xe798, fontFamily: _family);
  static const IconData icon_userauthority = IconData(0xe799, fontFamily: _family);
  static const IconData icon_projectfilling = IconData(0xe79a, fontFamily: _family);
  static const IconData icon_CheckDataTabs = IconData(0xe79b, fontFamily: _family);
  static const IconData icon_TickFinish = IconData(0xe79d, fontFamily: _family);
  static const IconData icon_systemtool = IconData(0xe79e, fontFamily: _family);
  static const IconData icon_TickToDo = IconData(0xe79f, fontFamily: _family);
  static const IconData icon_Condition = IconData(0xe7a1, fontFamily: _family);
  static const IconData icon_QryAtt = IconData(0xe7a2, fontFamily: _family);
  static const IconData icon_SelAttRevise = IconData(0xe7a3, fontFamily: _family);
  static const IconData icon_LoginDetail = IconData(0xe7a4, fontFamily: _family);
  static const IconData icon_ReptDetail = IconData(0xe7a5, fontFamily: _family);
  static const IconData icon_AttsModify = IconData(0xe7a6, fontFamily: _family);
  static const IconData icon_EntityLog = IconData(0xe7a7, fontFamily: _family);
  static const IconData icon_AssignEntity = IconData(0xe7a8, fontFamily: _family);
  static const IconData icon_ExpMoreInput = IconData(0xe7a9, fontFamily: _family);
  static const IconData icon_TickDetail = IconData(0xe7aa, fontFamily: _family);
  static const IconData icon_logs = IconData(0xe7ab, fontFamily: _family);
  static const IconData icon_TickQuery = IconData(0xe7ac, fontFamily: _family);
  static const IconData icon_TickCheck = IconData(0xe7ad, fontFamily: _family);
  static const IconData icon_Ticket = IconData(0xe7ae, fontFamily: _family);
  static const IconData icon_ImpTable = IconData(0xe7af, fontFamily: _family);
  static const IconData icon_retracting = IconData(0xe7b0, fontFamily: _family);
  static const IconData icon_LoginLog = IconData(0xe7b1, fontFamily: _family);
  static const IconData icon_querycount = IconData(0xe7b2, fontFamily: _family);
  static const IconData icon_queryfilter = IconData(0xe7b3, fontFamily: _family);
  static const IconData icon_partreport = IconData(0xe7b4, fontFamily: _family);
  static const IconData icon_CheckOther = IconData(0xe7b5, fontFamily: _family);
  static const IconData icon_departmentedit = IconData(0xe7b6, fontFamily: _family);
  static const IconData icon_CheckResult = IconData(0xe7b7, fontFamily: _family);
  static const IconData icon_AssignButton = IconData(0xe7c5, fontFamily: _family);
  static const IconData icon_ImpExcel = IconData(0xe7c6, fontFamily: _family);
  static const IconData icon_departmentanduser = IconData(0xe7c7, fontFamily: _family);
  static const IconData icon_attachment = IconData(0xe7c8, fontFamily: _family);
  static const IconData icon_allreport = IconData(0xe7c9, fontFamily: _family);
  static const IconData icon_projectoverview = IconData(0xe7ca, fontFamily: _family);
  static const IconData icon_search = IconData(0xe7cb, fontFamily: _family);
  static const IconData icon_services = IconData(0xe7cc, fontFamily: _family);
  static const IconData icon_stcleanup = IconData(0xe7cd, fontFamily: _family);
  static const IconData icon_window = IconData(0xe7ce, fontFamily: _family);
  static const IconData icon_determine = IconData(0xe7cf, fontFamily: _family);
  static const IconData icon_dateimport = IconData(0xe7d0, fontFamily: _family);
  static const IconData icon_datamining = IconData(0xe7d1, fontFamily: _family);
  static const IconData icon_ptparameter = IconData(0xe7d2, fontFamily: _family);
  static const IconData icon_contact = IconData(0xe7d3, fontFamily: _family);
  static const IconData icon_viewattachment = IconData(0xe7d4, fontFamily: _family);
  static const IconData icon_through = IconData(0xe7d5, fontFamily: _family);
  static const IconData icon_viewopinion = IconData(0xe7d6, fontFamily: _family);
  static const IconData icon_applyedit = IconData(0xe7d7, fontFamily: _family);
  static const IconData icon_userexamine = IconData(0xe7d8, fontFamily: _family);
  static const IconData icon_subsidy = IconData(0xe7d9, fontFamily: _family);
  static const IconData icon_notthrough = IconData(0xe7da, fontFamily: _family);
  static const IconData icon_policies = IconData(0xe7db, fontFamily: _family);
  static const IconData icon_all = IconData(0xe7b8, fontFamily: _family);
  static const IconData icon_launch = IconData(0xe7bf, fontFamily: _family);
  static const IconData icon_news = IconData(0xe7c0, fontFamily: _family);
  static const IconData icon_application = IconData(0xe7c1, fontFamily: _family);
  static const IconData icon_work = IconData(0xe7c2, fontFamily: _family);
  static const IconData icon_leavep = IconData(0xe7c3, fontFamily: _family);
  static const IconData icon_my = IconData(0xe7c4, fontFamily: _family);
  static const IconData icon_location = IconData(0xe7dc, fontFamily: _family);
  static const IconData icon_addto = IconData(0xe7dd, fontFamily: _family);
  static const IconData icon_calendar = IconData(0xe7de, fontFamily: _family);
  static const IconData icon_plf = IconData(0xe7df, fontFamily: _family);

  static String getFamily()
  {
    return _family;
  }

  static Map<String, IconData> _maps = {};

  static void test()
  {
    _maps["icon_plf"] = icon_plf;
  }
}