import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutteroa/%20core/CommonWidgetCreator.dart';
import 'package:flutteroa/%20core/WidgetCreator.dart';
import 'package:flutteroa/constant/MapKeyConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 完接通知组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class ProcCompWidget extends WidgetStatefulBase
{
  ProcCompWidget({Key key, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new ProcCompWidgetState();
  }

  static String toStrings()
  {
    return "ProcCompWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class ProcCompWidgetState extends WidgetBaseState<ProcCompWidget>
{
  Widget widget_body = WidgetCreator.createCommonNoData(text: BasStringRes.progressbar_text);

  void initState()
  {
    super.initState();
    query();
  }

  Widget build(BuildContext context)
  {
    return createCommonRefresh(widget_body);
  }

  @override
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryProcComp")
    {
      if (values is List)
      {
        setState(()
        {
          String mBage = values.length.toString();
          String mDescribe = values.first["reason"];
          String mTime = TimelineUtil.formatByDateTime(DateUtil.getDateTime(values.first["instime"]));

          widget_body = new GestureDetector(child: new Card(child: CommonWidgetCreator.createImgLeftTextRight(
              "assets/images/icon-notice.png",
              title: '提醒',
              describe: mDescribe,
              time: mTime,
              bage: mBage)),
            onTap: ()
            {
              mChildUniversalInitPara[MapKeyConstant.MAP_KEY_LIST] = values;
              runPlugin<bool>(DigitValueConstant.APP_DIGIT_VALUE_0).then((bool result)
              {
                if (result != null && result)
                {
                  query();
                }
              });
            },);

          // 更新消息主视图数量
          updateMainBadge(mBage);
        });
      }
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  @override
  void onNetWorkFaild(String method, Object values)
  {
    if (method == "queryProcComp")
    {
      setState(()
      {
        widget_body = WidgetCreator.createCommonNoData(text: values);
        updateMainBadge('0');
      });
    }
    else
    {
      super.onNetWorkFaild(method, values);
    }
  }


  @override
  Future<void> onRefresh()
  {
    query();
    return super.onRefresh();
  }


  void query()
  {
//    ProcCompService.queryProcComp(this);
  }


  /*
   * 更新消息主视图数量
   * @param badge 消息数量
   */
  void updateMainBadge(String badge)
  {
//    State state = StateManager.getInstance().queryRunTimeType(MainPlugin.toStrings());
//    if (state != null && state is MainPluginState)
//    {
//      state.updateBadge(badge);
//    }
  }
}



/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class ProcCompWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new ProcCompWidget();
  }
}
