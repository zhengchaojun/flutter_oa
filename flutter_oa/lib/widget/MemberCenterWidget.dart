import 'package:flutter/material.dart';
import 'package:flutteroa/%20core/CommonWidgetCreator.dart';
import 'package:flutteroa/%20core/WidgetCreator.dart';
import 'package:flutteroa/constant/MapKeyConstant.dart';
import 'package:flutteroa/constant/PluginInitParamKeyConstant.dart';
import 'package:flutteroa/manager/ExitManager.dart';
import 'package:flutteroa/plugin/LoginPlugin.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/UserMethod.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';

/*
 * 个人中心组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class MemberCenterWidget extends WidgetStatefulBase
{
  MemberCenterWidget({Key key, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new MemberCenterWidgetState();
  }

  static String toStrings()
  {
    return "MemberCenterWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class MemberCenterWidgetState extends WidgetBaseState<MemberCenterWidget>
{
  List<dynamic> _mList;

  String mImgUrl;

  MemberCenterWidgetState()
  {}

  void initState()
  {
    super.initState();
    _mList = mInitParaMap[MapKeyConstant.MAP_KEY_LIST];
    if (UserMethod.getUser().getGender() == 0)
    {
      mImgUrl = "assets/images/icon-gheadportrait.png";
    }
    else
    {
      mImgUrl = "assets/images/icon-headportrait.png";
    }
  }

  Widget build(BuildContext context)
  {
    return createCommonRefresh(new ListView(
      children: <Widget>[
        new Container(
            height: MarginPaddingHeightConstant.APP_MARGIN_PADDING_232,
            child:
            new Stack(
              children: <Widget>[
                new WeSwipe(
                    height: MarginPaddingHeightConstant.APP_MARGIN_PADDING_232,
                    autoPlay: false,
                    itemCount: 1,
                    indicators: false,
                    itemBuilder: (index)
                    {
                      return WidgetCreator.createCommonImage(
                        mInitParaMap[PluginInitParamKeyConstant
                            .PLUGIN_INIT_PARAM_MAIN_KEY_IMAGE_URL],
                        fit: BoxFit.fill,
                        alignment: Alignment.topCenter,
                      );
                    }
                ),
                new Align(
                    child: new Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
//                      new CircleAvatarCusWidget(
//                        mImgUrl: mImgUrl,),
                      new Text(UserMethod.getUser().getName(), style: BasTextStyleRes.text_color_text4_larger,),
                    ],)
                )
              ],
            )),
        new Card(
            margin: const EdgeInsets.only(left: 19, right: 19, top: 20),
            child: new WeCells(
                children: createCellWidget())),
        new Padding(padding: EdgeInsets.only(left: 19, right: 19, top: 15), child: new WeButton(
          '退出登录',
          theme: WeButtonType.warn,
          onClick: ()
          {
            WeDialog.confirm(context)(
                '确定退出当前登录用户?',
                title: '注销登录',
                onConfirm: ()
                {
                  doClickLogout();
                }
            );
          },
        ),),
      ],
    ));
  }

  /*
   * 创建cell的表格
   */
  List<Widget> createCellWidget()
  {
    List<Widget> list = <Widget>[];
    _mList.forEach((map)
    {
      list.add(CommonWidgetCreator.createCommonLeftImgText(map, size: 40, onClick: ()
      {
        if (_mList.last == map)
        {
          WeDialog.confirm(context)(
              '确定清空缓存?',
              title: '清空缓存',
              onConfirm: ()
              {
                showToast("清除成功");
              }
          );
        }
        else
        {
          runPluginName(map[MapKeyConstant.MAP_KEY_PLUGIN]);
        }
      }));
    });
    return list;
  }

  void doClickLogout()
  {
    StateManager.getInstance().startWidegtStateAndRemove(new LoginPlugin(), StateManager.getInstance().currentState());
    ExitManager.clearLocalData();
  }

  @override
  Future<void> onRefresh()
  {
    return super.onRefresh();
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class MemberCenterWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new MemberCenterWidget();
  }
}
