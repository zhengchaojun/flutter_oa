import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutteroa/constant/MapKeyConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_bas/zpub_bas.dart';
/*
 * 类描述：主页面代办组件
 * 作者：郑朝军 on 2019/5/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/6
 * 修改备注：
 */
class HomeWillCardWidget extends WidgetStatefulBase
{
  HomeWillCardWidget({Key key, plugin}) : super(key: key, plugin: plugin);

  @override
  State<StatefulWidget> createState()
  {
    return new HomeWillCardState();
  }

  static String toStrings()
  {
    return "HomeWillCardWidget";
  }
}

class HomeWillCardState extends WidgetBaseState<HomeWillCardWidget>
{
  String textValue = BasStringValueConstant.STR_VALUE_BOTH_ZERO;

  String textValuer = BasStringValueConstant.STR_VALUE_BOTH_ZERO;

  String textValued = BasStringValueConstant.STR_VALUE_BOTH_ZERO;

  String textValues = BasStringValueConstant.STR_VALUE_BOTH_ZERO;

  void initState()
  {
    super.initState();
    initData();
    query(1);
  }

  @override
  Widget build(BuildContext context)
  {
    return new Card(
      child: new Row(
        children: <Widget>[
          new Flexible(
            child: new Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 5, bottom: 5),
              child: new GestureDetector(
                child: new Column(
                  children: <Widget>[
                    new Flexible(child: new Text(
                      textValue, style: TextStyle(fontSize: 20, color: Colors.red),), flex: 1,),

                    new Flexible(child: new Text(
                        "待办箱", style: BasTextStyleRes.text_color_text1_small), flex: 1,),
                  ],
                ),
                onTap: ()
                {
                  doClickLeadingButton();
                },
              ),
              height: 45,
            ),
            flex: 1,
          ),
          new Flexible(
            child: new Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 5, bottom: 5),
              child: new GestureDetector(
                child: new Column(
                  children: <Widget>[
                    new Flexible(
                      child: new Text(textValuer, style: TextStyle(fontSize: 20, color: Colors.red),), flex: 1,),

                    new Flexible(child: new Text("候选箱", style: BasTextStyleRes.text_color_text1_small), flex: 1,),
                  ],
                ),
                onTap: ()
                {
                  doClickLeadingRightButton();
                },
              ),
              height: 45,
            ),
            flex: 1,
          ),
          new Flexible(
            child: new Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 5, bottom: 5),
              child: new GestureDetector(
                child: new Column(
                  children: <Widget>[
                    new Flexible(
                      child: new Text(textValued, style: TextStyle(fontSize: 20, color: Colors.red),), flex: 1,),

                    new Flexible(child: new Text("考勤记录",
                        style: BasTextStyleRes.text_color_text1_small), flex: 1,),
                  ],
                ),
                onTap: ()
                {
                  doClickLeadingLeftButton();
                },
              ),
              height: 45,
            ),
            flex: 1,
          ),
          new Flexible(
            child: new Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 5, bottom: 5),
              child: new GestureDetector(
                child: new Column(
                  children: <Widget>[
                    new Flexible(
                      child: new Text(textValues, style: TextStyle(fontSize: 20, color: Colors.red),), flex: 1,),

                    new Flexible(child: new Text("考勤异常", style: BasTextStyleRes.text_color_text1_small), flex: 1,),
                  ],
                ),
                onTap: ()
                {
                  doClickLeadingRightRightButton();
                },
              ),
              height: 45,
            ),
            flex: 1,
          ),
        ],
      ),
    );
  }

  /*
   * 查询我的代办,候选，考勤任务最大数量
   */
  void query(int page)
  {
//    ProcessService.queryMyTaskMax(this);
//    ProcessService.queryUserTaskMax(this);
//    AttendanceService.queryAttendanceMax(this);
//    AttendanceService.queryUnAttendanceMax(this);
  }

  @override
  void onNetWorkFaild(String method, Object values)
  {}

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryMyTaskMax")
    {
      if (values is Map)
      {
        notifyDataSetChangeTextValue(
            values[MapKeyConstant.MAP_KEY_SIZE].toString());
      }
    }
    else if (method == "queryUserTaskMax")
    {
      if (values is Map)
      {
        notifyDataSetChangeTextValuer(
            values[MapKeyConstant.MAP_KEY_SIZE].toString());
      }
    }
    else if (method == "queryAttendanceMax")
    {
      if (values is Map)
      {
        notifyDataSetChangeTextValued(
            values[MapKeyConstant.MAP_KEY_SIZE].toString());
      }
    }
    else if (method == "queryUnAttendanceMax")
    {
      if (values is Map)
      {
        notifyDataSetChangeTextValues(
            values[MapKeyConstant.MAP_KEY_SIZE].toString());
      }
    }
  }

  void notifyDataSetChangeTextValue(String length)
  {
    setState(()
    {
      textValue = length;
    });
  }

  void notifyDataSetChangeTextValuer(String length)
  {
    setState(()
    {
      textValuer = length;
    });
  }

  void notifyDataSetChangeTextValued(String length)
  {
    setState(()
    {
      textValued = length;
    });
  }

  void notifyDataSetChangeTextValues(String length)
  {
    setState(()
    {
      textValues = length;
    });
  }

  /*
   * 点击代办箱
   */
  void doClickLeadingButton()
  {
    if (textValue == BasStringValueConstant.STR_VALUE_BOTH_ZERO ||
        textValue == BasStringValueConstant.STR_VALUE_ZERO)
    {
      showToast(BasStringRes.no_data);
      return;
    }

    runPlugin<bool>(DigitValueConstant.APP_DIGIT_VALUE_0).then((bool result)
    {
      if (result != null && result)
      {
        query(1);
      }
    });
  }

  /*
   * 点击候选箱
   */
  void doClickLeadingRightButton()
  {
    if (textValuer == BasStringValueConstant.STR_VALUE_BOTH_ZERO ||
        textValuer == BasStringValueConstant.STR_VALUE_ZERO)
    {
      showToast(BasStringRes.no_data);
      return;
    }

    runPlugin<bool>(DigitValueConstant.APP_DIGIT_VALUE_1).then((bool result)
    {
      if (result != null && result)
      {
        query(1);
      }
    });
  }

  /*
   * 点击考勤记录
   */
  void doClickLeadingLeftButton()
  {
    showToast(BasStringRes.development);
//    runPlugin<bool>(DigitValueConstant.APP_DIGIT_VALUE_2);
  }

  /*
   * 点击考勤记录
   */
  void doClickLeadingRightRightButton()
  {
    showToast(BasStringRes.development);
//    runPlugin<bool>(DigitValueConstant.APP_DIGIT_VALUE_3);
  }
}


/*
 * 类描述：首页组件提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class HomeWillCardWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new HomeWillCardWidget(
      key: initPara[MapKeyConstant.MAP_KEY_GLOBAL_KEY],);
  }
}
