import 'package:flutter/material.dart';
import 'package:flutteroa/%20core/CommonWidgetCreator.dart';
import 'package:flutteroa/%20core/WidgetCreator.dart';
import 'package:flutteroa/constant/FldConstant.dart';
import 'package:flutteroa/constant/MapKeyConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/MenuMethod.dart';
import 'package:zpub_bas/zpub_bas.dart';
/*
 * 功能组组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class FuncWidget extends WidgetStatefulBase
{
  FuncWidget({Key key, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new FuncWidgetState();
  }

  static String toStrings()
  {
    return "FuncWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class FuncWidgetState extends WidgetBaseState<FuncWidget>
{
  /*
   * 所有菜单集合
   */
  List<Map<dynamic, List<dynamic>>> mMenu;

  void initState()
  {
    super.initState();
    MenuMethod menu = MenuMethod.getInstance();
    mMenu = menu.queryMenu();
  }

  Widget build(BuildContext context)
  {
    Widget widget = new Container(margin: EdgeInsets.only(left: 15, right: 15), child: new ListView(
      children: createMenu(),
    ));
    return widget;
  }

  /*
   * 创建所有菜单
   */
  List<Widget> createMenu()
  {
    List<Widget> list = <Widget>[];
    mMenu.forEach((value)
    {
      value.forEach((key, menu)
      {
        list.add(new Column(children: <Widget>[
          new Align(alignment: Alignment.topLeft, child: new Padding(padding: EdgeInsets.all(10), child: new Text(
              key[FldConstant.FLD_NAMEC],
              textAlign: TextAlign.start,
              style: BasTextStyleRes.text_color_text1_larger_fontw900)),),
          new Card(
              margin: const EdgeInsets.only(left: 5, right: 5),
              child: createMenuGridView(menu))
        ],));
      });
    });
    return list;
  }

  /*
   * 创建一个菜单中的所有条目
   */
  GridView createMenuGridView(List<dynamic> menu)
  {
    List<Widget> list = <Widget>[];
    menu.forEach((value)
    {
      list.add(CommonWidgetCreator.createCommonTopImgText2(value, onTap: ()
      {
        runPluginName(value[MapKeyConstant.MAP_KEY_PLUGIN]);
      }));
    });
    return WidgetCreator.createMiddleGridViewWrap(list);
  }
}



/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class FuncWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new FuncWidget();
  }
}
