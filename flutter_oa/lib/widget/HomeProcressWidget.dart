import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutteroa/%20core/WidgetCreator.dart';
import 'package:flutteroa/constant/MapKeyConstant.dart';
import 'package:flutteroa/font/IconFont.dart';
import 'package:flutteroa/widget/listview/BaseWrapListView.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/bean/Pager.dart';
import 'package:zpub_bas/zpub_bas.dart';

import 'bean/ProcInstsProcess.dart';
import 'listview/StartedProcessListView.dart';

void main()
{
  runApp(new HomeProcressWidget());
}

/*
 * 类描述：流程功能组件
 * 作者：郑朝军 on 2019/5/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/6
 * 修改备注：
 */
class HomeProcressWidget extends FuctionStateFulBase
{
  HomeProcressWidget({Key key}) :super(key: key);

  @override
  State<StatefulWidget> createState()
  {
    return new HomeProcressState();
  }

  static String toStrings()
  {
    return "HomeProcressWidget";
  }
}

class HomeProcressState extends FuctionStateBase<HomeProcressWidget>
{
  Widget widget_body;

  Pager m_pager = new Pager();

  List<ProcInstsProcess> mList = new List();

  bool usePushStack()
  {
    return false;
  }

  void initState()
  {
    super.initState();
    query(1);
  }

  @override
  Widget build(BuildContext context)
  {
    return buildBodyProgressBar(new Column(
      children: <Widget>[
        new Row(
          children: <Widget>[
            new Chip(label: new Text(
                "我发起的流程", style: BasTextStyleRes.text_color_text1_larger),
                labelPadding: EdgeInsets.only(left: 5),
                avatar: new Icon(
                  IconFont.icon_application,
                  size: 18,
                  color: Colors.blue,
                ), backgroundColor: Colors.white),
          ],
        ),
        WidgetCreator.createCommonDevider(),
        widget_body == null
            ? WidgetCreator.createCommonNoData(text: BasStringRes.progressbar_text)
            : widget_body,
      ],
    ));
  }

  /*
   * 查询我发起的流程
   */
  void query(int page)
  {
    if (page == 1 && widget_body is StartedProcessListView)
    {
      widget_body = null;
      mList.clear();
    }
//    ProcessService.queryMyStarted(page, m_pager, this);
  }

  @override
  void onNetWorkFaild(String method, Object values)
  {
    if (method == "queryMyStarted")
    {
      setState(()
      {
        widget_body = WidgetCreator.createCommonNoData(text: values.toString(), onTap: ()
        {
          query(1);
        });
      });
    }
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryMyStarted")
    {
      setState(()
      {
        if (values is Map)
        {
          widget_body = values[MapKeyConstant.MAP_KEY_WIDGET];
          if (widget_body == null)
          {
            m_pager = values[MapKeyConstant.MAP_KEY_PAGER];
            mList.addAll(values[MapKeyConstant.MAP_KEY_LIST]);
            if (widget_body is BaseWrapListView)
            {}
            else
            {
              widget_body = new StartedProcessListView(
                list: mList,
                mHttpListener: this,
                mValueChangedMethod: (item)
                {
                  String pluginName = "AttSeeProcPlugin";
                  PluginsFactory.getInstance().get(pluginName).runPlugin(this, initPara: item);
                },
              );
            }
          }
        }
      });
    }
    else if (method == "updateRevokeProcess")
    {
      query(1);
    }
  }
}


/*
 * 类描述：首页组件提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class HomeProcressWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new HomeProcressWidget(key: initPara[MapKeyConstant.MAP_KEY_GLOBAL_KEY],);
  }
}
