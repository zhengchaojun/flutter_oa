// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProcInstsProcess.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProcInstsProcess _$ProcInstsProcessFromJson(Map<String, dynamic> json) {
  return ProcInstsProcess()
    ..id = json['id'] as String
    ..startTime = json['startTime'] as String
    ..processDefinitionId = json['processDefinitionId'] as String
    ..processDefinitionName = json['processDefinitionName'] as String
    ..businessKey = json['businessKey'] as String
    ..endTime = json['endTime'] as String
    ..startUserId = json['startUserId'] as String
    ..variables = json['variables'] == null
        ? null
        : VariablesBean.fromJson(json['variables'] as Map<String, dynamic>)
    ..tag = json['tag'];
}

Map<String, dynamic> _$ProcInstsProcessToJson(ProcInstsProcess instance) =>
    <String, dynamic>{
      'id': instance.id,
      'startTime': instance.startTime,
      'processDefinitionId': instance.processDefinitionId,
      'processDefinitionName': instance.processDefinitionName,
      'businessKey': instance.businessKey,
      'endTime': instance.endTime,
      'startUserId': instance.startUserId,
      'variables': instance.variables,
      'tag': instance.tag
    };
