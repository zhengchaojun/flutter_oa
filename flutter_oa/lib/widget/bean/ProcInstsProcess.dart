import 'package:flutteroa/widget/bean/VariablesBean.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ProcInstsProcess.g.dart';

/*
 * 类描述：
 * 作者：郑朝军 on 2019/5/9
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/9
 * 修改备注：
 */
@JsonSerializable()
class ProcInstsProcess
{
  /**
   * id : 582643
   * startTime : 2019-05-08 14:26:33
   * processDefinitionId : ddbxProcess:1:527591
   * processDefinitionName : 地大报销流程
   * businessKey :
   * endTime :
   * startUserId : 1705
   * variables : {"reason":"【地大报销】-加油票(04-30 1,585.00元)","applyUserName":"郑朝军","applyId":"58","total":"1585","businessKey":"1","bill":"DD-1905-024","position":"员工","depart":"移动产品开发部","table":"f_ddbxd","applyUserId":"1705","NAME_didaReview":"于国华","USER_didaReview":"1505","TQ_didaReview":"1"}
   */
  @JsonKey()
  String id;
  @JsonKey()
  String startTime;
  @JsonKey()
  String processDefinitionId;
  @JsonKey()
  String processDefinitionName;
  @JsonKey()
  String businessKey;
  @JsonKey()
  String endTime;
  @JsonKey()
  String startUserId;
  @JsonKey()
  VariablesBean variables;
  dynamic tag;

  String getId()
  {
    return id;
  }

  void setId(String id)
  {
    this.id = id;
  }

  String getStartTime()
  {
    return startTime;
  }

  void setStartTime(String startTime)
  {
    this.startTime = startTime;
  }

  String getProcessDefinitionId()
  {
    return processDefinitionId;
  }

  void setProcessDefinitionId(String processDefinitionId)
  {
    this.processDefinitionId = processDefinitionId;
  }

  String getProcessDefinitionName()
  {
    return processDefinitionName;
  }

  void setProcessDefinitionName(String processDefinitionName)
  {
    this.processDefinitionName = processDefinitionName;
  }

  String getBusinessKey()
  {
    return businessKey;
  }

  void setBusinessKey(String businessKey)
  {
    this.businessKey = businessKey;
  }

  String getEndTime()
  {
    return endTime;
  }

  void setEndTime(String endTime)
  {
    this.endTime = endTime;
  }

  String getStartUserId()
  {
    return startUserId;
  }

  void setStartUserId(String startUserId)
  {
    this.startUserId = startUserId;
  }

  VariablesBean getVariables()
  {
    return variables;
  }

  void setVariables(VariablesBean variables)
  {
    this.variables = variables;
  }

  dynamic getTag()
  {
    return tag;
  }

  void setTag(dynamic tag)
  {
    this.tag = tag;
  }


  ProcInstsProcess()
  {}

  /*
   * 反序列化
   */
  factory ProcInstsProcess.fromJson(Map<String, dynamic> json) =>
      _$ProcInstsProcessFromJson(json);

  /*
   * 序列化
   */
  Map<String, dynamic> toJson()
  => _$ProcInstsProcessToJson(this);
}
