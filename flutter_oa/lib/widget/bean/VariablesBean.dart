import 'package:json_annotation/json_annotation.dart';

part 'VariablesBean.g.dart';

/*
 * 类描述：
 * 作者：郑朝军 on 2019/5/9
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/9
 * 修改备注：
 */
@JsonSerializable()
class VariablesBean
{
  /**
   * reason : 【地大报销】-加油票(04-30 1,585.00元)
   * applyUserName : 郑朝军
   * applyId : 58
   * total : 1585
   * businessKey : 1
   * bill : DD-1905-024
   * position : 员工
   * depart : 移动产品开发部
   * table : f_ddbxd
   * applyUserId : 1705
   * NAME_didaReview : 于国华
   * USER_didaReview : 1505
   * TQ_didaReview : 1
   */

  @JsonKey()
  String reason;
  @JsonKey()
  String applyUserName;
  @JsonKey()
  String applyId;
  @JsonKey()
  String total;
  @JsonKey()
  String businessKey;
  @JsonKey()
  String bill;
  @JsonKey()
  String position;
  @JsonKey()
  String depart;
  @JsonKey()
  String table;
  @JsonKey()
  String applyUserId;
  @JsonKey()
  String NAME_didaReview;
  @JsonKey()
  String USER_didaReview;
  @JsonKey()
  String TQ_didaReview;

  String getReason()
  {
    return reason;
  }

  void setReason(String reason)
  {
    this.reason = reason;
  }

  String getApplyUserName()
  {
    return applyUserName;
  }

  void setApplyUserName(String applyUserName)
  {
    this.applyUserName = applyUserName;
  }

  String getApplyId()
  {
    return applyId;
  }

  void setApplyId(String applyId)
  {
    this.applyId = applyId;
  }

  String getTotal()
  {
    return total;
  }

  void setTotal(String total)
  {
    this.total = total;
  }

  String getBusinessKey()
  {
    return businessKey;
  }

  void setBusinessKey(String businessKey)
  {
    this.businessKey = businessKey;
  }

  String getBill()
  {
    return bill;
  }

  void setBill(String bill)
  {
    this.bill = bill;
  }

  String getPosition()
  {
    return position;
  }

  void setPosition(String position)
  {
    this.position = position;
  }

  String getDepart()
  {
    return depart;
  }

  void setDepart(String depart)
  {
    this.depart = depart;
  }

  String getTable()
  {
    return table;
  }

  void setTable(String table)
  {
    this.table = table;
  }

  String getApplyUserId()
  {
    return applyUserId;
  }

  void setApplyUserId(String applyUserId)
  {
    this.applyUserId = applyUserId;
  }

  String getNAME_didaReview()
  {
    return NAME_didaReview;
  }

  void setNAME_didaReview(String NAME_didaReview)
  {
    this.NAME_didaReview = NAME_didaReview;
  }

  String getUSER_didaReview()
  {
    return USER_didaReview;
  }

  void setUSER_didaReview(String USER_didaReview)
  {
    this.USER_didaReview = USER_didaReview;
  }

  String getTQ_didaReview()
  {
    return TQ_didaReview;
  }

  void setTQ_didaReview(String TQ_didaReview)
  {
    this.TQ_didaReview = TQ_didaReview;
  }

  VariablesBean()
  {}

  /*
   * 反序列化
   */
  factory VariablesBean.fromJson(Map<String, dynamic> json) => _$VariablesBeanFromJson(json);

  /*
   * 序列化
   */
  Map<String, dynamic> toJson() => _$VariablesBeanToJson(this);
}
