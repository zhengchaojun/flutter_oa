import 'package:flutter/material.dart';

/*
 * 功能：列表控件基类：
 * 1：解决无限高度问题 2：禁用ListView滑动事件
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
abstract class BaseWrapListView<T> extends StatefulWidget
{
  final List<T> mList;

  final bool mWrap;

  const BaseWrapListView({Key key, @required this.mList, this.mWrap: true}) : super(key: key);
}

/*
 *  列表控件状态基类<br/>
 */
abstract class BaseWrapListViewState<T extends BaseWrapListView> extends State<T>
{
  Widget build(BuildContext context)
  {
    List<Widget> list_tile = widget.mList.map((item)
    {
      return createListTile(item);
    }).toList();

    List<Widget> list_widget =
    ListTile.divideTiles(context: context, tiles: list_tile).toList();

    return new ListView(
        physics: widget.mWrap ? NeverScrollableScrollPhysics() : null,
        shrinkWrap: widget.mWrap,
        children: list_widget);
  }

  Widget createListTile(dynamic item);
}
