import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutteroa/%20core/CommonWidgetCreator.dart';
import 'package:flutteroa/constant/MapKeyConstant.dart';
import 'package:flutteroa/font/IconFont.dart';
import 'package:flutteroa/widget/bean/ProcInstsProcess.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';

import 'BaseWrapListView.dart';

/*
 * 我发起流程的列表<br/>
 */
class StartedProcessListView extends BaseWrapListView<ProcInstsProcess>
{
  ValueChanged<ProcInstsProcess> mValueChangedMethod;

  final HttpListener mHttpListener;

  StartedProcessListView({Key key, @required list, this.mHttpListener, @required this.mValueChangedMethod})
      : super(key: key, mList: list);

  State<StatefulWidget> createState()
  {
    return new ListViewState();
  }
}

class ListViewState extends BaseWrapListViewState<StartedProcessListView>
{

  Widget createListTile(_item)
  {
    ProcInstsProcess item = _item as ProcInstsProcess;
    return new ListTile(contentPadding: EdgeInsets.all(0), title: CommonWidgetCreator.createImgLeftTextRight(
        IconFont.icon_leavep,
        color: Colors.blue,
        imgSize: DigitValueConstant.APP_DIGIT_VALUE_30.toDouble(),
        title: item.getProcessDefinitionName(),
        describe: TimelineUtil.formatByDateTime(DateUtil.getDateTime(item.getStartTime())),
        time: item.getTag()[MapKeyConstant.MAP_KEY_NAME],
        column: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Flexible(
              child: new Container(
                child: new Text('${item.getVariables().getDepart()}',
                  style: BasTextStyleRes.text_red_color_text1_small, maxLines: 1,),
              ),
              flex: 1,
            ),

            new Flexible(
              child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      WeButton(
                        '撤销',
                        theme: WeButtonType.primary,
                        size: WeButtonSize.mini,
                        onClick: ()
                        {
                          WeDialog.confirm(context)(
                              '确认撤销吗?',
                              title: '提示信息',
                              onConfirm: ()
                              {
//                                ProcessService.updateRevokeProcess(item.getId(), widget.mHttpListener);
                              }
                          );
                        },
                      )
                    ],)
              ),
              flex: 1,
            ),
          ],
        )), onTap: ()
    {
      widget.mValueChangedMethod(item);
    },);
  }
}
