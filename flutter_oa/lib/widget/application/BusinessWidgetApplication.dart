
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_bas/zpub_bas.dart';

import '../FuncWidget.dart';
import '../HomeProcressWidget.dart';
import '../HomeProcsFunWidget.dart';
import '../HomeWidget.dart';
import '../HomeWillCardWidget.dart';
import '../MemberCenterWidget.dart';
import '../ProcCompWidget.dart';


/*
 * 类描述：其他模块的注册中心暂时写到此处，将来按照模块的方式依赖的时候再分开：
 * 插件中需要更换对应到组件需要把组件进行注册，如果对应功能到组件一定不会更换可以不用注册
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class BusinessWidgetApplication
{
  void onCreate()
  {
    List<Plugin> plugin = InitSvrMethod.getInitSvrPlugins();
    plugin.forEach((plugin)
    {
      if (plugin.classurl == HomeWidget.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
      { // 主页
        WidgetsFactory.getInstance().add(plugin.name, new HomeWidgetService());
      }
      else if (plugin.classurl == MemberCenterWidget.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
      { // 个人中心
        WidgetsFactory.getInstance().add(
            plugin.name, new MemberCenterWidgetService());
      }
      else if (plugin.classurl == FuncWidget.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
      { // 功能组
        WidgetsFactory.getInstance().add(
            plugin.name, new FuncWidgetService());
      }
      else if (plugin.classurl == ProcCompWidget.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
      { // 完结通知
        WidgetsFactory.getInstance().add(
            plugin.name, new ProcCompWidgetService());
      }
      else if (plugin.classurl == HomeProcsFunWidget.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
      { // 流程常用功能组件
        WidgetsFactory.getInstance().add(
            plugin.name, new HomeDailyFunWidgetService());
      }
      else if (plugin.classurl == HomeProcressWidget.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
      { // 流程功能组件
        WidgetsFactory.getInstance().add(
            plugin.name, new HomeProcressWidgetService());
      }
      else if (plugin.classurl == HomeWillCardWidget.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
      { // 主页面代办组件
        WidgetsFactory.getInstance().add(
            plugin.name, new HomeWillCardWidgetService());
      }
//      else if (plugin.classurl == TemplateAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 模板组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new TemplateWidgetService());
//      }
//      else if (plugin.classurl == HrLeaveAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 请假模板属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new HrLeaveAttWidgetService());
//      }
//      else if (plugin.classurl == HrLeaveProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 请假模板按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new HrLeaveProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == DuplicateDocWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 附件doc组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new DuplicateDocWidgetService());
//      }
//      else if (plugin.classurl == MessageTableWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 反馈意见组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new MessageTableWidgetService());
//      }
//      else if (plugin.classurl == TemplateBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 网络模版按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new TemplateBtnWidgetService());
//      }
//      else if (plugin.classurl == MyPrjWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 我的项目组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new MyPrjWidgetService());
//      }
//      else if (plugin.classurl == MyYearWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 我的年历
//        WidgetsFactory.getInstance().add(
//            plugin.name, new MyYearWidgetService());
//      }
//      else if (plugin.classurl == MessageStepperWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 消息步进组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new MessageStepperWidgetService());
//      }
//      else if (plugin.classurl == DdbxdProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 地大报销单按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new DdbxdProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == LicenseProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // License单按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new LicenseProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == BustripProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 出差单申请按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new BustripProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == DdbxdAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 地大报销单属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new DdbxdAttWidgetService());
//      }
//      else if (plugin.classurl == LicenseAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // License属性组件
//        WidgetsFactory.getInstance().add(plugin.name, new LicenseAttWidgetService());
//      }
//      else if (plugin.classurl == BustripAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 出差申请属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new BustripAttWidgetService());
//      }
//      else if (plugin.classurl == OaSealAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 印章使用流程属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new OaSealAttWidgetService());
//      }
//      else if (plugin.classurl == OaSealProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 印章使用流程属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new OaSealProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == OutProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 外出登记底部按钮属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new OutProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == OutAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 外出登记属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new OutAttWidgetService());
//      }
//
//      else if (plugin.classurl == GiftuseProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 礼品领用流程底部按钮
//        WidgetsFactory.getInstance().add(
//            plugin.name, new GiftuseProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == GiftuseAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 礼品领用流程属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new GiftuseAttWidgetService());
//      }
//      else if (plugin.classurl == HtpsProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 合同评审流程底部按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new HtpsProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == HtpsAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 合同评审流程属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new HtpsAttWidgetService());
//      }
//      else if (plugin.classurl == BjbdnProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 笔记本申请底部按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new BjbdnProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == FyBxdBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 费用报销单底部按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new FyBxdBtnWidgetService());
//      }
//      else if (plugin.classurl == BjbdnAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 笔记本申请底部按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new BjbdnAttWidgetService());
//      }
//      else if (plugin.classurl == PunchAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 补打卡流程属性组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new PunchAttWidgetService());
//      }
//      else if (plugin.classurl == PunchProcsBtnWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 补打卡流程底部按钮组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new PunchProcsBtnWidgetService());
//      }
//      else if (plugin.classurl == AutoCodeWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 自动编码组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new AutoCodeWidgetService());
//      }
//      else if (plugin.classurl == FBorrowAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 费用借支流程组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new FBorrowAttWidgetService());
//      }
//      else if (plugin.classurl == TxfbxAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 费用报销流程组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new TxfbxAttWidgetService());
//      }
//      else if (plugin.classurl == HtpscgAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 采购合同评审流程组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new HtpscgAttWidgetService());
//      }
//      else if (plugin.classurl == FyBxdAttWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 费用报销流程组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new FyBxdAttWidgetService());
//      }
//
//      // bill类别组件
//      else if (plugin.classurl == QtfWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 其他费组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new QtfWidgetService());
//      }
//      else if (plugin.classurl == SnjtfWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 室内交通费组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new SnjtfWidgetService());
//      }
//      else if (plugin.classurl == YtjtWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 远途交通费组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new YtjtWidgetService());
//      }
//      else if (plugin.classurl == ZsfWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 住宿费组件
//        WidgetsFactory.getInstance().add(
//            plugin.name, new ZsfWidgetService());
//      }
    });
  }
}
